﻿CREATE PROCEDURE [dbo].[usp_HCP_GetPropertyInfo]
(
@fhaNumber varchar(MAX)
)
AS

select p.PropertyID  as PropertyId,
	   p.ProjectName as PropertyName,
	   p.Reac_Last_Inspection_Score as ReacScore,
	   p.Troubled_Code as TroubledCode,
	   p.Is_Active_Dec_Case_Ind as ActiveDecCaseInd,
	   ISNULL(a.AddressLine1,' ') as StreetAddress,
	   ISNULL(a.City,' ') as City,
	   ISNULL(a.StateCode,' ') as State,
	   ISNULL(a.ZIP,' ') as  Zipcode,
	   ISNULL(CONVERT(VARCHAR,p.Initial_Endorsement_Date),' ') as InitialEndorsementDate,
	   ISNULL(a.AddressID, 0) as AddressId 
 from ProjectInfo p 
join address a on p.Property_AddressID = a.AddressID
join Lender_FHANumber lf on p.FHANumber = lf.FHANumber and p.LenderID = lf.LenderID and lf.FHA_EndDate is null and lf.FHANumber = @fhaNumber
order by p.ModifiedOn desc
