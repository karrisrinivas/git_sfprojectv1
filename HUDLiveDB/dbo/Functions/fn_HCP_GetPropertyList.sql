﻿
CREATE FUNCTION [dbo].[fn_HCP_GetPropertyList]
(
   @wlmListString NVARCHAR(100),
   @AeListString NVARCHAR(100),
   @LenderListString NVARCHAR(100)
)
RETURNS @PropertyIdsTable TABLE
(
	PropertyID INT
)
AS
BEGIN
	DECLARE @wlmList TABLE (HUD_WorkLoad_Manager_ID INT)
	DECLARE @useWlmList INT;

	IF @wlmListString IS NOT NULL AND @wlmListString != '-1'
	BEGIN
	    INSERT INTO @wlmList SELECT * FROM dbo.fn_StringSplitterToInt(@wlmListString)
	    IF EXISTS (SELECT * FROM @wlmList) 
	    BEGIN
		    SET @useWlmList = 1
	    END
	    ELSE
	    BEGIN
		    SET @useWlmList = 0
	    END
	END
	ELSE IF @wlmListString IS NOT NULL AND @wlmListString = '-1'
	BEGIN
	    SET @useWlmList = -1
	END
	ELSE
	BEGIN
		SET @useWlmList = 0
	END

	DECLARE @aeList TABLE (HUD_Account_Executive_ID INT)
	DECLARE @useAeList INT;

	IF @aeListString IS NOT NULL AND @AeListString != '-1'
	BEGIN
		INSERT INTO @aeList SELECT * FROM dbo.fn_StringSplitterToInt(@aeListString)
		IF EXISTS (SELECT * FROM @aeList) 
		BEGIN
			SET @useAeList = 1
		END
		ELSE
		BEGIN
			SET @useAeList = 0
		END
	END
	ELSE IF @aeListString IS NOT NULL AND @AeListString = '-1'
	BEGIN
		SET @useAeList = -1
	END
	ELSE
	BEGIN
	    SET @useAeList = 0
	END

	DECLARE @lenderList TABLE (HUD_Lender_ID INT)
	DECLARE @useLenderList INT;

	IF @lenderListString IS NOT NULL AND @LenderListString != '-1'
	BEGIN
	    INSERT INTO @lenderList SELECT * FROM dbo.fn_StringSplitterToInt(@lenderListString)
		IF EXISTS (SELECT * FROM @lenderList) 
		BEGIN
			SET @useLenderList = 1
		END
		ELSE
		BEGIN
			SET @useLenderList = 0
		END
	END
	ELSE IF @lenderListString IS NOT NULL AND @LenderListString = '-1'
	BEGIN
		SET @useLenderList = -1
	END
	ELSE
	BEGIN
		SET @useLenderList = 0
	END

	
	INSERT INTO @PropertyIdsTable SELECT DISTINCT PropertyID
						  from [dbo].[ProjectInfo]
						  WHERE (   @useWlmList = 0
						         OR @useWlmList = -1 AND HUD_WorkLoad_Manager_ID IS NULL
								 OR HUD_WorkLoad_Manager_ID in (select * from @wlmList))
						  AND   (   @useAeList = 0
						         OR @useAeList = -1 AND HUD_Project_Manager_ID IS NULL
						         OR HUD_Project_Manager_ID in (select * from @aeList))
						  AND   (   @useLenderList = 0
						         OR @useLenderList = -1 AND LenderID IS NULL
								 OR LenderID in (select * from @lenderList))
						  AND   (Deleted_Ind IS NULL OR Deleted_Ind = 0)

	RETURN 
END

