﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_MissingProject_ManagementReport_HighLevel]
(
@Year int
)

AS
SET FMTONLY OFF
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

	
	CREATE TABLE #MissingProjects
	(
		ProjectName NVARCHAR(100) null,
		FHANumber NVARCHAR(100) null,
		DebtCoverageRatio DECIMAL(19,2) null,
		WorkingCapital DECIMAL(19,2) null,
		DaysCashOnHand	DECIMAL(19,2) null,
		DaysInAcctReceivable DECIMAL(19,2) null,
		AvgPaymentPeriod DECIMAL(19,2) null,
		Amortized_Unpaid_Principal_Bal DECIMAL(19,2) null,
		HudProjectManagerID INT null,
		HudProjectManagerName NVARCHAR(100) null,
		HudProjectManagerEmailAddress NVARCHAR(100) null,
		HudWorkLoadManagerID INT null,
		HudWorkLoadManagerName NVARCHAR(100) null,
		HudWorkLoadManagerEmailAddress NVARCHAR(100) null,	
		NumberOfProjects INT null,
		ExpectedNumber INT null,
		ReceivedNumber INT null,
		MissingNumber AS (ExpectedNumber - ReceivedNumber)
	);
	DECLARE @CurrentYear INT
	IF @Year = YEAR(GETDATE())
		SET @CurrentYear = 1
	ELSE 
		SET @CurrentYear = 0
	BEGIN
		IF @CurrentYear = 1
		BEGIN
			INSERT INTO #MissingProjects
			SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			(SELECT COUNT(DISTINCT(PF.FHANumber)) FROM [$(DatabaseName)].dbo.Lender_FHANumber LF
			 INNER JOIN [$(DatabaseName)].dbo.ProjectInfo PF
			 ON LF.FHANumber = PF.FHANumber
			 WHERE (FHA_EndDate IS NULL OR Year(FHA_EndDate) > (GETDATE() - 1))
							       AND (Year(FHA_StartDate) <= @Year)),
			(SELECT dbo.fn_HCP_GetTotalExpectedCount(@Year,0,0)),
			(SELECT dbo.fn_HCP_GetReceivedCount(@Year,0,0))			
		END
		ELSE IF @CurrentYear = 0
		BEGIN
			INSERT INTO #MissingProjects
			SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			(SELECT COUNT(DISTINCT(PF.FHANumber)) FROM [$(DatabaseName)].dbo.Lender_FHANumber LF
			 INNER JOIN [$(DatabaseName)].dbo.ProjectInfo PF
			 ON LF.FHANumber = PF.FHANumber
			 WHERE (FHA_EndDate IS NULL OR Year(FHA_EndDate) > @Year)
							       AND (Year(FHA_StartDate) <= @Year)),
			(SELECT dbo.fn_HCP_GetTotalExpectedCount(@Year,0,0)),
			(SELECT dbo.fn_HCP_GetReceivedCount(@Year,0,0))
		END	
	END
	SELECT * FROM #MissingProjects
END



SET FMTONLY ON

GO

