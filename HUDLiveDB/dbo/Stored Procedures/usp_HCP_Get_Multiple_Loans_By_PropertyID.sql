﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Multiple_Loans_By_PropertyID]
(
@PropertyID int
)
AS

SELECT 
	PI.FHANumber, 
	LI.Lender_Name,
	A.AddressLine2,
	A.City,
	A.StateCode, 
	A.ZIP
FROM [$(DatabaseName)].[dbo].[ProjectInfo] PI
LEFT JOIN [$(DatabaseName)].[dbo].[LenderInfo] LI
ON PI.LenderID = LI.LenderID
LEFT JOIN [$(DatabaseName)].[dbo].[Address] A
ON PI.Lender_AddressID = A.AddressID
WHERE PI.PropertyID = @PropertyID

