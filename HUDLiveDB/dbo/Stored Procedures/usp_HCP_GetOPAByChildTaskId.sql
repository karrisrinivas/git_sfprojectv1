﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetOPAByChildTaskId]
(
@TaskInstanceId uniqueidentifier
)      
AS
BEGIN
       
       SELECT OP.ProjectActionFormId,
       op.FhaNumber,
       op.PropertyName,
       op.RequestDate,
       op.ServicerSubmissionDate,
       op.ProjectActionStartDate,
       op.ProjectActionTypeId,
       op.RequestStatus,
       op.MytaskId,
       op.CreatedOn,
       op.ModifiedOn,
       op.CreatedBy,
       op.ModifiedBy,
       op.ServicerComments,
       op.AEComments,
       op.TaskInstanceId,
       op.RequesterName,
       op.IsAddressChange
       FROM [$(TaskDB)].dbo.ParentChildTask PT
       JOIN OPAForm OP
       ON PT.ParentTaskInstanceId = OP.TaskInstanceId
       WHERE PT.ChildTaskInstanceId = @TaskInstanceId
END

GO

