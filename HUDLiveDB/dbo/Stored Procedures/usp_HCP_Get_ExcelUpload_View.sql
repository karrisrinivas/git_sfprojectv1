﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_ExcelUpload_View]
(
@UserID INT
)
AS

--SELECT distinct LDI.DataInserted, LI.Lender_Name, LDI.LenderID,  A.FirstName + ' ' + A.LastName as UserName FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] LDI
--INNER JOIN [$(DatabaseName)].[dbo].[LenderInfo] LI ON LDI.LenderID = LI.LenderID
--INNER JOIN [$(DatabaseName)].[dbo].[HCP_Authentication] A ON LDI.UserID = A.UserID
--WHERE LDI.UserID = @UserID

SELECT distinct LDI.DataInserted, LI.Lender_Name, LDI.LenderID,  A.FirstName + ' ' + A.LastName as UserName, 
		(SELECT COUNT(*) FROM [$(DatabaseName)].[dbo].[Lender_FHANumber] LF WHERE LF.LenderID =  LDI.LenderID AND LF.FHA_EndDate IS NULL) AS Total_FHANumber
		--(SELECT COUNT(*) FROM [$(DatabaseName)].[dbo].[Lender_FHANumber] LF WHERE LF.LenderID =  LDI.LenderID and LF.FHANumber = LDI.FHANumber) AS Lender_FHANumer_Matching
FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] LDI
INNER JOIN [$(DatabaseName)].[dbo].[LenderInfo] LI ON LDI.LenderID = LI.LenderID
INNER JOIN [$(DatabaseName)].[dbo].[HCP_Authentication] A ON LDI.UserID = A.UserID
WHERE LDI.UserID = @UserID


