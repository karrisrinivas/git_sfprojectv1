﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_MissingProject_ManagementReport]
(
@UserType NVARCHAR(100),
@Username  NVARCHAR(100),
@Year INT
)

AS
SET FMTONLY OFF
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @Count INT
SET @Count = 0
DECLARE @CurrentYear INT
IF @Year = YEAR(GETDATE())
	SET @CurrentYear = 1
ELSE 
	SET @CurrentYear = 0
CREATE TABLE #MissingProjects
(
	ProjectName NVARCHAR(100) NULL,
	FHANumber NVARCHAR(100) NULL,
	DebtCoverageRatio DECIMAL(19,2) NULL,
	WorkingCapital DECIMAL(19,2) NULL,
	DaysCashOnHand	DECIMAL(19,2) NULL,
	DaysInAcctReceivable DECIMAL(19,2) NULL,
	AvgPaymentPeriod DECIMAL(19,2) NULL,
	Amortized_Unpaid_Principal_Bal DECIMAL(19,2) NULL,
	HudProjectManagerID INT NULL,
	HudProjectManagerName NVARCHAR(100) NULL,
	HudProjectManagerEmailAddress NVARCHAR(100) NULL,
	HudWorkLoadManagerID INT NULL,
	HudWorkLoadManagerName NVARCHAR(100) NULL,
	HudWorkLoadManagerEmailAddress NVARCHAR(100) NULL,	
	MissingQuarter NVARCHAR(100) NULL,
	QuarterEndDate DATETIME NULL,
	NumberOfProjects INT NULL,
	ExpectedNumber INT NULL,
	ReceivedNumber INT NULL,
	MissingNumber AS (ExpectedNumber - ReceivedNumber)
);
IF @UserType = 'AccountExecutive'
	BEGIN	
	IF @CurrentYear = 1
		BEGIN
			INSERT INTO #MissingProjects			
			SELECT FI.FHADescription,D.FHANumber, NULL,NULL,NULL,NULL,NULL,NULL,
			D.HUD_Project_Manager_ID,D.HUD_Project_Manager_Name,@Username,
			D.HUD_WorkLoad_Manager_ID,D.HUD_WorkLoad_Manager_Name,NULL,	QuarterMissed, QuarterEndDate, NULL,NULL,NULL FROM 
			(SELECT DISTINCT PF.FHANumber,AE.HUD_Project_Manager_ID,AE.HUD_Project_Manager_Name,AD.Email,
			WLM.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name FROM ProjectInfo PF 
			INNER JOIN HUD_Project_Manager AE ON PF.HUD_Project_Manager_ID = AE.HUD_Project_Manager_ID
			INNER JOIN Address AD ON AD.AddressID = AE.AddressID 
			INNER JOIN Lender_DataUpload_Live LDL ON LDL.FHANumber = PF.FHANumber
			INNER JOIN HUD_WorkLoad_Manager WLM ON WLM.HUD_WorkLoad_Manager_ID = PF.HUD_WorkLoad_Manager_ID
			WHERE AD.Email = @Username 
			AND PF.FHANumber IN 
			(SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
			WHERE (FHA_EndDate IS NULL OR Year(FHA_EndDate) > (GETDATE() - 1))
							       AND (Year(FHA_StartDate) <= @Year))) AS D
			INNER JOIN FHAInfo_iREMS FI ON FI.FHANumber = D.FHANumber 
				CROSS APPLY [dbo].fn_HCP_GetMissingQuarters(@Year,D.FHANumber) 
		END
	ELSE IF @CurrentYear = 0
		BEGIN 
			INSERT INTO #MissingProjects			
			SELECT FI.FHADescription,D.FHANumber, NULL,NULL,NULL,NULL,NULL,NULL,
			D.HUD_Project_Manager_ID,D.HUD_Project_Manager_Name,@Username,
			D.HUD_WorkLoad_Manager_ID,D.HUD_WorkLoad_Manager_Name,NULL,	QuarterMissed, QuarterEndDate, NULL,NULL,NULL FROM 
			(SELECT DISTINCT PF.FHANumber,AE.HUD_Project_Manager_ID,AE.HUD_Project_Manager_Name,AD.Email,
			WLM.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name FROM ProjectInfo PF 
			INNER JOIN HUD_Project_Manager AE ON PF.HUD_Project_Manager_ID = AE.HUD_Project_Manager_ID
			INNER JOIN Address AD ON AD.AddressID = AE.AddressID 
			INNER JOIN Lender_DataUpload_Live LDL ON LDL.FHANumber = PF.FHANumber
			INNER JOIN HUD_WorkLoad_Manager WLM ON WLM.HUD_WorkLoad_Manager_ID = PF.HUD_WorkLoad_Manager_ID
			WHERE AD.Email = @Username 
			AND PF.FHANumber IN 
			(SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
			WHERE (FHA_EndDate IS NULL OR Year(FHA_EndDate) > @Year)
							       AND (Year(FHA_StartDate) <= @Year))) AS D
			INNER JOIN FHAInfo_iREMS FI ON FI.FHANumber = D.FHANumber 
				CROSS APPLY [dbo].fn_HCP_GetMissingQuarters(@Year,D.FHANumber) 
		END
	END
ELSE 
IF @UserType = 'WorkflowManager'
	BEGIN
		IF @CurrentYear = 1
		BEGIN
			INSERT INTO #MissingProjects
			SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,PF.HUD_Project_Manager_ID,AE.HUD_Project_Manager_Name, MAX(AD.Email),
			MAX(WLM.HUD_WorkLoad_Manager_ID),MAX(WLM.HUD_WorkLoad_Manager_Name), @Username,NULL,NULL,
			COUNT(*),
			dbo.fn_HCP_GetTotalExpectedCount(@Year,0,PF.HUD_Project_Manager_ID),  
			dbo.fn_HCP_GetReceivedCount(@Year,0,PF.HUD_Project_Manager_ID)
			FROM [$(DatabaseName)].dbo.ProjectInfo PF
			INNER JOIN [$(DatabaseName)].dbo.HUD_Project_Manager AE ON PF.HUD_Project_Manager_ID = AE.HUD_Project_Manager_ID
			INNER JOIN [$(DatabaseName)].dbo.Address AD ON AD.AddressID = AE.AddressID
			INNER JOIN [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			WHERE FHANumber IN 
			(SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
				WHERE (FHA_EndDate IS NULL OR FHA_EndDate > (GETDATE() - 1))
								   AND (Year(FHA_StartDate) <= @Year))
			AND PF.HUD_WorkLoad_Manager_ID = (
			SELECT HUD_WorkLoad_Manager_ID FROM [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WHERE AddressID =
			 (SELECT AddressID FROM [$(DatabaseName)].dbo.Address WHERE Email = @Username)) 
			 GROUP BY PF.HUD_Project_Manager_ID, AE.HUD_Project_Manager_Name, AE.HUD_Project_Manager_ID
		END
		ELSE IF @CurrentYear = 0
		BEGIN
			INSERT INTO #MissingProjects
			SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,PF.HUD_Project_Manager_ID,AE.HUD_Project_Manager_Name, MAX(AD.Email),
			MAX(WLM.HUD_WorkLoad_Manager_ID),MAX(WLM.HUD_WorkLoad_Manager_Name), @Username,NULL,NULL,
			COUNT(*),
			dbo.fn_HCP_GetTotalExpectedCount(@Year,0,PF.HUD_Project_Manager_ID),  
			dbo.fn_HCP_GetReceivedCount(@Year,0,PF.HUD_Project_Manager_ID)
			FROM [$(DatabaseName)].dbo.ProjectInfo PF
			INNER JOIN [$(DatabaseName)].dbo.HUD_Project_Manager AE ON PF.HUD_Project_Manager_ID = AE.HUD_Project_Manager_ID
			INNER JOIN [$(DatabaseName)].dbo.Address AD ON AD.AddressID = AE.AddressID
			INNER JOIN [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			WHERE FHANumber IN 
			(SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
				WHERE (FHA_EndDate IS NULL OR FHA_EndDate > @Year)
								   AND (Year(FHA_StartDate) <= @Year))
			AND PF.HUD_WorkLoad_Manager_ID = (
			SELECT HUD_WorkLoad_Manager_ID FROM [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WHERE AddressID =
			 (SELECT AddressID FROM [$(DatabaseName)].dbo.Address WHERE Email = @Username)) 
			 GROUP BY PF.HUD_Project_Manager_ID, AE.HUD_Project_Manager_Name, AE.HUD_Project_Manager_ID
		END		
	END
ELSE 
	BEGIN
		IF @UserType = 'HUDAdmin' OR @UserType = 'SuperUser' OR @UserType = 'HUDDirector'
			BEGIN 
			IF @CurrentYear = 1
				BEGIN
					INSERT INTO #MissingProjects
					SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
					WLM.HUD_WorkLoad_Manager_ID,
					WLM.HUD_WorkLoad_Manager_Name, MAX(AD.Email) ,NULL,NULL,
					COUNT(*),			
					dbo.fn_HCP_GetTotalExpectedCount(@Year,PF.HUD_WorkLoad_Manager_ID,0),
					dbo.fn_HCP_GetReceivedCount(@Year,PF.HUD_WorkLoad_Manager_ID,0)
					FROM [$(DatabaseName)].dbo.ProjectInfo PF
					INNER JOIN [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID 
					INNER JOIN Address AD ON AD.AddressID = WLM.AddressID
					WHERE FHANumber IN(
						SELECT DISTINCT FHANumber
						FROM [$(DatabaseName)].dbo.ProjectInfo
						WHERE FHANumber IN( 
							SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
							WHERE (FHA_EndDate IS NULL OR FHA_EndDate > (GETDATE() - 1))
								   AND (Year(FHA_StartDate) <= @Year))
							GROUP BY HUD_WorkLoad_Manager_ID,FHANumber) 
					GROUP BY PF.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name,WLM.HUD_WorkLoad_Manager_ID 
				END
			ELSE IF @CurrentYear = 0
				BEGIN
					INSERT INTO #MissingProjects
					SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
					WLM.HUD_WorkLoad_Manager_ID,
					WLM.HUD_WorkLoad_Manager_Name, MAX(AD.Email) ,NULL,NULL,
					COUNT(*),			
					dbo.fn_HCP_GetTotalExpectedCount(@Year,PF.HUD_WorkLoad_Manager_ID,0),
					dbo.fn_HCP_GetReceivedCount(@Year,PF.HUD_WorkLoad_Manager_ID,0)
					FROM [$(DatabaseName)].dbo.ProjectInfo PF
					INNER JOIN [$(DatabaseName)].dbo.HUD_WorkLoad_Manager WLM ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID 
					INNER JOIN Address AD ON AD.AddressID = WLM.AddressID
					WHERE FHANumber IN(
						SELECT DISTINCT FHANumber
						FROM [$(DatabaseName)].dbo.ProjectInfo
						WHERE FHANumber IN( 
							SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_FHANumber 
							WHERE (FHA_EndDate IS NULL OR Year(FHA_EndDate) > @Year)
							       AND (Year(FHA_StartDate) <= @Year))
						GROUP BY HUD_WorkLoad_Manager_ID,FHANumber) 
					GROUP BY PF.HUD_WorkLoad_Manager_ID,WLM.HUD_WorkLoad_Manager_Name,WLM.HUD_WorkLoad_Manager_ID 
				END						
				
			END			
	END
	SELECT * FROM #MissingProjects
END



SET FMTONLY ON







GO

