﻿--USE [HCP_Live_db_Prod]
--GO

--/****** Object:  StoredProcedure [dbo].[usp_HCP_GetLenderPAMReportDetail]    Script Date: 10/20/2017 2:27:13 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE PROCEDURE [dbo].[usp_HCP_GetLenderPAMReportDetail]
(
	@RoleList varchar(MAX),
	@LAMList varchar(MAX),
	@BAMList varchar(MAX),
	@LARList varchar(MAX),
	@StartDate datetime,
	@EndDate datetime,
	@Status int,
	@ProjectAction varchar(100),
	@UserId int,
	@UserLenderId int

)
AS
BEGIN
	DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10) 
	Declare @SelectedLendersList table (LenderUserId int)

	--get role name for user
	Declare @UserRoleName nvarchar(256)
	select @UserRoleName = webRoles.[RoleName] from [dbo].[webpages_UsersInRoles] webRolesUser 
	inner join [dbo].[webpages_Roles] webRoles 
	on webRolesUser.RoleId = webRoles.RoleId
	where webRolesUser.UserId = @UserId

	--if user role name is lar, then insert userid to lenders list table. 
	if (@UserRoleName ='LenderAccountRepresentative')
		insert into @SelectedLendersList values (@UserId)
	else if (@UserRoleName ='LenderAccountManager' or @UserRoleName ='BackupAccountManager')
	begin

		if(@RoleList IS NULL OR @RoleList = '')
		begin
			INSERT INTO @SelectedLendersList	select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
												inner join [dbo].[User_Lender] userLender
												on userLender.[User_ID] = userMaster.UserID
												inner join [dbo].[webpages_UsersInRoles] userRoles
												on userRoles.UserId = userMaster.UserID
												where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId in(13,14,15)
		end
		else
		begin

			
		if CHARINDEX('1',@RoleList) > 0
		begin
			--IF @LAMList IS NULL OR @LAMList = ''
			--	INSERT INTO @SelectedLendersList    select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
			--										inner join [dbo].[User_Lender] userLender
			--										on userLender.[User_ID] = userMaster.UserID
			--										inner join [dbo].[webpages_UsersInRoles] userRoles
			--										on userRoles.UserId = userMaster.UserID
			--										where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =15
			--ELSE 
				INSERT INTO @SelectedLendersList SELECT * FROM dbo.fn_StringSplitterToInt(@LAMList)
		end

			
		if CHARINDEX('2',@RoleList) > 0
		begin
			--IF @BAMList IS NULL OR @BAMList = ''
			--	INSERT INTO @SelectedLendersList   select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
			--										inner join [dbo].[User_Lender] userLender
			--										on userLender.[User_ID] = userMaster.UserID
			--										inner join [dbo].[webpages_UsersInRoles] userRoles
			--										on userRoles.UserId = userMaster.UserID
			--										where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =14
			--ELSE 
				INSERT INTO @SelectedLendersList SELECT * FROM dbo.fn_StringSplitterToInt(@BAMList)
		end

			
		if CHARINDEX('3',@RoleList) > 0
		begin
			--IF @LARList IS NULL OR @LARList = ''
			--	INSERT INTO @SelectedLendersList    select userMaster.[UserID] from [dbo].[HCP_Authentication] userMaster
			--										inner join [dbo].[User_Lender] userLender
			--										on userLender.[User_ID] = userMaster.UserID
			--										inner join [dbo].[webpages_UsersInRoles] userRoles
			--										on userRoles.UserId = userMaster.UserID
			--										where userLender.Lender_ID = @UserLenderId  and userRoles.RoleId =13
			--ELSE 
				INSERT INTO @SelectedLendersList SELECT * FROM dbo.fn_StringSplitterToInt(@LARList)
		end

			
		if CHARINDEX('0',@RoleList) > 0
		begin
			IF NOT EXISTS(SELECT 1 FROM @SelectedLendersList  WHERE LenderUserId = @UserId)
			insert into @SelectedLendersList values (@UserId)
		end
		 else
		 begin
			IF EXISTS(SELECT 1 FROM @SelectedLendersList  WHERE LenderUserId = @UserId)
				delete from  @SelectedLendersList  WHERE LenderUserId = @UserId
		 end
		 end
	end

	if (@StartDate is not null and @EndDate is null)    set @EndDate =   GETUTCDATE()	
	if (@StartDate is null and @EndDate is not null)    set @StartDate = '01-01-2015'

	DECLARE @SelectedProjectActions TABLE (PamProjectActionId INT)		
	IF @ProjectAction IS NULL OR @ProjectAction = ''
		INSERT INTO @SelectedProjectActions  SELECT PamProjectActionId FROM  PamProjectActionTypes 
	ELSE 
		INSERT INTO @SelectedProjectActions SELECT * FROM dbo.fn_StringSplitterToInt(@ProjectAction)

	CREATE TABLE #TaskReAssignment
	(
		FromAeId int,
		ToAeid int,
		TaskId int,
		CreatedOn Datetime NULL,
		From_HUD_Project_Manager_ID int null,
		To_HUD_Project_Manager_ID  int null,
		From_HUD_WorkLoad_Manager_ID int null,
		To_HUD_WorkLoad_Manager_ID int null,
		ReAssignedTo VARCHAR(100) null,
		ReAssignedBy VARCHAR(100) null,
		ReAssignedFrom VARCHAR(100) null,
  		HUDPM VARCHAR(100) null
	);

	INSERT INTO #TaskReAssignment
	SELECT 
	b.UserID,
	a.userid,
	t.TaskId,
	tassign.CreatedOn,
	from_pmger.HUD_Project_Manager_ID,
	to_pmger.HUD_Project_Manager_ID,
	null,
	null,
	a.FirstName+' '+a.LastName as ReAssginedTo,
	c.FirstName+' '+c.LastName  as ReAssignedBy, 
    tassign.FromAE as ReAssginedFrom,
    t1.AssignedTo as HUDPM
			 FROM [$(TaskDB)].[dbo].[TaskReAssignments] tassign
	           JOIN    [$(TaskDB)].dbo.[Task] t ON tassign.TaskInstanceId = t.TaskInstanceId
					 left JOIN    [$(TaskDB)].dbo.[Task] t1  ON (tassign.TaskInstanceId = t1.TaskInstanceId and t1.SequenceId = 0)
				  INNER JOIN 
					(
						SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
						FROM [$(TaskDB)].[dbo].[Task]
						GROUP BY TaskInstanceId
					) TMAX
					ON t.TaskInstanceId = TMAX.TaskInstanceId and t.SequenceId = TMAX.maxSeqId  
						 JOIN  dbo.HCP_Authentication a
					 ON a.UserName = tassign.ReAssignedTo
				 JOIN  dbo.HCP_Authentication b
					 ON b.UserName = tassign.FromAE 
				JOIN  dbo.HCP_Authentication c
					 ON c.userid = tassign.CreatedBy   
				 JOIN User_WLM_PM from_pm_ulm 
					 ON from_pm_ulm.userid = a.UserID-- and ulm.Deleted_Ind = 0
				 JOIN HUD_Project_Manager to_pmger 
					 ON to_pmger.HUD_Project_Manager_ID = from_pm_ulm.HUD_Project_Manager_ID
				  JOIN User_WLM_PM to_pm_ulm 
					 ON to_pm_ulm.userid = b.UserID-- and ulm.Deleted_Ind = 0
				  JOIN  HUD_Project_Manager from_pmger 
					 ON from_pmger.HUD_Project_Manager_ID = to_pm_ulm.HUD_Project_Manager_ID
				 WHERE t.IsReassigned = 1
				 
				 
				 	 --  update from workload manager
	UPDATE t set From_HUD_WorkLoad_Manager_ID = pinfo.HUD_WorkLoad_Manager_ID
	from  #TaskReAssignment t
		JOIN ProjectInfo pinfo on pinfo.HUD_Project_Manager_ID = t.From_HUD_Project_Manager_ID
	--- Update to work load manager
	UPDATE t set To_HUD_WorkLoad_Manager_ID = pinfo.HUD_WorkLoad_Manager_ID
	from  #TaskReAssignment t
		JOIN ProjectInfo pinfo on pinfo.HUD_Project_Manager_ID = t.To_HUD_Project_Manager_ID

	CREATE TABLE #LenderPAMReport
	(
		FHANumber NVARCHAR(15), 
		PropertyId INT, 
		PropertyName NVARCHAR(100), 
		HudProjectManagerName NVARCHAR(100),
		HudWorkloadManagerName NVARCHAR(100),
		ProjectActionName NVARCHAR(100),
		Comment NVARCHAR(2000),
		ProjectActionSubmitDate DATETIME,
		ProjectActionCompletionDate DATETIME,
		ProjectActionDaysToComplete INT,
		IsProjectActionOpen BIT,
		RequestedAmount DECIMAL(19,2),
		ApprovedAmount DECIMAL(19,2),
		LenderUserName NVARCHAR(100),
		UserRoleName varchar(50),
		Id uniqueidentifier,
		TaskId int,
		ReAssignedTo VARCHAR(100) null,
		ReAssignedDate DATETIME null,
		ReAssignedBy VARCHAR(100) null,
		RequestStatus VARCHAR(100) null,
		ProcessedBy VARCHAR(100) null,
		AdditionalInfoCount int null,
		ParentChildInstanceId  uniqueidentifier null,
		ReAssignedFrom VARCHAR(100) null,
		HUDPM VARCHAR(100) null
	);

	IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 1 )  
	BEGIN
		INSERT INTO #LenderPAMReport
		SELECT R4R.FHANumber, R4R.PropertyId, R4R.PropertyName, PM.HUD_Project_Manager_Name, WLM.HUD_WorkLoad_Manager_Name,
		'R4R',
		(CASE WHEN R4R.RequestStatus = 1 OR R4R.RequestStatus = 4 THEN 'Servicer Comments: '+R4R.ServicerRemarks
				WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 5 THEN IsNull('Servicer Comments: '+R4R.ServicerRemarks+@NewLineChar,'')+'' + IsNull('HUD Remarks: '+R4R.HudRemarks+@NewLineChar,'')+''+IsNull('Additional Remarks: '+R4R.AdditionalRemarks,'') ELSE NULL END), 
		R4R.ServicerSubmissionDate, 
		(CASE WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 OR R4R.RequestStatus = 5 THEN R4R.ModifiedOn ELSE NULL END),
		(CASE WHEN R4R.RequestStatus = 2 THEN DATEDIFF(DAY,R4R.ServicerSubmissionDate,R4R.ModifiedOn) ELSE DATEDIFF(DAY,R4R.ServicerSubmissionDate,GETUTCDATE()) END), 
		(CASE WHEN R4R.RequestStatus = 1 THEN 1 ELSE 0 END), 
		R4R.TotalRequestedAmount, 
		(CASE WHEN (R4R.IsLenderDelegate =1 AND R4R.RequestStatus = 4) THEN R4R.TotalApprovedAmount
				WHEN R4R.RequestStatus = 4 THEN R4R.TotalRequestedAmount 
				WHEN R4R.RequestStatus = 2 THEN R4R.TotalApprovedAmount
				ELSE NULL END),
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE UserID = R4R.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = R4R.CreatedBy)),
		R4RId as Id, R4R.TaskId, 
		null, 
		null,
		null,
		(CASE   
		        WHEN R4R.RequestStatus = 1 THEN 'Pending'
				WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN 'Accepted'
				WHEN R4R.RequestStatus = 5 THEN 'Denied'
				ELSE NULL END),
		(CASE WHEN R4R.IsLenderDelegate = 1 THEN 'Lender Delegated'
				WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 5 THEN 'Account Executive'
				WHEN R4R.RequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END),
				null ,null,
				null,
				(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE UserName = TRA.ReAssignedFrom) as HUDPM
		FROM Reserve_For_Replacement_Form_Data R4R
		INNER JOIN ProjectInfo PF
		ON R4R.FHANumber = PF.FHANumber
		INNER JOIN @SelectedLendersList lenders
		ON lenders.LenderUserId = R4R.CreatedBy
		INNER JOIN User_WLM_PM UWM
		ON PF.HUD_Project_Manager_ID = UWM.HUD_Project_Manager_ID
		--AND PF.HUD_Project_Manager_ID IN (SELECT HUD_Project_Manager_ID FROM @AeList)
		INNER JOIN HUD_Project_Manager PM
		ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
		INNER JOIN HUD_WorkLoad_Manager WLM
		ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
		LEFT JOIN #TaskReAssignment TRA
		ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = R4R.TaskId 
		WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
         (  (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   R4R.ServicerSubmissionDate>= @StartDate and R4R.ServicerSubmissionDate< DATEADD(day,1,@EndDate))
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND R4R.ServicerSubmissionDate <= GETUTCDATE()) )
		AND TRA.FromAeId is null and R4R.TaskId is not null and uwm.Deleted_Ind is Null-- task re assignments exclude

		-- Task re assgingment items include
		INSERT INTO #LenderPAMReport
		SELECT R4R.FHANumber, R4R.PropertyId, R4R.PropertyName, PM.HUD_Project_Manager_Name, from_wlm.HUD_WorkLoad_Manager_Name,
		'R4R',
		(CASE WHEN R4R.RequestStatus = 1 OR R4R.RequestStatus = 4 THEN 'Servicer Comments: '+R4R.ServicerRemarks
				WHEN R4R.RequestStatus = 2 THEN IsNull('Servicer Comments: '+R4R.ServicerRemarks+@NewLineChar,'')+''+ IsNull('HUD Remarks: '+R4R.HudRemarks+@NewLineChar,'')+''+IsNull('AdditionalRemarks: '+R4R.AdditionalRemarks,'') ELSE NULL END), 
		R4R.ServicerSubmissionDate, 
		(CASE WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN R4R.ModifiedOn ELSE NULL END),
		(CASE WHEN R4R.RequestStatus = 2 THEN DATEDIFF(DAY,TRA.CreatedOn,R4R.ModifiedOn) ELSE DATEDIFF(DAY,TRA.CreatedOn,GETUTCDATE()) END), 
		(CASE WHEN R4R.RequestStatus = 1 THEN 1 ELSE 0 END), 
		R4R.TotalRequestedAmount, 
		(CASE WHEN (R4R.IsLenderDelegate =1 AND R4R.RequestStatus = 4) THEN R4R.TotalApprovedAmount
				WHEN R4R.RequestStatus = 4 THEN R4R.TotalRequestedAmount 
				WHEN R4R.RequestStatus = 2 THEN R4R.TotalApprovedAmount
				ELSE NULL END),
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE UserID = R4R.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = R4R.CreatedBy)),
		R4RId as Id,
		R4R.TaskId, 
		TRA.ReAssignedTo, 
		TRA.CreatedOn,
		TRA.ReAssignedBy,
		(CASE   
				WHEN R4R.RequestStatus = 1 THEN 'Pending'
				WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN 'Accepted'
				ELSE NULL END),
		(CASE WHEN R4R.IsLenderDelegate = 1 THEN 'Lender Delegated'
				WHEN R4R.RequestStatus = 2 THEN 'Account Executive'
				WHEN R4R.RequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END),
				null ,null ,
				(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as ReAssignedFrom,
				(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom ) as HUDPM-- hudpm
		FROM #TaskReAssignment TRA 
		INNER JOIN  Reserve_For_Replacement_Form_Data R4R
			ON TRA.TaskId = R4R.TaskId
		INNER JOIN ProjectInfo PF
			ON R4R.FHANumber = PF.FHANumber
		INNER JOIN @SelectedLendersList lenders
			ON lenders.LenderUserId = R4R.CreatedBy 
			JOIN HUD_Project_Manager PM
			ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			JOIN HUD_WorkLoad_Manager  from_wlm
			ON TRA.From_HUD_WorkLoad_Manager_ID  = from_wlm.HUD_WorkLoad_Manager_ID
		WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
        (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate)) 
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE())
	END

	IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 0 )  

	BEGIN

		INSERT INTO #LenderPAMReport
		SELECT NCR.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, WLM.HUD_WorkLoad_Manager_Name,
		'NCR',(CASE WHEN NCR.RequestStatus = 2 then 'Servicer Comments: '+ NCR.ServicerRemarks else 'AE Comments: '+ Task.notes End), NCR.SubmittedDate, 
					(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 4 OR NCR.RequestStatus = 5 THEN isnull(NCR.ApprovedDate,NCR.ModifiedOn) ELSE NULL END),
		(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 THEN DATEDIFF(DAY,NCR.SubmittedDate,NCR.ApprovedDate) ELSE DATEDIFF(DAY,NCR.SubmittedDate,GETUTCDATE()) END), 
		(CASE WHEN NCR.RequestStatus = 1 THEN 1 ELSE 0 END), 
		NCR.PaymentRequested, 
		(CASE WHEN NCR.RequestStatus = 2 THEN NCR.PaymentRequested 
				WHEN NCR.RequestStatus = 3 THEN NCR.ApprovedAmount ELSE NULL END),
		(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = NCR.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = NCR.CreatedBy)),
		NonCriticalRepairsRequestID as Id,
		NCR.TaskId, null, null,null,
		(CASE WHEN NCR.RequestStatus = 1 THEN 'Pending'
				WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 4 THEN 'Accepted'
				WHEN NCR.RequestStatus = 3 THEN 'Accepted with Changes'
				WHEN NCR.RequestStatus = 5 THEN 'Denied'
				ELSE NULL END),
		(CASE WHEN  NCR.IsLenderDelegate = 1 THEN 'Lender Delegated'
				WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 5 THEN 'Account Executive'
				WHEN NCR.RequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END), 
        null,null,
		null,
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE UserName = task.AssignedTo) as HUDPM
		FROM NonCriticalRepairsRequests NCR
		INNER JOIN ProjectInfo PF
			ON NCR.FHANumber = PF.FHANumber
        INNER JOIN [$(TaskDB)].dbo.task Task
			on NCR.Taskid=Task.taskid
		INNER JOIN @SelectedLendersList lenders
			ON lenders.LenderUserId = NCR.CreatedBy
		INNER JOIN User_WLM_PM UWM
			ON PF.HUD_Project_Manager_ID = UWM.HUD_Project_Manager_ID
	--	AND PF.HUD_Project_Manager_ID IN (SELECT HUD_Project_Manager_ID FROM @AeList)
		INNER JOIN HUD_Project_Manager PM
			ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
		INNER JOIN HUD_WorkLoad_Manager WLM
		ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
		LEFT JOIN #TaskReAssignment TRA
			ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = NCR.TaskId 
		WHERE --NCR.SubmittedDate BETWEEN @StartDate AND @EndDate
            ((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  NCR.SubmittedDate>= @StartDate and NCR.SubmittedDate< DATEADD(day,1,@EndDate))
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND NCR.SubmittedDate <= GETUTCDATE()))
		AND TRA.FromAeId is null and uwm.Deleted_Ind is Null-- task re assignments exclude

		-- Task re assgingment items include
		INSERT INTO #LenderPAMReport
		SELECT NCR.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, from_wlm.HUD_WorkLoad_Manager_Name,
                                            'NCR','Servicer Comments: '+NCR.servicerremarks, NCR.SubmittedDate, 
					(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 4 OR NCR.RequestStatus = 5 THEN isnull(NCR.ApprovedDate,NCR.ModifiedOn) ELSE NULL END),
		(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 THEN DATEDIFF(DAY,TRA.CreatedOn,NCR.ApprovedDate) ELSE DATEDIFF(DAY,TRA.CreatedOn,GETUTCDATE()) END), 
		(CASE WHEN NCR.RequestStatus = 1 THEN 1 ELSE 0 END), 
		NCR.PaymentRequested, 
		(CASE WHEN NCR.RequestStatus = 2 THEN NCR.PaymentRequested 
				WHEN NCR.RequestStatus = 3 THEN NCR.ApprovedAmount ELSE NULL END),
		(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = NCR.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = NCR.CreatedBy)),
		NonCriticalRepairsRequestID as Id,
		NCR.TaskId,
		TRA.ReAssignedTo,
		TRA.CreatedOn,
		TRA.ReAssignedBy,
		(CASE WHEN NCR.RequestStatus = 1 THEN 'Pending'
				WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 4 THEN 'Accepted'
				WHEN NCR.RequestStatus = 3 THEN 'Accepted with Changes'
				WHEN NCR.RequestStatus = 5 THEN 'Denied'
				ELSE NULL END),
		(CASE WHEN  NCR.IsLenderDelegate = 1 THEN 'Lender Delegated'
				WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 5 THEN 'Account Executive'
				WHEN NCR.RequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END),
				null ,null,
				(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as ReAssignedFrom ,
				(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom)  as HUDPM  
		FROM #TaskReAssignment TRA 
		INNER JOIN  NonCriticalRepairsRequests NCR
			ON TRA.TaskId = NCR.TaskId
		INNER JOIN ProjectInfo PF
			ON NCR.FhaNumber  = PF.FhaNumber
		INNER JOIN @SelectedLendersList lenders
			ON lenders.LenderUserId = NCR.CreatedBy  
			JOIN HUD_Project_Manager PM
			ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			JOIN HUD_WorkLoad_Manager  from_wlm
			ON TRA.From_HUD_WorkLoad_Manager_ID = from_wlm.HUD_WorkLoad_Manager_ID
		WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
                                            (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE())

	END

	IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 2 )  
	BEGIN
		INSERT INTO #LenderPAMReport
		SELECT NX.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, WLM.HUD_WorkLoad_Manager_Name,
                                            'NCRExtension',(CASE WHEN NX.ExtensionRequestStatus = 2 then '.' else 'AE Comments: '+ Task.notes End), NX.CreatedOn, 
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE NX.ModifiedOn END),
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE DATEDIFF(DAY,NX.CreatedOn,NX.ModifiedOn) END), 
		CASE WHEN NX.ExtensionRequestStatus = 1 THEN 1 ELSE 0 END,
		NULL, NULL,
		(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = NX.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = NX.CreatedBy)),
		NX.NonCriticalRequestExtensionId as Id,
		NX.TaskId, null, null,null,
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN 'Pending'
				WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 4 THEN 'Accepted'
				WHEN NX.ExtensionRequestStatus = 3 THEN 'Denied'
				ELSE NULL END),
		(CASE WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 3 THEN 'Account Executive'
				WHEN NX.ExtensionRequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END),
		null,null,
		null,
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = Task.AssignedTo)	  
		FROM NonCriticalRequestExtensions NX
		INNER JOIN ProjectInfo PF
			ON NX.FHANumber = PF.FHANumber
        INNER JOIN [$(TaskDB)].dbo.task Task
			on NX.Taskid=Task.taskid
		INNER JOIN @SelectedLendersList lenders
			ON lenders.LenderUserId = NX.CreatedBy
		INNER JOIN User_WLM_PM UWM
			ON PF.HUD_Project_Manager_ID = UWM.HUD_Project_Manager_ID
	--	AND PF.HUD_Project_Manager_ID IN (SELECT HUD_Project_Manager_ID FROM @AeList)
		INNER JOIN HUD_Project_Manager PM
			ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
		INNER JOIN HUD_WorkLoad_Manager WLM
			ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
		LEFT JOIN #TaskReAssignment TRA
			ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = NX.TaskId 
		WHERE --NCR.SubmittedDate BETWEEN @StartDate AND @EndDate
                                            (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   NX.CreatedOn>= @StartDate and NX.CreatedOn< DATEADD(day,1,@EndDate))
		OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND NX.CreatedOn <= GETUTCDATE())
		AND TRA.FromAeId is null and uwm.Deleted_Ind is Null-- task re assignments exclude

		INSERT INTO #LenderPAMReport
		SELECT NX.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, from_wlm.HUD_WorkLoad_Manager_Name,
                                            'NCRExtension','AE Comments: '+Task.notes, NX.CreatedOn, 
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE NX.ModifiedOn END),
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE DATEDIFF(DAY,TRA.CreatedOn,NX.ModifiedOn) END), 
		CASE WHEN NX.ExtensionRequestStatus = 1 THEN 1 ELSE 0 END,
		NULL, NULL,
		(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = NX.CreatedBy),
		(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = NX.CreatedBy)),
		NX.NonCriticalRequestExtensionId as Id,
		NX.TaskId,
		TRA.ReAssignedTo,
		TRA.CreatedOn,
		TRA.ReAssignedBy,
		(CASE WHEN NX.ExtensionRequestStatus = 1 THEN 'Pending'
				WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 4 THEN 'Accepted'
				WHEN NX.ExtensionRequestStatus = 3 THEN 'Denied'
				ELSE NULL END),
		(CASE WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 3 THEN 'Account Executive'
				WHEN NX.ExtensionRequestStatus = 4 THEN 'Auto Approved'
				ELSE NULL END),
		null,null	,
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as ReAssignedFrom  ,
		(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as HUDPM          
		FROM #TaskReAssignment TRA 
		INNER JOIN  NonCriticalRequestExtensions NX
			ON TRA.TaskId = NX.TaskId
		INNER JOIN ProjectInfo PF
			ON NX.FhaNumber  = PF.FhaNumber
        INNER JOIN [$(TaskDB)].dbo.task Task
			on NX.Taskid=Task.taskid
		INNER JOIN @SelectedLendersList lenders
			ON lenders.LenderUserId = NX.CreatedBy 
		JOIN HUD_Project_Manager PM
			ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
		JOIN HUD_WorkLoad_Manager  from_wlm
			ON TRA.From_HUD_WorkLoad_Manager_ID  = from_wlm.HUD_WorkLoad_Manager_ID
		WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
              (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE())

	END

	IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId in (select PamProjectActionId from PAMProjectActionTypes where PamProjectActionId  > 2 ) ) 
		BEGIN
		 -- select * from HCP_Project_Action
			INSERT INTO #LenderPAMReport
			SELECT PAF.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, WLM.HUD_WorkLoad_Manager_Name,
			HPA.ProjectActionName,
		(CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
				  WHEN	 PAF.RequestStatus = 7 THEN CONCAT( isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
			  WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: ' + addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
			PAF.CreatedOn, 
			(CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
			(CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
			CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
			NULL, NULL,
			(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
			(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = PAF.CreatedBy)),
			PAF.ProjectActionFormId as Id,
			PAF.MytaskId as TaskId,
			null,
			null,
			null,
			(CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
				  WHEN PAF.RequestStatus = 2  THEN 'Accepted'
				  WHEN PAF.RequestStatus = 5 THEN 'Denied'
				  WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
				 ELSE NULL END),
			(CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
				  ELSE NULL END),
		    addl.addlcount,
			 null,
			 null,
			 TRA.ReAssignedFrom
			FROM OPAForm PAF
			INNER JOIN ProjectInfo PF
				ON PAF.FHANumber = PF.FHANumber
            LEFT JOIN 
			 (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
				FROM  AdditionalInformation 
				 GROUP BY ParentFormId)	addl
				ON addl.ParentFormId = PAF.ProjectActionFormId 
		   LEFT JOIN 
			 (SELECT 
				 ParentFormId,	
				 CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
				   ELSE null
				  END as cmt
				FROM  AdditionalInformation 
				where AEComment is not null or LenderComment is not null
				 GROUP BY ParentFormId)	addlCom
				ON addlCom.ParentFormId = PAF.ProjectActionFormId 

			INNER JOIN @SelectedLendersList lenders
				ON lenders.LenderUserId = PAF.CreatedBy
			INNER JOIN User_WLM_PM UWM
				ON PF.HUD_Project_Manager_ID = UWM.HUD_Project_Manager_ID
		--	AND PF.HUD_Project_Manager_ID IN (SELECT HUD_Project_Manager_ID FROM @AeList)
			INNER JOIN HUD_Project_Manager PM
				ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			INNER JOIN HUD_WorkLoad_Manager WLM
				ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			INNER JOIN HCP_Project_Action HPA 
				ON HPA.ProjectActionID = PAF.ProjectActionTypeId
			INNER JOIN PAMProjectActionTypes PAMType
				ON PAMType.Name = HPA.ProjectActionName
			INNER JOIN @SelectedProjectActions SPA
				ON SPA.PamProjectActionId = PAMType.PamProjectActionId
			LEFT JOIN #TaskReAssignment TRA
				ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = PAF.MytaskId  
			WHERE --NCR.SubmittedDate BETWEEN @StartDate AND @EndDate
			((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  PAF.CreatedOn>= @StartDate and PAF.CreatedOn< DATEADD(day,1,@EndDate))
			OR
			((@StartDate IS NULL AND @EndDate IS NULL) AND PAF.CreatedOn <= GETUTCDATE()))
			AND TRA.FromAeId is null and uwm.Deleted_Ind is Null-- task re assignments exclude

			INSERT INTO #LenderPAMReport
			SELECT PAF.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, from_wlm.HUD_WorkLoad_Manager_Name,
			HPA.ProjectActionName,
				(CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
				  WHEN	 PAF.RequestStatus = 7 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
			  WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
			PAF.CreatedOn, 
				(CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
			(CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
			CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
			NULL, NULL,
			(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
			(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = PAF.CreatedBy)),
			PAF.ProjectActionFormId as Id,
			PAF.MytaskId as TaskId,
			TRA.ReAssignedTo,
			TRA.CreatedOn,
			TRA.ReAssignedBy,
				(CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
				  WHEN PAF.RequestStatus = 2  THEN 'Accepted'
				  WHEN PAF.RequestStatus = 5 THEN 'Denied'
				  WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
				 ELSE NULL END),
			(CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
				  ELSE NULL END),
		    addl.addlcount,
			null,
			(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as ReAssignedFrom ,
			(SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = TRA.ReAssignedFrom) as HUDPM
			FROM #TaskReAssignment TRA 
			INNER JOIN  OPAForm  PAF
				ON TRA.TaskId = PAF.MytaskId 
			INNER JOIN ProjectInfo PF
				ON PAF.FhaNumber  = PF.FhaNumber
				  LEFT JOIN 
			 (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
				FROM  AdditionalInformation 
				 GROUP BY ParentFormId)	addl
				ON addl.ParentFormId = PAF.ProjectActionFormId 
				  LEFT JOIN 
			 (SELECT 
				 ParentFormId,	
				 CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
				   ELSE null
				  END as cmt
				FROM  AdditionalInformation 
				where AEComment is not null or LenderComment is not null
				 GROUP BY ParentFormId)	addlCom
				ON addlCom.ParentFormId = PAF.ProjectActionFormId 
			INNER JOIN @SelectedLendersList lenders
				ON lenders.LenderUserId = PAF.CreatedBy
			 JOIN HUD_Project_Manager PM
				ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			   JOIN HUD_WorkLoad_Manager  from_wlm
				ON TRA.From_HUD_WorkLoad_Manager_ID  = from_wlm.HUD_WorkLoad_Manager_ID
			INNER JOIN HCP_Project_Action HPA 
				ON HPA.ProjectActionID = PAF.ProjectActionTypeId
			WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
                (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
			 OR
			((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE())

			--Gets Parent child information
			 -- select * from HCP_Project_Action
			INSERT INTO #LenderPAMReport
			SELECT PAF.FHANumber, PF.PropertyID, PF.ProjectName, PM.HUD_Project_Manager_Name, WLM.HUD_WorkLoad_Manager_Name,
			HPA.ProjectActionName,
		(CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
				  WHEN	 PAF.RequestStatus = 7 THEN CONCAT( isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
			  WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: ' + addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
			PAF.CreatedOn, 
			(CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
			(CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
			CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
			NULL, NULL,
			(SELECT FirstName+''+LastName as LenderUserName FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
			(select [RoleName] from [dbo].[webpages_Roles] where [RoleId] = (select [RoleId] from [dbo].[webpages_UsersInRoles] where [UserId] = PAF.CreatedBy)),
			PAF.ProjectActionFormId as Id,
			PAF.MytaskId as TaskId,
			null,
			null,
			null,
			(CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
				  WHEN PAF.RequestStatus = 2  THEN 'Accepted'
				  WHEN PAF.RequestStatus = 5 THEN 'Denied'
				  WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
				 ELSE NULL END),
			(CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
				  ELSE NULL END),
		    addl.addlcount,
			 parentChildTask.ChildTaskInstanceId,
			 null,
			 (SELECT FirstName+''+LastName FROM HCP_Authentication WHERE Username = tra.ReAssignedFrom) as HUDPM
			FROM OPAForm PAF
			join (
			 select *
			 from [$(TaskDB)].dbo.ParentChildTask pt, [$(TaskDB)].dbo.task t
			 where pt.ChildTaskInstanceId = t.TaskInstanceId and t.TaskStepId=16) as parentChildTask on PAF.TaskInstanceId = parentChildTask.ParentTaskInstanceId
			INNER JOIN ProjectInfo PF
				ON PAF.FHANumber = PF.FHANumber
            LEFT JOIN 
			 (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
				FROM  AdditionalInformation 
				 GROUP BY ParentFormId)	addl
				ON addl.ParentFormId = PAF.ProjectActionFormId 
		   LEFT JOIN 
			 (SELECT 
				 ParentFormId,	
				 CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
				   ELSE null
				  END as cmt
				FROM  AdditionalInformation 
				where AEComment is not null or LenderComment is not null
				 GROUP BY ParentFormId)	addlCom
				ON addlCom.ParentFormId = PAF.ProjectActionFormId 

			INNER JOIN @SelectedLendersList lenders
				ON lenders.LenderUserId = PAF.CreatedBy
			INNER JOIN User_WLM_PM UWM
				ON PF.HUD_Project_Manager_ID = UWM.HUD_Project_Manager_ID
		--	AND PF.HUD_Project_Manager_ID IN (SELECT HUD_Project_Manager_ID FROM @AeList)
			INNER JOIN HUD_Project_Manager PM
				ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			INNER JOIN HUD_WorkLoad_Manager WLM
				ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
			INNER JOIN HCP_Project_Action HPA 
				ON HPA.ProjectActionID = PAF.ProjectActionTypeId
			INNER JOIN PAMProjectActionTypes PAMType
				ON PAMType.Name = HPA.ProjectActionName
			INNER JOIN @SelectedProjectActions SPA
				ON SPA.PamProjectActionId = PAMType.PamProjectActionId
			LEFT JOIN #TaskReAssignment TRA
				ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = PAF.MytaskId  
			WHERE --NCR.SubmittedDate BETWEEN @StartDate AND @EndDate
			((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  PAF.CreatedOn>= @StartDate and PAF.CreatedOn< DATEADD(day,1,@EndDate))
			OR
			((@StartDate IS NULL AND @EndDate IS NULL) AND PAF.CreatedOn <= GETUTCDATE()))
			AND TRA.FromAeId is null and uwm.Deleted_Ind is Null

		END

	SELECT DISTINCT FHANumber ,
		PropertyId , 
		PropertyName , 
		HudProjectManagerName ,
		HudWorkloadManagerName ,
		(case when ParentChildInstanceId is not null then ProjectActionName + '(RAI)' else ProjectActionName end) as ProjectActionName,
		Comment,
		ProjectActionSubmitDate ,
		ProjectActionCompletionDate ,
		ProjectActionDaysToComplete ,
		IsProjectActionOpen ,
		RequestedAmount,
		ApprovedAmount,
		LenderUserName ,
		UserRoleName ,
		(case when ParentChildInstanceId is not null then ParentChildInstanceId else id end) as Id ,						
		TaskId ,
		ReAssignedTo,
		ReAssignedDate,
		ReAssignedBy ,
		RequestStatus,
		ProcessedBy ,
		AdditionalInfoCount,
		ParentChildInstanceId,
		ReAssignedFrom,
		HUDPM,
		(case when ParentChildInstanceId is not null then 'Yes' else 'NA' end) as RAI  
		into #ProjectCompletedDateNull
	FROM #LenderPAMReport 
	where ( @Status is null or IsProjectActionOpen = @Status) and ProjectActionCompletionDate is null
	ORDER BY ProjectActionCompletionDate asc,ProjectActionSubmitDate
	--ORDER BY ProjectActionSubmitDate asc

	SELECT DISTINCT FHANumber ,
		PropertyId , 
		PropertyName , 
		HudProjectManagerName ,
		HudWorkloadManagerName ,
		(case when ParentChildInstanceId is not null then ProjectActionName + '(RAI)' else ProjectActionName end) as ProjectActionName,
		Comment,
		ProjectActionSubmitDate ,
		ProjectActionCompletionDate ,
		ProjectActionDaysToComplete ,
		IsProjectActionOpen ,
		RequestedAmount,
		ApprovedAmount,
		LenderUserName ,
		UserRoleName ,
		(case when ParentChildInstanceId is not null then ParentChildInstanceId else id end) as Id ,						
		TaskId ,
		ReAssignedTo,
		ReAssignedDate,
		ReAssignedBy ,
		RequestStatus,
		ProcessedBy ,
		AdditionalInfoCount,
		ParentChildInstanceId,
		ReAssignedFrom,
		HUDPM,
		(case when ParentChildInstanceId is not null then 'Yes' else 'NA' end) as RAI  
		into #ProjectCompletedDateNotNull
	FROM #LenderPAMReport 
	where ( @Status is null or IsProjectActionOpen = @Status) and (ProjectActionCompletionDate is not null) 
	--ORDER BY ProjectActionSubmitDate asc
	ORDER BY ProjectActionCompletionDate desc

	update #ProjectCompletedDateNotNull
	set ProjectActionDaysToComplete = DATEDIFF( day ,ProjectActionSubmitDate,ProjectActionCompletionDate);


			WITH cte AS 
				(
				select * from #ProjectCompletedDateNull
				),

			cte2 AS 
			(
			select * from #ProjectCompletedDateNotNull
			)

			SELECT ROW_NUMBER() OVER (ORDER BY cte.ProjectActionCompletionDate,ProjectActionSubmitDate) AS ID, * FROM cte
			UNION ALL
			SELECT ROW_NUMBER() OVER (ORDER BY cte2.ProjectActionCompletionDate desc) AS ID,* FROM cte2 


	--select * from #ProjectCompletedDateNull
	--union all
	--select * from #ProjectCompletedDateNotNull

END


GO


