﻿
CREATE PROCEDURE [dbo].[usp_HCP_AuthenticateInitial_LogIn]
(
@UserName varchar(50)
)
AS

Declare @IsotherAe bit
set @IsotherAe=0
  select top 1  @IsotherAe=   p.onbehalfofby    FROM [$(DatabaseName)].[dbo].HUD_Project_Manager p
  
  join  [$(DatabaseName)].[dbo].[HCP_Authentication] u on p.addressid=u.addressid 
  where u.username=@UserName


SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.PreferredTimeZone,
	MR.RoleName, 
	ISNULL(UL.Lender_ID, LF.LenderID) AS LenderID,
	UL.ServicerID,
	A.UserID,
	ISNULL(@IsotherAe, 0) AS IsotherAe
FROM [$(DatabaseName)].[dbo].[HCP_Authentication] A
INNER JOIN [$(DatabaseName)].[dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [$(DatabaseName)].[dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
LEFT JOIN [$(DatabaseName)].[dbo].[User_Lender] UL
ON A.UserID = UL.User_ID
LEFT JOIN [$(DatabaseName)].[dbo].Lender_FHANumber LF
ON UL.FHANumber = LF.FHANumber

WHERE A.UserName = @UserName

