﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetLenderUsersByAeUserId]
(
@UserId int
)
AS

SELECT DISTINCT
	A.UserName, 
	A.FirstName,
	A.LastName,
	A.PreferredTimeZone,
	MR.RoleName, 
	L.LenderID,
	LI.Lender_Name,
	UL.ServicerID,
	A.UserID
FROM [dbo].[HCP_Authentication] A
INNER JOIN [dbo].[webpages_UsersInRoles] UIR
ON A.UserID = UIR.UserId
INNER JOIN [dbo].[webpages_Roles] MR
ON UIR.RoleId = MR.RoleId
INNER JOIN [dbo].[User_Lender] UL
ON A.UserID = UL.User_ID
INNER JOIN
(SELECT DISTINCT
	LF.LenderID
FROM [dbo].[User_Lender] UL
INNER JOIN [dbo].[Lender_FHANumber] LF
ON UL.FHANumber = LF.FHANumber AND LF.FHA_EndDate IS NULL
WHERE UL.User_ID = @UserId) L
ON L.LenderID = UL.Lender_ID
INNER JOIN [dbo].[LenderInfo] LI
ON L.LenderID = LI.LenderID
WHERE MR.RoleName = 'LenderAccountManager' OR MR.RoleName = 'BackupAccountManager' OR MR.RoleName = 'LenderAccountRepresentative' OR MR.RoleName = 'Servicer'
AND (A.Deleted_Ind = 0 OR A.Deleted_Ind IS NULL)
ORDER BY LI.Lender_Name, A.FirstName


GO

