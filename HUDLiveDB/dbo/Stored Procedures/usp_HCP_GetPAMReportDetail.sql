﻿CREATE PROCEDURE [dbo].[usp_HCP_GetPAMReportDetail]
(
@AEIdList varchar(MAX),
@StartDate datetime,
@EndDate datetime,
@Status int,
@ProjectAction varchar(100),
@UserId int,
@UserRole varchar(100),
@ISOUIdList varchar(MAX)
)
AS
/**
06/19/2018	rganapathy			Corrected Task Reassignment for OPA
06/20/2018	rchitturi			Corrected Task for R4R, NCR, NCR - EX
06/20/2018  rganapathy			Corrected Parent and Child task records for OPA
06/21/2018  rchitturi			Corrected NCRE - EX
07/05/2018  rchitturi			ISOU TaskRessignment - Data issue (Removed bad data)
07/05/2018  rchitturi			ISOU TaskRessignment - Display ISOU PAM Report
07/15/2018  rchitturi			Reassigned From - all AE reassignment and NCRE , NCRE-EX ISOU reassignment
07/18/2018	rchitturi           ReassignedFrom - HUD Project Manager , Current AE
07/26/2018	rganapathy			Task#3871 Corrected R4R queries to display correct counts for Default and Select all search
07/30/2018  rganapathy			Task#3871 Corrected NCR queries to display correct counts for Default and Select all search
								+Default search AE list to match Select All AE list
08/01/2018  rganapathy			Task#3898 Corrected R4R/NCR/NCR Ext/OPA to display the selected AE/WLM/ISOU records
10/04/2018  rchitturi           Task #3694 PAM Report (OPA) Filters display Fix

**/
BEGIN
     DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10) 
  if (@StartDate is not null and @EndDate is null)   set @EndDate =   GETUTCDATE()
  
  if (@StartDate is null and @EndDate is not null)   set @StartDate = '01-01-2015'
 

 DECLARE @AeList TABLE (HUD_Project_Manager_ID INT) 
 DECLARE @SelectedProjectActions TABLE (PamProjectActionId INT) 
 DECLARE @ISOUList TABLE (InternalSpecialOptionUserId INT) 
 
 
 if OBJECT_ID('TEMPDB..#CompletedDateNull') is not null
 drop table #CompletedDateNull;

 if OBJECT_ID('TEMPDB..#CompletedDateNotNull') is not null
 drop table #CompletedDateNotNull;
 if OBJECT_ID('TEMPDB..#PAMReport') is not null
 drop table #PAMReport;
 if OBJECT_ID('TEMPDB..#TaskReAssignmentISOU') is not null
 drop table #TaskReAssignmentISOU;
 if OBJECT_ID('TEMPDB..#TaskReAssignment') is not null
 drop table #TaskReAssignment
  
 IF @ProjectAction IS NULL OR @ProjectAction = ''
  INSERT INTO @SelectedProjectActions  SELECT PamProjectActionId FROM  PamProjectActionTypes 
 ELSE 
  INSERT INTO @SelectedProjectActions SELECT * FROM dbo.fn_StringSplitterToInt(@ProjectAction)

 IF (@AEIdList IS NULL OR @AEIdList = '') and (@ISOUIdList IS NULL OR @ISOUIdList = '')
  INSERT INTO @AeList SELECT distinct pm.HUD_Project_Manager_ID FROM HUD_Project_Manager pm join [dbo].[HUD_WorkLoad_Manager_Portfolio_Manager] wpm on pm.HUD_Project_Manager_id = wpm.HUD_Project_Manager_id
  
 ELSE 
  INSERT INTO @AeList SELECT * FROM dbo.fn_StringSplitterToInt(@AEIdList)
  DECLARE @ReAssignAeList TABLE (HUD_Project_Manager_ID INT)

  IF @ISOUIdList IS NULL OR @ISOUIdList = ''
		INSERT INTO @ISOUList SELECT DISTINCT InternalSpecialOptionUserId FROM HUD_WorkloadManager_InternalSpecialOptionUser
	ELSE
	INSERT INTO @ISOUList SELECT * FROM dbo.fn_StringSplitterToInt(@ISOUIDLIST)  
 --drop table #TaskReAssignment
 CREATE TABLE #TaskReAssignment
 (
  FromAeId int,
  ToAeid int,
  TaskId int,
  CreatedOn Datetime NULL,
  From_HUD_Project_Manager_ID int null,
  To_HUD_Project_Manager_ID  int null,
  From_HUD_WorkLoad_Manager_ID int null,
  To_HUD_WorkLoad_Manager_ID int null,
  ReAssignedTo VARCHAR(100) null,
  ReAssignedBy VARCHAR(100) null,
  TaskInstanceId uniqueidentifier null,
  ReAssignedFrom VARCHAR(100) null,
  HUDPM VARCHAR(100) null
 );

 CREATE TABLE #TaskReAssignmentISOU
 (
  FromAeId int,
  ToAeid int,
  TaskId int,
  CreatedOn Datetime NULL,
  From_HUD_Project_Manager_ID int null,
  To_HUD_Project_Manager_ID  int null,
  From_HUD_WorkLoad_Manager_ID int null,
  To_HUD_WorkLoad_Manager_ID int null,
  ReAssignedTo VARCHAR(100) null,
  ReAssignedBy VARCHAR(100) null,
  TaskInstanceId uniqueidentifier null,
  ReAssignedFrom VARCHAR(100) null,
  HUDPM VARCHAR(100) null
 );
 
 --2221 3271 6423 2016-03-12 16:33:21.857 5
 -- select * from User_WLM_PM where userid = 2221--3271
 INSERT INTO #TaskReAssignment
 SELECT 
 b.UserID,
 a.userid,
 t.TaskId,
 tassign.CreatedOn,
 from_pmger.HUD_Project_Manager_ID,
 to_pmger.HUD_Project_Manager_ID,
 null,
 null,
 a.FirstName+' '+a.LastName as ReAssginedTo,
 c.FirstName+' '+c.LastName  as ReAssignedBy ,
 t.TaskInstanceId,
 tassign.FromAE,
   t1.AssignedTo  as HUDPM        
   FROM [$(TaskDB)].[dbo].[TaskReAssignments] tassign
            JOIN    [$(TaskDB)].dbo.[Task] t
         ON tassign.TaskInstanceId = t.TaskInstanceId
left JOIN    [$(TaskDB)].dbo.[Task] t1  ON (tassign.TaskInstanceId = t1.TaskInstanceId and t1.SequenceId = 0)
     left Join [$(TaskDB)].dbo.ParentChildTask ParentChildTask
     on tassign.[TaskInstanceId]= ParentChildTask.ChildTaskInstanceId 
      INNER JOIN 
     (
      SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
      FROM [$(TaskDB)].[dbo].[Task]      
      GROUP BY TaskInstanceId
     ) TMAX
     ON t.TaskInstanceId = TMAX.TaskInstanceId and t.SequenceId = TMAX.maxSeqId       
       JOIN  dbo.HCP_Authentication a
      ON a.UserName = tassign.ReAssignedTo
     JOIN  dbo.HCP_Authentication b
      ON b.UserName = tassign.FromAE 
    JOIN  dbo.HCP_Authentication c
      ON c.userid = tassign.CreatedBy   
     JOIN User_WLM_PM from_pm_ulm 
      ON from_pm_ulm.userid = a.UserID-- and ulm.Deleted_Ind = 0
     JOIN HUD_Project_Manager to_pmger 
      ON to_pmger.HUD_Project_Manager_ID = from_pm_ulm.HUD_Project_Manager_ID
      JOIN User_WLM_PM to_pm_ulm 
      ON to_pm_ulm.userid = b.UserID-- and ulm.Deleted_Ind = 0
      JOIN  HUD_Project_Manager from_pmger 
      ON from_pmger.HUD_Project_Manager_ID = to_pm_ulm.HUD_Project_Manager_ID
    WHERE t.IsReassigned = 1  and  (tassign.Deleted_Ind = 0 or tassign.Deleted_Ind is null)
INSERT INTO #TaskReAssignmentISOU
SELECT 
b.UserID,
a.userid,
t.TaskId,
tassign.CreatedOn,
null as HUD_Project_Manager_ID,
null as HUD_Project_Manager_ID,
c.UserID as ReAssignedUserID,
a.UserID as ReAssignedByUserID,
a.FirstName+' '+a.LastName+'*' as ReAssginedTo,
c.FirstName+' '+c.LastName  as ReAssignedBy ,
t.TaskInstanceId,
tassign.FromAE,
t1.AssignedTo
	FROM [$(TaskDB)].[dbo].[TaskReAssignments] tassign
	   left JOIN    [$(TaskDB)].[dbo].[Task]  t
				ON tassign.TaskInstanceId = t.TaskInstanceId
left JOIN    [$(TaskDB)].[dbo].[Task]  t1  ON (tassign.TaskInstanceId = t1.TaskInstanceId and t1.SequenceId = 0)
			left Join [$(TaskDB)].dbo.ParentChildTask ParentChildTask
			on tassign.[TaskInstanceId]= ParentChildTask.ChildTaskInstanceId 
			INNER JOIN 
			(
				SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
				FROM [$(TaskDB)].[dbo].[Task]						
				GROUP BY TaskInstanceId
			) TMAX
			ON t.TaskInstanceId = TMAX.TaskInstanceId and t.SequenceId = TMAX.maxSeqId  					
					JOIN  dbo.HCP_Authentication a
				ON a.UserName = tassign.ReAssignedTo
			JOIN  dbo.HCP_Authentication b
				ON b.UserName = tassign.FromAE 
		JOIN  dbo.HCP_Authentication c
				ON c.userid = tassign.CreatedBy   
			JOIN User_WLM_PM from_pm_ulm 
				ON from_pm_ulm.userid = a.UserID
			join dbo.HUD_WorkloadManager_InternalSpecialOptionUser wmlisou
			ON  wmlisou.InternalSpecialOptionUserId = a.UserID  -- corrected
    WHERE t.IsReassigned = 1  and  (tassign.Deleted_Ind = 0 or tassign.Deleted_Ind is null)

     --  update from workload manager
     UPDATE t set From_HUD_WorkLoad_Manager_ID = pinfo.HUD_WorkLoad_Manager_ID
      from  #TaskReAssignment t
       JOIN ProjectInfo pinfo on pinfo.HUD_Project_Manager_ID = t.From_HUD_Project_Manager_ID
    --- Update to work load manager
     UPDATE t set To_HUD_WorkLoad_Manager_ID = pinfo.HUD_WorkLoad_Manager_ID
      from  #TaskReAssignment t
       JOIN ProjectInfo pinfo on pinfo.HUD_Project_Manager_ID = t.To_HUD_Project_Manager_ID


 CREATE TABLE #PAMReport
 (
  FHANumber NVARCHAR(15), 
  PropertyId INT, 
  PropertyName NVARCHAR(100), 
  HudProjectManagerName NVARCHAR(100),
  HudWorkloadManagerName NVARCHAR(100),
  ProjectActionName NVARCHAR(100),
  Comment NVARCHAR(2000),
  ProjectActionSubmitDate DATETIME,
  ProjectActionCompletionDate DATETIME,
  ProjectActionDaysToComplete INT,
  IsProjectActionOpen BIT,
  RequestedAmount DECIMAL(19,2),
  ApprovedAmount DECIMAL(19,2),
  LenderUserName NVARCHAR(100),
  LenderEmail NVARCHAR(100),
  Id uniqueidentifier,
  TaskId int,
  ReAssignedTo VARCHAR(100) null,
  ReAssignedDate DATETIME null,
  ReAssignedBy VARCHAR(100) null,
  RequestStatus VARCHAR(100) null,
  ProcessedBy VARCHAR(100) null,
  AdditionalInfoCount int null,
  ParentChildInstanceId  uniqueidentifier null,
  ReAssignedFrom VARCHAR(100) null,
  HUDPM VARCHAR(100) null
 );
 if(@UserRole != 'InternalSpecialOptionUser')
 Begin 
 IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 1 )  
  BEGIN
  -- Display all R4R without reassigned Tasks
   INSERT INTO #PAMReport
   SELECT R4R.FHANumber
 , R4R.PropertyId, R4R.PropertyName
 ,case 
		when (task.AssignedBy like '%hud.gov%' or task.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedBy)
		when (task.AssignedTo like '%hud.gov%' or task.AssignedTo = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedTo)
	end as HudProjectManagerName
 ,WLM.HUD_WorkLoad_Manager_Name,
   'R4R',
   (CASE WHEN R4R.RequestStatus = 1 OR R4R.RequestStatus = 4 THEN 'Servicer Comments: '+R4R.ServicerRemarks
    WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 5 THEN IsNull('Servicer Comments: '+R4R.ServicerRemarks+@NewLineChar,'')+'' + IsNull('HUD Remarks: '+R4R.HudRemarks+@NewLineChar,'')+''+IsNull('Additional Remarks: '+R4R.AdditionalRemarks,'') ELSE NULL END), 
   R4R.ServicerSubmissionDate, 
   (CASE WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 OR R4R.RequestStatus = 5 THEN R4R.ModifiedOn ELSE NULL END),
   (CASE WHEN R4R.RequestStatus = 2 THEN DATEDIFF(DAY,R4R.ServicerSubmissionDate,R4R.ModifiedOn) ELSE DATEDIFF(DAY,R4R.ServicerSubmissionDate,GETUTCDATE()) END), 
   (CASE WHEN R4R.RequestStatus = 1 THEN 1 ELSE 0 END), 
   R4R.TotalRequestedAmount, 
   (CASE WHEN (R4R.IsLenderDelegate =1 AND R4R.RequestStatus = 4) THEN R4R.TotalApprovedAmount
      WHEN R4R.RequestStatus = 4 THEN R4R.TotalRequestedAmount 
      WHEN R4R.RequestStatus = 2 THEN R4R.TotalApprovedAmount
      ELSE NULL END),
   l.Lender_Name,
   (SELECT UserName FROM HCP_Authentication WHERE UserID = R4R.CreatedBy),
   R4RId as Id, R4R.TaskId, 
   null, 
   null,
   null,
   (CASE 
      WHEN R4R.RequestStatus = 1 THEN 'Pending'
      WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN 'Accepted'
      WHEN R4R.RequestStatus = 5 THEN 'Denied'

     ELSE NULL END),
   (CASE WHEN R4R.IsLenderDelegate = 1 THEN 'Lender Delegated'
      WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 5 THEN 'Account Executive'
      WHEN R4R.RequestStatus = 4 THEN 'Auto Approved'
      ELSE NULL END),
      null ,null,
	  null,
	  case 
		when (task.AssignedBy like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedBy)
		when (task.AssignedTo like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedTo)
	end
	 -- (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task1.AssignedTo)
    from [$(DatabaseName)].dbo.Reserve_For_Replacement_Form_Data r4r 
inner join 

(SELECT distinct TaskInstanceId, max(SequenceId) as seqId,max(taskid)  as taskid, IsReassigned,assignedto,assignedby FROM
		(SELECT *, CONVERT(varchar(MAX), DataStore1) as [XMLDataString] FROM [$(TaskDB)].dbo.task) x
	WHERE [XMLDataString] like '%ReserveForReplacementFormModel%' 
	and (IsReassigned is null or IsReassigned = 0) 
	group by TaskInstanceId,assignedto,assignedby,IsReassigned) task
	on r4r.TaskId = task.taskid
   INNER JOIN ProjectInfo PF ON R4R.FHANumber = PF.FHANumber and R4R.PropertyId = pf.PropertyID 
   join User_Lender u on u.User_ID = r4r.CreatedBy
	inner join [LenderInfo] l on u.Lender_ID = l.LenderID    
   left join (select a.UserName,a.userid,pm.HUD_Project_Manager_ID from HUD_Project_Manager pm
                                    join Address ad on pm.addressid = ad.AddressId
                                    join HCP_Authentication a on a.Username = ad.Email
									  where  pm.HUD_Project_Manager_ID in 
									 ( SELECT HUD_Project_Manager_ID FROM @AeList)
             ) aeUser1 on aeUser1.username = task.assignedto
	left join (select a.UserName,a.userid,pm.HUD_Project_Manager_ID from HUD_Project_Manager pm
                        join Address ad on pm.addressid = ad.AddressId
                        join HCP_Authentication a on a.Username = ad.Email
				where  pm.HUD_Project_Manager_ID in 
				( SELECT HUD_Project_Manager_ID FROM @AeList)
		      ) aeUser2 on aeUser2.username = task.assignedby
	
   inner join HUD_Project_Manager PM 
   on pm.HUD_Project_Manager_ID =  (isnull(aeUser1.HUD_Project_Manager_ID,'')+isnull(aeUser2.HUD_Project_Manager_ID,'')) --getting one hud_project_manager_id column from both assignedby and assignedto
   inner join HUD_WorkLoad_Manager_Portfolio_Manager wpm
   on pm.HUD_Project_Manager_ID = wpm.HUD_Project_Manager_ID
   INNER JOIN HUD_WorkLoad_Manager WLM
   ON wpm.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
  
  
   WHERE 
	((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   R4R.ServicerSubmissionDate>= @StartDate and R4R.ServicerSubmissionDate< DATEADD(day,1,@EndDate))
	
   OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND R4R.ServicerSubmissionDate <= GETUTCDATE()))
   
	and 
	R4R.TaskId is not null 
	
   order by R4R.ServicerSubmissionDate ASC

  
   -- ****************************************************  R4R Get Tasks Reassigned to AE     *************************************************
   INSERT INTO #PAMReport
      SELECT R4R.FHANumber, R4R.PropertyId, R4R.PropertyName, TRA.ReAssignedTo, to_wlm.HUD_WorkLoad_Manager_Name,
   'R4R',
   (CASE WHEN R4R.RequestStatus = 1 OR R4R.RequestStatus = 4 THEN 'Servicer Comments: '+R4R.ServicerRemarks
    WHEN R4R.RequestStatus = 2 THEN IsNull('Servicer Comments: '+R4R.ServicerRemarks+@NewLineChar,'')+''+ IsNull('HUD Remarks: '+R4R.HudRemarks+@NewLineChar,'')+''+IsNull('AdditionalRemarks: '+R4R.AdditionalRemarks,'') ELSE NULL END), 
   R4R.ServicerSubmissionDate, 
   (CASE WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN R4R.ModifiedOn ELSE NULL END),
   (CASE WHEN R4R.RequestStatus = 2 THEN DATEDIFF(DAY,TRA.CreatedOn,R4R.ModifiedOn) ELSE DATEDIFF(DAY,TRA.CreatedOn,GETUTCDATE()) END), 
   (CASE WHEN R4R.RequestStatus = 1 THEN 1 ELSE 0 END), 
   R4R.TotalRequestedAmount, 
   (CASE WHEN (R4R.IsLenderDelegate =1 AND R4R.RequestStatus = 4) THEN R4R.TotalApprovedAmount
      WHEN R4R.RequestStatus = 4 THEN R4R.TotalRequestedAmount 
      WHEN R4R.RequestStatus = 2 THEN R4R.TotalApprovedAmount
      ELSE NULL END),
   l.Lender_Name,
   (SELECT UserName FROM HCP_Authentication WHERE UserID = R4R.CreatedBy),
   R4RId as Id,
   R4R.TaskId, 
   TRA.ReAssignedTo, 
   TRA.CreatedOn,
   TRA.ReAssignedBy,
   (CASE  
      WHEN R4R.RequestStatus = 1 THEN 'Pending'  
      WHEN R4R.RequestStatus = 2 OR R4R.RequestStatus = 4 THEN 'Accepted'
     ELSE NULL END),
   (CASE WHEN R4R.IsLenderDelegate = 1 THEN 'Lender Delegated'
      WHEN R4R.RequestStatus = 2 THEN 'Account Executive'
      WHEN R4R.RequestStatus = 4 THEN 'Auto Approved'
      ELSE NULL END),
      null,null,
	  (SELECT FirstName + ' ' + LastName as ReAssignedFrom FROM HCP_Authentication WHERE UserName =  TRA.ReAssignedFrom),
	  (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName =  TRA.HUDPM) 

     FROM #TaskReAssignment TRA 
   INNER JOIN  Reserve_For_Replacement_Form_Data R4R
    ON TRA.TaskId = R4R.TaskId

	INNER JOIN ProjectInfo PF    ON R4R.FHANumber = PF.FHANumber and R4R.PropertyId = pf.PropertyID 
	join User_Lender u on u.User_ID = r4r.CreatedBy
	inner join [LenderInfo] l on u.Lender_ID = l.LenderID  

    inner JOIN @AeList AE
    ON AE.HUD_Project_Manager_ID = TRA.To_HUD_Project_Manager_ID  
    JOIN HUD_Project_Manager PM
    ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
     left JOIN HUD_WorkLoad_Manager  to_wlm
    ON TRA.To_HUD_WorkLoad_Manager_ID  = to_wlm.HUD_WorkLoad_Manager_ID
   WHERE 
    (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
		OR
	((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE())
   
   order by TRA.CreatedOn
   END
END
 -- IF (@ProjectAction is null OR @ProjectAction in (0, 10, 20))
IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 0 )  
-- **************** Display all NCR without reassigned Tasks **************************
  BEGIN
  if(@UserRole != 'InternalSpecialOptionUser')
	begin
		INSERT INTO #PAMReport
		 SELECT NCR.FHANumber
		   , PF.PropertyID, PF.ProjectName
		  ,case 
				when (task.AssignedBy like '%hud.gov%' or task.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedBy)
				when (task.AssignedTo like '%hud.gov%' or task.AssignedTo = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedTo)
			end as HudProjectManagerName
		   ,WLM.HUD_WorkLoad_Manager_Name,
		   'NCR',(CASE WHEN NCR.RequestStatus = 2 then 'Servicer Comments: '+ NCR.ServicerRemarks else 'AE Comments: '+ Task.notes End), NCR.SubmittedDate, 
			  (CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 4 OR NCR.RequestStatus = 5 THEN isnull(NCR.ApprovedDate,NCR.modifiedon) ELSE NULL END),
		   (CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 THEN DATEDIFF(DAY,NCR.SubmittedDate,NCR.ApprovedDate) ELSE DATEDIFF(DAY,NCR.SubmittedDate,GETUTCDATE()) END), 
		   (CASE WHEN NCR.RequestStatus = 1 THEN 1 ELSE 0 END), 
		   NCR.PaymentRequested, 
		   (CASE WHEN NCR.RequestStatus = 2 THEN NCR.PaymentRequested 
			  WHEN NCR.RequestStatus = 3 THEN NCR.ApprovedAmount ELSE NULL END),
		   l.Lender_Name,
		   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NCR.CreatedBy),
		   NonCriticalRepairsRequestID as Id,
		   NCR.TaskId, null, null,null,
		   (CASE WHEN NCR.RequestStatus = 1 THEN 'Pending'
			  WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 4 THEN 'Accepted'
			  WHEN NCR.RequestStatus = 3 THEN 'Accepted with Changes'
			  WHEN NCR.RequestStatus = 5 THEN 'Denied'
			 ELSE NULL END),
		   (CASE WHEN  NCR.IsLenderDelegate = 1 THEN 'Lender Delegated'
			  WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 5 THEN 'Account Executive'
			  WHEN NCR.RequestStatus = 4 THEN 'Auto Approved'
			  ELSE NULL END), 
					null,null,
					null,
			case 
				when (Task.AssignedBy like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedBy)
				when (Task.AssignedTo like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedTo)
			end
					FROM NonCriticalRepairsRequests NCR

   
			inner join 
  
			(select tk1.taskinstanceid,tk1.SequenceId, tk1.taskid, tk1.IsReassigned,tk1.assignedto,tk1.assignedby,tk1.Notes from [$(TaskDB)].dbo.Task tk1 
				join
			(SELECT  TaskInstanceId, max(SequenceId) as seqId,max(taskid)  as taskid,IsReassigned
						FROM
					(SELECT *, CONVERT(varchar(MAX), DataStore1) as [XMLDataString] FROM [$(TaskDB)].dbo.task) x
				WHERE [XMLDataString] like '%NonCriticalRepairsViewModel%' 
				and (IsReassigned is null or IsReassigned = 0) 
				group by TaskInstanceId,IsReassigned
			) tk2
			on tk1.TaskInstanceId = tk2.TaskInstanceId and tk1.SequenceId = tk2.seqId
			) Task
		
		on NCR.Taskid=Task.taskid

		INNER JOIN ProjectInfo PF    ON NCR.FHANumber = PF.FHANumber and NCR.PropertyId = pf.PropertyID 
		join User_Lender u on u.User_ID = NCR.CreatedBy
		inner join [LenderInfo] l on u.Lender_ID = l.LenderID  
		left join (select a.UserName,a.userid,pm.HUD_Project_Manager_ID from HUD_Project_Manager pm
                                    join Address ad on pm.addressid = ad.AddressId
                                    join HCP_Authentication a on a.Username = ad.Email
									  where  pm.HUD_Project_Manager_ID in 
                                   ( SELECT HUD_Project_Manager_ID FROM @AeList)
             ) aeUser1 on aeUser1.username = task.assignedto
		left join (select a.UserName,a.userid,pm.HUD_Project_Manager_ID from HUD_Project_Manager pm
										join Address ad on pm.addressid = ad.AddressId
										join HCP_Authentication a on a.Username = ad.Email
										  where  pm.HUD_Project_Manager_ID in  
					( SELECT HUD_Project_Manager_ID FROM @AeList)
				  ) aeUser2 on aeUser2.username = task.assignedby

		inner join HUD_Project_Manager PM 
		on pm.HUD_Project_Manager_ID =  (isnull(aeUser1.HUD_Project_Manager_ID,'')+isnull(aeUser2.HUD_Project_Manager_ID,'')) --getting one hud_project_manager_id column from both assignedby and assignedto
		inner join HUD_WorkLoad_Manager_Portfolio_Manager wpm
		on pm.HUD_Project_Manager_ID = wpm.HUD_Project_Manager_ID
		INNER JOIN HUD_WorkLoad_Manager WLM
		ON wpm.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
  
		WHERE 
		((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  NCR.SubmittedDate>= @StartDate and NCR.SubmittedDate< DATEADD(day,1,@EndDate))
			OR
		((@StartDate IS NULL AND @EndDate IS NULL) AND NCR.SubmittedDate <= GETUTCDATE()))
   
		and NCR.TaskId is not null 
		order by NCR.SubmittedDate 
-- ****************  Select All NCR Task ReAssgingment items *************************
		INSERT INTO #PAMReport
		   SELECT NCR.FHANumber, PF.PropertyID, PF.ProjectName, TRA.ReAssignedTo, from_wlm.HUD_WorkLoad_Manager_Name,
														'NCR','Servicer Comments: '+NCR.servicerremarks, NCR.SubmittedDate, 
			  (CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 4 OR NCR.RequestStatus = 5 THEN isnull(NCR.ApprovedDate,NCR.modifiedon)  ELSE NULL END),
		   (CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 THEN DATEDIFF(DAY,TRA.CreatedOn,NCR.ApprovedDate) ELSE DATEDIFF(DAY,TRA.CreatedOn,GETUTCDATE()) END), 
		   (CASE WHEN NCR.RequestStatus = 1 THEN 1 ELSE 0 END), 
		   NCR.PaymentRequested, 
		   (CASE WHEN NCR.RequestStatus = 2 THEN NCR.PaymentRequested 
			  WHEN NCR.RequestStatus = 3 THEN NCR.ApprovedAmount ELSE NULL END),
		   l.Lender_Name,
		   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NCR.CreatedBy),
		   NonCriticalRepairsRequestID as Id,
		   NCR.TaskId,
		   TRA.ReAssignedTo,
		   TRA.CreatedOn,
		   TRA.ReAssignedBy,
		   (CASE WHEN NCR.RequestStatus = 1 THEN 'Pending'
			  WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 4 THEN 'Accepted'
			  WHEN NCR.RequestStatus = 3 THEN 'Accepted with Changes'
			  WHEN NCR.RequestStatus = 5 THEN 'Denied'
			 ELSE NULL END),
		   (CASE WHEN  NCR.IsLenderDelegate = 1 THEN 'Lender Delegated'
			  WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 5 THEN 'Account Executive'
			  WHEN NCR.RequestStatus = 4 THEN 'Auto Approved'
			  ELSE NULL END),
			  null,null,
			( SELECT FirstName + ' ' + LastName as ReAssignedFrom FROM HCP_Authentication WHERE UserName = TRA.ReAssignedFrom ),
			(SELECT FirstName + ' ' + LastName FROM HCP_Authentication WHERE UserName = TRA.HUDPM ) as HUDPM    
		   FROM #TaskReAssignment TRA 
		   JOIN  NonCriticalRepairsRequests NCR
			ON TRA.TaskId = NCR.TaskId
  
			INNER JOIN ProjectInfo PF    ON NCR.FHANumber = PF.FHANumber and NCR.PropertyId = pf.PropertyID 
			join User_Lender u on u.User_ID = NCR.CreatedBy
			inner join [LenderInfo] l on u.Lender_ID = l.LenderID  

			INNER JOIN @AeList AE
			ON AE.HUD_Project_Manager_ID = TRA.To_HUD_Project_Manager_ID  
			JOIN HUD_Project_Manager PM
			ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
			  JOIN HUD_WorkLoad_Manager  from_wlm
			ON TRA.From_HUD_WorkLoad_Manager_ID = from_wlm.HUD_WorkLoad_Manager_ID
			WHERE 
				(@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
				 OR
				((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE()) 
    end
------------------------ NCR ISOU TASK REASSIGNMENTS--------------------------------
	BEGIN

		INSERT INTO #PAMReport
		SELECT 
		NCR.FHANumber,
		PF.PropertyID,
		PF.ProjectName, 
		au.FirstName +' '+au.LastName+'*' as HudProjectManagerName,
		WLM.HUD_WorkLoad_Manager_Name,
		'NCR',
		(CASE WHEN NCR.RequestStatus = 2 then 'Servicer Comments: '+ NCR.ServicerRemarks else 'AE Comments: '+ Task.notes End),
		NCR.SubmittedDate, 
		(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 4 OR NCR.RequestStatus = 5 THEN isnull(NCR.ApprovedDate,NCR.modifiedon) ELSE NULL END),
		(CASE WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 THEN DATEDIFF(DAY,NCR.SubmittedDate,NCR.ApprovedDate) ELSE DATEDIFF(DAY,NCR.SubmittedDate,GETUTCDATE()) END), 
		(CASE WHEN NCR.RequestStatus = 1 THEN 1 ELSE 0 END), 
		NCR.PaymentRequested, 
		(CASE WHEN NCR.RequestStatus = 2 THEN NCR.PaymentRequested 
		WHEN NCR.RequestStatus = 3 THEN NCR.ApprovedAmount ELSE NULL END),
		l.Lender_Name,
		(SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NCR.CreatedBy),
		NonCriticalRepairsRequestID as Id,
		NCR.TaskId,
		(SELECT a.FirstName + ' '  + a.LastName as ReAssignedBy FROM HCP_Authentication a WHERE UserID = TR.ToAeid),
		TR.CreatedOn,
		TR.ReAssignedBy,--(SELECT a.FirstName + ' '  + a.LastName as ReAssignedTo FROM HCP_Authentication a WHERE UserName = TR.ReAssignedBy),
		(CASE WHEN NCR.RequestStatus = 1 THEN 'Pending'
		WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 4 THEN 'Accepted'
		WHEN NCR.RequestStatus = 3 THEN 'Accepted with Changes'
		WHEN NCR.RequestStatus = 5 THEN 'Denied'
		ELSE NULL END),
		(CASE WHEN  NCR.IsLenderDelegate = 1 THEN 'Lender Delegated'
		WHEN NCR.RequestStatus = 2 OR NCR.RequestStatus = 3 OR NCR.RequestStatus = 5 THEN 'Account Executive'
		WHEN NCR.RequestStatus = 4 THEN 'Auto Approved'
		ELSE NULL END), 
		null,
		null,
		(SELECT a.FirstName + ' '  + a.LastName as ReAssignedFrom FROM HCP_Authentication a WHERE UserName = TR.ReAssignedFrom ),
		(SELECT a.FirstName + ' '  + a.LastName FROM HCP_Authentication a WHERE UserName = TR.HUDPM ) as HUDPM
			FROM #TaskReAssignmentISOU TR
			INNER JOIN  NonCriticalRepairsRequests NCR
			ON TR.TaskId = NCR.TaskId

			INNER JOIN ProjectInfo PF    ON NCR.FHANumber = PF.FHANumber and NCR.PropertyId = pf.PropertyID 
			join User_Lender u on u.User_ID = NCR.CreatedBy
			inner join [LenderInfo] l on u.Lender_ID = l.LenderID  

			INNER JOIN [$(TaskDB)].dbo.task Task
			on NCR.Taskid=Task.taskid
			Inner join HCP_Authentication au
			on au.UserID = TR.ToAeid
			Inner join @ISOUList isou
			on isou.InternalSpecialOptionUserId = au.UserID
			left join HUD_WorkloadManager_InternalSpecialOptionUser wlm_isou
			on wlm_isou.InternalSpecialOptionUserId = au.UserID
			left JOIN HUD_WorkLoad_Manager wlm
			ON wlm.HUD_WorkLoad_Manager_ID = wlm_isou.HUD_WorkloadManagerId
			WHERE 
			(@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  Tr.CreatedOn>= @StartDate and TR.CreatedOn< DATEADD(day,1,@EndDate))

			OR
			((@StartDate IS NULL AND @EndDate IS NULL) AND TR.CreatedOn <= GETUTCDATE()) 
	  END
  END
-- ***********************************************************


 --IF (@ProjectAction is null OR @ProjectAction in (2, 20, 12))
 
IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 2 )  
  BEGIN
   if(@UserRole != 'InternalSpecialOptionUser')
   -- ************** Insert NCR Extention to PAM Report with out reassigned Tasks *************
  Begin
   INSERT INTO #PAMReport
  SELECT NX.FHANumber, PF.PropertyID, PF.ProjectName, 
 case 
	  when (t.AssignedBy like '%hud.gov%' or t.AssignedBy = 'acctexec001@gmail.com') then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = t.AssignedBy)
	  when (t.AssignedTo like '%hud.gov%' or t.AssignedTo = 'acctexec001@gmail.com') then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = t.AssignedTo)
		when (t.AssignedTo is null or t.AssignedBy is null) then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE Userid = nx.ModifiedBy)
	end as HudProjectManagerName,
 WLM.HUD_WorkLoad_Manager_Name,
    'NCRExtension',(CASE WHEN NX.ExtensionRequestStatus = 2 then '.' else 'AE Comments: '+ T.notes End), NX.CreatedOn, 
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE NX.ModifiedOn END),
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN DATEDIFF(DAY,NX.CreatedOn,GETUTCDATE()) ELSE DATEDIFF(DAY,NX.CreatedOn,NX.ModifiedOn) END), 
   CASE WHEN NX.ExtensionRequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NX.CreatedBy),
   NX.NonCriticalRequestExtensionId as Id,
   NX.TaskId, null, null,null,
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN 'Pending'
      WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 4 THEN 'Accepted'
      WHEN NX.ExtensionRequestStatus = 3 THEN 'Denied'
     ELSE NULL END),
   (CASE WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 3 THEN 'Account Executive'
      WHEN NX.ExtensionRequestStatus = 4 THEN 'Auto Approved'
     ELSE NULL END),
   null,null,
   null   ,
   case 
		when (t.AssignedBy like '%hud.gov%' or t.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = t.AssignedBy)
		when (t.AssignedTo like '%hud.gov%' or t.AssignedTo = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = t.AssignedTo)
		when (t.AssignedTo is null or t.AssignedBy is null) then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE Userid = nx.ModifiedBy)
	end
   FROM NonCriticalRequestExtensions NX
	left join [$(TaskDB)].dbo.task t on (NX.taskid = t.TaskId and (IsReassigned is null or IsReassigned = 0) )
	INNER JOIN ProjectInfo PF    ON NX.FHANumber = PF.FHANumber and NX.PropertyId = pf.PropertyID 
   join User_Lender u on u.User_ID = NX.CreatedBy
   inner join [LenderInfo] l on u.Lender_ID = l.LenderID  
    INNER JOIN HUD_WorkLoad_Manager WLM
    ON PF.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
   WHERE 
	
	 NX.ModifiedBy in (select a.UserID  from HUD_Project_Manager pm
										join Address ad on pm.addressid = ad.AddressId
										join HCP_Authentication a on a.Username = ad.Email
								where  pm.HUD_Project_Manager_ID in 
					  ( SELECT HUD_Project_Manager_ID FROM @AeList)	)
	and 
	( (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND   NX.CreatedOn>= @StartDate and NX.CreatedOn< DATEADD(day,1,@EndDate))
		or 
    NX.ModifiedBy in (select a.UserID  from HUD_Project_Manager pm
										join Address ad on pm.addressid = ad.AddressId
										join HCP_Authentication a on a.Username = ad.Email
								where  pm.HUD_Project_Manager_ID in 
					  ( SELECT HUD_Project_Manager_ID FROM @AeList)	)
	and ((@StartDate IS NULL AND @EndDate IS NULL) AND NX.CreatedOn <= GETUTCDATE()))
 -- ************** Insert NCR Extention reassigned Tasks to PAM Report   *************
  
   INSERT INTO #PAMReport
   SELECT NX.FHANumber, PF.PropertyID, PF.ProjectName, TRA.ReAssignedTo, from_wlm.HUD_WorkLoad_Manager_Name,
                                                'NCRExtension','AE Comments: '+ Task.notes, NX.CreatedOn, 
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE NX.ModifiedOn END),
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN DATEDIFF(DAY,TRA.CreatedOn,GETUTCDATE()) ELSE DATEDIFF(DAY,TRA.CreatedOn,NX.ModifiedOn) END), 
   CASE WHEN NX.ExtensionRequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NX.CreatedBy),
   NX.NonCriticalRequestExtensionId as Id,
   NX.TaskId,
   TRA.ReAssignedTo,
   TRA.CreatedOn,
   TRA.ReAssignedBy,
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN 'Pending'
      WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 4 THEN 'Accepted'
      WHEN NX.ExtensionRequestStatus = 3 THEN 'Denied'
     ELSE NULL END),
   (CASE WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 3 THEN 'Account Executive'
      WHEN NX.ExtensionRequestStatus = 4 THEN 'Auto Approved'
     ELSE NULL END),
   null,null,
   (SELECT FirstName + ' ' + LastName as ReAssignedFrom FROM HCP_Authentication WHERE UserName = TRA.ReAssignedFrom )   ,
   (SELECT FirstName + ' ' + LastName FROM HCP_Authentication WHERE UserName = tra.HUDPM ) as HUDPM    
   FROM #TaskReAssignment TRA 
   INNER JOIN  NonCriticalRequestExtensions NX
    ON TRA.TaskId = NX.TaskId
	INNER JOIN ProjectInfo PF    ON NX.FHANumber = PF.FHANumber and NX.PropertyId = pf.PropertyID 
   join User_Lender u on u.User_ID = NX.CreatedBy
 
	inner join [LenderInfo] l on u.Lender_ID = l.LenderID  

                                                                INNER JOIN [$(TaskDB)].dbo.task Task
                                                                on NX.Taskid=Task.taskid
    INNER JOIN @AeList AE
    ON AE.HUD_Project_Manager_ID = TRA.To_HUD_Project_Manager_ID  
    JOIN HUD_Project_Manager PM
    ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
      JOIN HUD_WorkLoad_Manager  from_wlm
    ON TRA.From_HUD_WorkLoad_Manager_ID  = from_wlm.HUD_WorkLoad_Manager_ID
   WHERE --R4R.ServicerSubmissionDate BETWEEN @StartDate AND @EndDate
                 (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
				
    OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE()) 
   ENd

   -- NCRE- EXT insert ISOU taskreassignment to the pamreport
Begin
	insert into #PAMReport
SELECT 
	NX.FHANumber, 
	PF.PropertyID, 
	PF.ProjectName,
	au.FirstName +' '+au.LastName+'*' as HudProjectManagerName,
	WLM.HUD_WorkLoad_Manager_Name,
     'NCRExtension','AE Comments: '+ nx.Comments,
	  NX.CreatedOn, 
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN NULL ELSE NX.ModifiedOn END),
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN DATEDIFF(DAY,Tr.CreatedOn,GETUTCDATE()) ELSE DATEDIFF(DAY,TR.CreatedOn,NX.ModifiedOn) END), 
   CASE WHEN NX.ExtensionRequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = NX.CreatedBy),
   NX.NonCriticalRequestExtensionId as Id,
   NX.TaskId,
   (SELECT a.FirstName + ' '  + a.LastName as ReAssignedBy FROM HCP_Authentication a WHERE UserID = TR.ToAeid),
   TR.CreatedOn,
   TR.ReAssignedBy,--(SELECT a.FirstName + ' '  + a.LastName as ReAssignedTo FROM HCP_Authentication a WHERE UserName = TR.ReAssignedBy),
   (CASE WHEN NX.ExtensionRequestStatus = 1 THEN 'Pending'
      WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 4 THEN 'Accepted'
      WHEN NX.ExtensionRequestStatus = 3 THEN 'Denied'
     ELSE NULL END),
   (CASE WHEN NX.ExtensionRequestStatus = 2 OR NX.ExtensionRequestStatus = 3 THEN 'Account Executive'
      WHEN NX.ExtensionRequestStatus = 4 THEN 'Auto Approved'
     ELSE NULL END),
   null,null,
   (SELECT a.FirstName + ' '  + a.LastName as ReAssignedFrom FROM HCP_Authentication a WHERE UserName = tr.ReAssignedFrom   ),
   (SELECT FirstName + ' ' + LastName FROM HCP_Authentication WHERE UserName = tr.HUDPM ) as HUDPM 
   from #TaskReAssignmentISOU tr
	inner join NonCriticalRequestExtensions nx  on nx.TaskId = tr.TaskId

	INNER JOIN ProjectInfo PF    ON NX.FHANumber = PF.FHANumber and NX.PropertyId = pf.PropertyID 
   join User_Lender u on u.User_ID = NX.CreatedBy
	inner join [LenderInfo] l on u.Lender_ID = l.LenderID  

   	Inner join HCP_Authentication au
		on au.UserID = TR.ToAeid
	Inner join @ISOUList isou
		on isou.InternalSpecialOptionUserId = au.UserID
    Left join HUD_WorkloadManager_InternalSpecialOptionUser wlm_isou
	    on wlm_isou.InternalSpecialOptionUserId = au.UserID
   Left JOIN HUD_WorkLoad_Manager wlm
    ON wlm.HUD_WorkLoad_Manager_ID = wlm_isou.HUD_WorkloadManagerId
	WHERE 
        (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TR.CreatedOn>= @StartDate and TR.CreatedOn< DATEADD(day,1,@EndDate))
		

    OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND TR.CreatedOn <= GETUTCDATE()) 
END
  END
-- ***********************************************************
  --***********OPA Parent TASKS*****************--
IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId in (select PamProjectActionId from PAMProjectActionTypes where PamProjectActionId  > 2 ) ) 
  BEGIN
   if(@UserRole != 'InternalSpecialOptionUser')
  Begin
   -- select * from HCP_Project_Action
   INSERT INTO #PAMReport
   
  SELECT PAF.FHANumber
  , PF.PropertyID, PF.ProjectName
  ,case 
		when (task.AssignedBy like '%hud.gov%' or task.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedBy)
		when (task.AssignedTo like '%hud.gov%' or task.AssignedTo = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = task.AssignedTo)
	end as HudProjectManagerName
  , WLM.HUD_WorkLoad_Manager_Name,
   HPA.ProjectActionName,
  (CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
      WHEN  PAF.RequestStatus = 7 THEN CONCAT( isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
     WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: ' + addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
   PAF.CreatedOn, 
   (CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
   (CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
   CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
   PAF.ProjectActionFormId as Id,
   PAF.MytaskId as TaskId,
   null,
   null,
   null,
   (CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
      WHEN PAF.RequestStatus = 2  THEN 'Accepted'
      WHEN PAF.RequestStatus = 5 THEN 'Denied'
      WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
     ELSE NULL END),
   (CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
      ELSE NULL END),
      addl.addlcount,
   null,
   null,
  case 
		when (task.AssignedBy like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedBy)
		when (task.AssignedTo like '%hud.gov%' )  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = task.AssignedTo)
	end
   FROM OPAForm PAF
   inner join 
       (SELECT  distinct Taskinstanceid,max(taskid) as taskid,MAx(Sequenceid) as seqid, AssignedTo,IsReassigned ,Notes,AssignedBy FROM
             (SELECT * FROM [$(TaskDB)].dbo.task) x
       WHERE PageTypeId = 3       and (
             assignedby in (select a.UserName from HUD_Project_Manager pm
										join Address ad on pm.addressid = ad.AddressId
										join HCP_Authentication a on a.Username = ad.Email
								where  pm.HUD_Project_Manager_ID in 
								( SELECT HUD_Project_Manager_ID FROM @AeList) )
             or 
             assignedto in (select a.UserName from HUD_Project_Manager pm
										join Address ad on pm.addressid = ad.AddressId
										join HCP_Authentication a on a.Username = ad.Email
								where  pm.HUD_Project_Manager_ID in 
								( SELECT HUD_Project_Manager_ID FROM @AeList) )
             ) and (IsReassigned is null or IsReassigned = 0) 
       group by taskinstanceid,AssignedTo,IsReassigned,Notes,AssignedBy ) task
       on PAF.MytaskId = task.taskid 
   INNER JOIN ProjectInfo PF
    ON PAF.FHANumber = PF.FHANumber 
       --inner join [Lender_FHANumber] lf on lf.FHANumber = PAF.FHANumber
          inner join User_Lender ul on ul.User_ID = paf.CreatedBy 
         inner join [LenderInfo] l on ul.Lender_ID = l.LenderID

            LEFT JOIN 
    (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
    FROM  AdditionalInformation 
     GROUP BY ParentFormId) addl
    ON addl.ParentFormId = PAF.ProjectActionFormId 
     LEFT JOIN 
    (SELECT 
     ParentFormId, 
     CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
       ELSE null
      END as cmt
    FROM  AdditionalInformation 
    where AEComment is not null or LenderComment is not null
     GROUP BY ParentFormId) addlCom
    ON addlCom.ParentFormId = PAF.ProjectActionFormId 
	
	inner join (select a.UserID,UserName from HCP_Authentication a 
				join webpages_UsersInRoles ur on a.userid = ur.userid 
				join webpages_roles r on ur.RoleId = r.RoleId
				and r.RoleName = 'AccountExecutive') au
	on au.Username = task.Assignedto or au.Username = task.Assignedby
	inner join (select pm.*,ad.Email from HUD_Project_Manager pm
				join Address ad on pm.addressid = ad.addressid) pm_ad
	on au.Username = pm_ad.Email
   INNER JOIN @AeList AE
    ON AE.HUD_Project_Manager_ID = pm_ad.HUD_Project_Manager_Id
   inner join HUD_WorkLoad_Manager_Portfolio_Manager wpm
   on wpm.HUD_Project_Manager_ID = pm_ad.HUD_Project_Manager_ID
   INNER JOIN HUD_Project_Manager PM
    ON pm_ad.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
   INNER JOIN HUD_WorkLoad_Manager WLM
    ON wpm.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
   INNER JOIN HCP_Project_Action HPA 
    ON HPA.ProjectActionID = PAF.ProjectActionTypeId
   INNER JOIN PAMProjectActionTypes PAMType
    ON PAMType.Name = HPA.ProjectActionName
   INNER JOIN @SelectedProjectActions SPA
    ON SPA.PamProjectActionId = PAMType.PamProjectActionId
   WHERE
   ((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  PAF.CreatedOn>= @StartDate and PAF.CreatedOn< DATEADD(day,1,@EndDate))
   --and lf.FHA_EndDate is null 
   and (task.IsReassigned is null or task.IsReassigned = 0)
   OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND PAF.CreatedOn <= GETUTCDATE()))
   --and lf.FHA_EndDate is null 
   and (task.IsReassigned is null or task.IsReassigned = 0)
    
	--****************OPA My Reassignment Tasks************************--
   
   INSERT INTO #PAMReport
   SELECT PAF.FHANumber, PF.PropertyID, PF.ProjectName, TRA.ReAssignedTo, to_wlm.HUD_WorkLoad_Manager_Name,
   HPA.ProjectActionName,
    (CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
      WHEN  PAF.RequestStatus = 7 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
     WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
   PAF.CreatedOn, 
    (CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
   (CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
   CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
   PAF.ProjectActionFormId as Id,
   PAF.MytaskId as TaskId,
   TRA.ReAssignedTo,
   TRA.CreatedOn,
   TRA.ReAssignedBy,
    (CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
      WHEN PAF.RequestStatus = 2  THEN 'Accepted'
      WHEN PAF.RequestStatus = 5 THEN 'Denied'
      WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
     ELSE NULL END),
   (CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
      ELSE NULL END),
      addl.addlcount,
   null,
   (SELECT FirstName + ' ' + LastName as ReAssignedFrom FROM HCP_Authentication WHERE UserName = TRA.ReAssignedFrom ),
   (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = TRA.HUDPM) as HUDPM 
   FROM #TaskReAssignment TRA 
   INNER JOIN  OPAForm  PAF
    ON TRA.TaskId = PAF.MytaskId     
   INNER JOIN ProjectInfo PF
    ON PAF.FhaNumber  = PF.FhaNumber
	--inner join [Lender_FHANumber] lf on lf.FHANumber = PAF.FHANumber
	 inner join User_Lender ul on ul.User_ID = paf.CreatedBy 
         inner join [LenderInfo] l on ul.Lender_ID = l.LenderID

      LEFT JOIN 
    (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
    FROM  AdditionalInformation 
     GROUP BY ParentFormId) addl
    ON addl.ParentFormId = PAF.ProjectActionFormId 
      LEFT JOIN 
    (SELECT 
     ParentFormId, 
     CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
       ELSE null
      END as cmt
    FROM  AdditionalInformation 
    where AEComment is not null or LenderComment is not null
     GROUP BY ParentFormId) addlCom
    ON addlCom.ParentFormId = PAF.ProjectActionFormId 
    INNER JOIN @AeList AE
    ON AE.HUD_Project_Manager_ID = TRA.To_HUD_Project_Manager_ID  
    inner JOIN HUD_Project_Manager PM
    ON TRA.From_HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
    left  JOIN HUD_WorkLoad_Manager  to_wlm
    ON TRA.To_HUD_WorkLoad_Manager_ID  = to_wlm.HUD_WorkLoad_Manager_ID
   INNER JOIN HCP_Project_Action HPA 
    ON HPA.ProjectActionID = PAF.ProjectActionTypeId
   INNER JOIN PAMProjectActionTypes PAMType
    ON PAMType.Name = HPA.ProjectActionName
   INNER JOIN @SelectedProjectActions SPA
    ON SPA.PamProjectActionId = PAMType.PamProjectActionId
   WHERE 
	(@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  TRA.CreatedOn>= @StartDate and TRA.CreatedOn< DATEADD(day,1,@EndDate))
	--and lf.FHA_EndDate is null
    OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND TRA.CreatedOn <= GETUTCDATE()) --and lf.FHA_EndDate is null

   ----**************OPA Child Tasks**************************------
   INSERT INTO #PAMReport
   SELECT PAF.FHANumber, PF.PropertyID, PF.ProjectName, 
   --PM.HUD_Project_Manager_Name, 
   case 
         when (parentChildTask.AssignedBy like '%hud.gov%' or parentChildTask.AssignedBy = 'acctexec001@gmail.com')then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = parentChildTask.AssignedBy)
             when (parentChildTask.AssignedTo like '%hud.gov%' or parentChildTask.AssignedTo = 'acctexec001@gmail.com')then (SELECT FirstName + ' ' + LastName as HudProjectManagerName FROM HCP_Authentication WHERE UserName = parentChildTask.AssignedTo)
   end as HUD_Project_Manager_Name,
   WLM.HUD_WorkLoad_Manager_Name,
   HPA.ProjectActionName,
  (CASE WHEN PAF.RequestStatus = 1 OR PAF.RequestStatus = 4    THEN 'Servicer Comments: '+PAF.ServicerComments 
      WHEN  PAF.RequestStatus = 7 THEN CONCAT( isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: '+addlCom.cmt+@NewLineChar,'')) 
     WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 5 THEN CONCAT(  isnull('Servicer Comments: '+PAF.ServicerComments+@NewLineChar,''), isnull('Additional Remarks: ' + addlCom.cmt+@NewLineChar,''),  isnull('AE Comments: '+PAF.AEComments,'') ) ELSE NULL END),
   PAF.CreatedOn, 
   (CASE WHEN PAF.RequestStatus = 1 THEN NULL ELSE PAF.ModifiedOn END),
   (CASE WHEN (PAF.RequestStatus = 1 OR PAF.RequestStatus = 7) THEN DATEDIFF(DAY,PAF.CreatedOn,GETUTCDATE()) ELSE ( DATEDIFF(DAY,PAF.CreatedOn,PAF.ModifiedOn) - isnull(addl.totAddlDays,0))  END), 
   CASE WHEN PAF.RequestStatus = 1 THEN 1 ELSE 0 END,
   NULL, NULL,
   l.Lender_Name,
   (SELECT UserName as LenderEmail FROM HCP_Authentication WHERE UserID = PAF.CreatedBy),
   PAF.ProjectActionFormId as Id,
   PAF.MytaskId as TaskId,
   null,
   null,
   null,
   (CASE WHEN PAF.RequestStatus = 1 THEN 'Pending'
      WHEN PAF.RequestStatus = 2  THEN 'Accepted'
      WHEN PAF.RequestStatus = 5 THEN 'Denied'
      WHEN PAF.RequestStatus = 7 THEN 'Requested Additional Info'
     ELSE NULL END),
   (CASE WHEN PAF.RequestStatus = 2 OR PAF.RequestStatus = 3 OR PAF.RequestStatus = 5 OR PAF.RequestStatus = 7 THEN 'Account Executive'
      ELSE NULL END),
      addl.addlcount,
   parentChildTask.ChildTaskInstanceId,
    null,
	case 
		when (parentChildTask.AssignedBy like '%hud.gov%' or parentChildTask.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = parentChildTask.AssignedBy)
		when (parentChildTask.AssignedTo like '%hud.gov%' or parentChildTask.AssignedBy = 'acctexec001@gmail.com')  then (SELECT FirstName + ' ' + LastName as HUDPM FROM HCP_Authentication WHERE UserName = parentChildTask.AssignedTo)
	end
   FROM (select op1.* from OPAForm op1 join
			(select max(MytaskId) as MytaskId from OPAForm 
			group by TaskInstanceId  having count(Taskinstanceid) in (1,2)) op2
		 on op1.MytaskId = op2.	MytaskId
		) PAF
    join (
    select *
    from [$(TaskDB)].dbo.ParentChildTask pt, [$(TaskDB)].dbo.task t
    where pt.ChildTaskInstanceId = t.TaskInstanceId and t.TaskStepId=16 ) as parentChildTask 
	on PAF.TaskInstanceId = parentChildTask.ParentTaskInstanceId
   INNER JOIN ProjectInfo PF
    ON PAF.FHANumber = PF.FHANumber
	--inner join [Lender_FHANumber] lf on lf.FHANumber = PAF.FHANumber
	  	   inner join User_Lender ul on ul.User_ID = paf.CreatedBy 
         inner join [LenderInfo] l on ul.Lender_ID = l.LenderID

            LEFT JOIN 
    (SELECT ParentFormId,count(*) as addlcount, sum(datediff (day, isnull(CreatedDate,GETUTCDATE()), isnull(ModifiedDate,GETUTCDATE()) )) as totAddlDays 
    FROM  AdditionalInformation 
     GROUP BY ParentFormId) addl
    ON addl.ParentFormId = PAF.ProjectActionFormId 
     LEFT JOIN 
    (SELECT 
     ParentFormId, 
     CASE WHEN  count(ParentFormId) > 0 THEN 'Yes'
       ELSE null
      END as cmt
    FROM  AdditionalInformation 
    where AEComment is not null or LenderComment is not null
     GROUP BY ParentFormId) addlCom
    ON addlCom.ParentFormId = PAF.ProjectActionFormId 
  
	inner join (select a.UserID,UserName from HCP_Authentication a 
				join webpages_UsersInRoles ur on a.userid = ur.userid 
				join webpages_roles r on ur.RoleId = r.RoleId
				and r.RoleName = 'AccountExecutive') au
	on au.Username = parentChildTask.Assignedby
	inner join (select pm.*,ad.Email from HUD_Project_Manager pm
				join Address ad on pm.addressid = ad.addressid) pm_ad
	on au.Username = pm_ad.Email
   INNER JOIN @AeList AE
    ON AE.HUD_Project_Manager_ID = pm_ad.HUD_Project_Manager_Id
   
   inner join HUD_WorkLoad_Manager_Portfolio_Manager wpm
   on wpm.HUD_Project_Manager_ID = pm_ad.HUD_Project_Manager_ID
   INNER JOIN HUD_Project_Manager PM
    ON pm_ad.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
   INNER JOIN HUD_WorkLoad_Manager WLM
    ON wpm.HUD_WorkLoad_Manager_ID = WLM.HUD_WorkLoad_Manager_ID
   INNER JOIN HCP_Project_Action HPA 
    ON HPA.ProjectActionID = PAF.ProjectActionTypeId
   INNER JOIN PAMProjectActionTypes PAMType
    ON PAMType.Name = HPA.ProjectActionName
   INNER JOIN @SelectedProjectActions SPA
    ON SPA.PamProjectActionId = PAMType.PamProjectActionId
   --LEFT JOIN #TaskReAssignment TRA
   -- ON TRA.FromAeId = UWM.UserID AND TRA.TaskId = PAF.MytaskId 
   WHERE --NCR.SubmittedDate BETWEEN @StartDate AND @EndDate
   ((@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND  PAF.CreatedOn>= @StartDate and PAF.CreatedOn< DATEADD(day,1,@EndDate))
   --and lf.FHA_EndDate is null
   OR
   ((@StartDate IS NULL AND @EndDate IS NULL) AND PAF.CreatedOn <= GETUTCDATE())) --and lf.FHA_EndDate is null
   --AND TRA.FromAeId is null
   order by PAF.CreatedOn asc
END

  END

  if(@UserRole = 'InternalSpecialOptionUser')
        Begin
			SELECT distinct a.FHANumber, 
           a.PropertyId , 
           a.PropertyName, 
           a.HudProjectManagerName ,
           a.HudWorkloadManagerName ,
           (case when ParentChildInstanceId is not null then ProjectActionName + '(RAI)' else ProjectActionName end) as ProjectActionName,
           a.Comment ,
           a.ProjectActionSubmitDate ,
           a.ProjectActionCompletionDate ,
           a.ProjectActionDaysToComplete ,
           a.IsProjectActionOpen ,
           a.RequestedAmount ,
           a.ApprovedAmount ,
           a.LenderUserName ,
           a.LenderEmail,
           a.Id ,
           a.TaskId ,
           a.ReAssignedTo ,
           a.ReAssignedDate ,
           a.ReAssignedBy ,
           a.RequestStatus ,
           a.ProcessedBy ,
           a.AdditionalInfoCount,
           a.ParentChildInstanceId,
           (case when ParentChildInstanceId is not null then 'Yes' else 'NA' end) as RAI,
		   ReAssignedFrom,
		   HUDPM 
         FROM #PAMReport a
                          -- Join User_Lender ul on a.FHANumber = ul.FHANumber 
                           where ( @Status is null or IsProjectActionOpen = @Status) and a.FHANumber not in('000-00001','000-00002')
               -- and ul.[User_ID] = @userId  
         ORDER BY ProjectActionSubmitDate asc
        End
        Else
		Begin
			SELECT distinct FHANumber, 
       PropertyId , 
       PropertyName, 
       HudProjectManagerName ,
       HudWorkloadManagerName ,
       (case when ParentChildInstanceId is not null then ProjectActionName + '(RAI)' else ProjectActionName end) as ProjectActionName,
       Comment ,
       ProjectActionSubmitDate ,
       ProjectActionCompletionDate ,
       ProjectActionDaysToComplete ,
       IsProjectActionOpen ,
       RequestedAmount ,
       ApprovedAmount ,
       LenderUserName ,
       LenderEmail,
       (case when ParentChildInstanceId is not null then ParentChildInstanceId else id end) as Id ,
       TaskId ,
       ReAssignedTo ,
       ReAssignedDate ,
       ReAssignedBy ,
       RequestStatus ,
       ProcessedBy ,
       AdditionalInfoCount,
       ParentChildInstanceId,
       (case when ParentChildInstanceId is not null then 'Yes' else 'NA' end) as RAI,
	    ReAssignedFrom,
		HUDPM
       into #CompletedDateNull
      FROM #PAMReport 
      where (( @Status is null or IsProjectActionOpen = @Status)  and ProjectActionCompletionDate is null) and FHANumber not in('000-00001','000-00002')
      ORDER BY ProjectActionCompletionDate asc,ProjectActionSubmitDate

			SELECT distinct FHANumber, 
			   PropertyId , 
			   PropertyName, 
			   HudProjectManagerName ,
			   HudWorkloadManagerName ,
			   (case when ParentChildInstanceId is not null then ProjectActionName + '(RAI)' else ProjectActionName end) as ProjectActionName,
			   Comment ,
			   ProjectActionSubmitDate ,
			   ProjectActionCompletionDate ,
			   ProjectActionDaysToComplete ,
			   IsProjectActionOpen ,
			   RequestedAmount ,
			   ApprovedAmount ,
			   LenderUserName ,
			   LenderEmail,
			   (case when ParentChildInstanceId is not null then ParentChildInstanceId else id end) as Id ,
			   TaskId ,
			   ReAssignedTo ,
			   ReAssignedDate ,
			   ReAssignedBy ,
			   RequestStatus ,
			   ProcessedBy ,
			   AdditionalInfoCount,
			   ParentChildInstanceId,
			   (case when ParentChildInstanceId is not null then 'Yes' else 'NA' end) as RAI ,
			   ReAssignedFrom,
			   HUDPM
			   into #CompletedDateNotNull
			  FROM #PAMReport 
			  where ( ( @Status is null or IsProjectActionOpen = @Status) and ProjectActionCompletionDate is not null) and FHANumber not in('000-00001','000-00002')
			  ORDER BY ProjectActionCompletionDate desc

			  update #CompletedDateNotNull
				set ProjectActionDaysToComplete = DATEDIFF( day ,ProjectActionSubmitDate,ProjectActionCompletionDate);

			  WITH cte AS 
				  (
					select * from #CompletedDateNull
				  ),
				  cte2 AS 
				  (
						select * from #CompletedDateNotNull
				  )

	  
			  SELECT ROW_NUMBER() OVER (ORDER BY cte.ProjectActionCompletionDate,ProjectActionSubmitDate) AS ID, * FROM cte
			  UNION ALL
			  SELECT ROW_NUMBER() OVER (ORDER BY cte2.ProjectActionCompletionDate desc) AS ID,* FROM cte2 
	   END
      
      --select * from #CompletedDateNull

      --union all
      --select * from #CompletedDateNotNull
END

--exec [dbo].[usp_HCP_GetPAMReportDetail] 50,null,null,null,null,'4687','AccountExecutive',''

----exec [dbo].[usp_HCP_GetPAMReportDetail] null,null,null,null,'0,2','4452','HUDAdmin','5772'


--SELECT DATEDIFF( day , '2016-08-03 04:00:00.000 ',GETUTCDATE() )