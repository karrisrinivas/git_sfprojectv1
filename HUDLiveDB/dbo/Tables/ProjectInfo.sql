﻿
CREATE TABLE [dbo].[ProjectInfo](
	[ProjectInfoID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyID] [int] NOT NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[ProjectName] [nvarchar](100) NULL,
	[Property_AddressID] [int] NULL,
	[LenderID] [int] NULL,
	[ServicerID] [int] NULL,
	[ManagementID] [int] NULL,
	[HUD_WorkLoad_Manager_ID] [int] NULL,
	[HUD_Project_Manager_ID] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Primary_Loan_Code] [int] NULL,
	[Initial_Endorsement_Date] [datetime] NULL,
	[Final_Endorsement_Date] [datetime] NULL,
	[Field_Office_Status_Name] [nvarchar](35) NULL,
	[Soa_Code] [nvarchar](5) NULL,
	[Soa_Numeric_Name] [nvarchar](50) NULL,
	[Soa_Description_Text] [nvarchar](100) NULL,
	[Troubled_Code] [nvarchar](25) NULL,
	[Troubled_status_update_date] [nvarchar](100) NULL,
	[Reac_Last_Inspection_Score] [nvarchar](30) NULL,
	[Reac_Last_Inspection_Date] [datetime] NULL,
	[Mddr_In_Default_Status_Name] [nvarchar](15) NULL,
	[Is_Active_Dec_Case_Ind] [nvarchar](5) NULL,
	[Original_Loan_Amount] [decimal](19, 2) NULL,
	[Original_Interest_Rate] [decimal](4, 2) NULL,
	[Amortized_Unpaid_Principal_Bal] [decimal](19, 2) NULL,
	[Is_Nursing_Home_Ind] [nvarchar](15) NULL,
	[Is_Board_And_Care_Ind] [nvarchar](15) NULL,
	[Is_Assisted_Living_Ind] [nvarchar](15) NULL,
	[Deleted_Ind] [bit] NULL,
	[Lender_AddressID] [int] NULL,
	[Lender_Mortgagee_Name] [nvarchar](80) NULL,
	[Servicer_AddressID] [int] NULL,
	[Servicing_Mortgagee_Name] [nvarchar](80) NULL,
	[Owner_AddressID] [int] NULL,
	[Owner_Company_Type] [nvarchar](25) NULL,
	[Owner_Legal_Structure_Name] [nvarchar](50) NULL,
	[Owner_Organization_Name] [varchar](80) NULL,
	[Owner_Contact_AddressID] [int] NULL,
	[Owner_Contact_Indv_Fullname] [nvarchar](50) NULL,
	[Owner_Contact_Indv_Title] [nvarchar](50) NULL,
	[Mgmt_Agent_AddressID] [int] NULL,
	[Mgmt_Agent_Company_Type] [nvarchar](25) NULL,
	[Mgmt_Agent_org_name] [nvarchar](80) NULL,
	[Mgmt_Contact_AddressID] [int] NULL,
	[Mgmt_Contact_FullName] [nvarchar](50) NULL,
	[Mgmt_Contact_Indv_Title] [nvarchar](50) NULL,
	[Portfolio_Number] [nvarchar](15) NULL,
	[Portfolio_Name] [nvarchar](100) NULL,
	[CreditReview] [nvarchar](25) NULL,
 CONSTRAINT [PK_ProjectInfo] PRIMARY KEY CLUSTERED 
(
	[ProjectInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProjectInfo]  WITH CHECK ADD  CONSTRAINT [FK_ProjectInfo_HUD_Project_Manager] FOREIGN KEY([HUD_Project_Manager_ID])
REFERENCES [dbo].[HUD_Project_Manager] ([HUD_Project_Manager_ID])
GO

ALTER TABLE [dbo].[ProjectInfo] CHECK CONSTRAINT [FK_ProjectInfo_HUD_Project_Manager]
GO

ALTER TABLE [dbo].[ProjectInfo]  WITH CHECK ADD  CONSTRAINT [FK_ProjectInfo_HUD_Workload_Manager] FOREIGN KEY([HUD_WorkLoad_Manager_ID])
REFERENCES [dbo].[HUD_WorkLoad_Manager] ([HUD_WorkLoad_Manager_ID])
GO

ALTER TABLE [dbo].[ProjectInfo] CHECK CONSTRAINT [FK_ProjectInfo_HUD_Workload_Manager]
GO

ALTER TABLE [dbo].[ProjectInfo]  WITH CHECK ADD  CONSTRAINT [FK_ProjectInfo_LenderInfo] FOREIGN KEY([LenderID])
REFERENCES [dbo].[LenderInfo] ([LenderID])
GO

ALTER TABLE [dbo].[ProjectInfo] CHECK CONSTRAINT [FK_ProjectInfo_LenderInfo]
GO

ALTER TABLE [dbo].[ProjectInfo]  WITH CHECK ADD  CONSTRAINT [FK_ProjectInfo_PropertyInfo_iREMS] FOREIGN KEY([PropertyID])
REFERENCES [dbo].[PropertyInfo_iREMS] ([PropertyID])
GO

ALTER TABLE [dbo].[ProjectInfo] CHECK CONSTRAINT [FK_ProjectInfo_PropertyInfo_iREMS]
GO

