﻿
CREATE TABLE [dbo].[HUDAccountExecutiveTitle](
	[AccountExecutiveTitleID] [int] IDENTITY(1,1) NOT NULL,
	[AccountExecutiveTitle] [nvarchar](100) NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_HUDAccountExecutiveTitle] PRIMARY KEY CLUSTERED 
(
	[AccountExecutiveTitleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

