﻿
CREATE TABLE [dbo].[ReviewerTaskAssignment](
	[ReviewerTaskAssignmentId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[FHANumber] [nvarchar](20) NOT NULL,
	[ReviwerUserId] [int] NOT NULL,
	[Status] [int] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ReviewerTaskAssignment] PRIMARY KEY CLUSTERED 
(
	[ReviewerTaskAssignmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

