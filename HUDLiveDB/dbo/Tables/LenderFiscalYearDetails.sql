﻿
CREATE TABLE [dbo].[LenderFiscalYearDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[LenderId] [int] NOT NULL,
	[FiscalYearEndMonth] [int] NOT NULL,
	[Q1] [int] NOT NULL,
	[Q2] [int] NOT NULL,
	[Q3] [int] NOT NULL,
	[Q4] [int] NOT NULL,
 CONSTRAINT [PK_LenderFiscalYearDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

