﻿
CREATE TABLE [dbo].[HCP_PageType](
	[PageTypeId] [tinyint] NOT NULL,
	[PageTypeDescription] [nvarchar](50) NOT NULL,
	[ModuleId] [int] NULL,
	[Orderby] [int] NULL,
 CONSTRAINT [PK_HCP_PageType1] PRIMARY KEY CLUSTERED 
(
	[PageTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

