﻿
CREATE TABLE [dbo].[Lender_DataUpload_Live](
	[LDP_ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectName] [nvarchar](100) NULL,
	[ServiceName] [nvarchar](100) NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[LenderID] [int] NOT NULL,
	[Servicer_ID] [int] NULL,
	[OperatorOwner] [nvarchar](25) NULL,
	[PeriodEnding] [datetime] NOT NULL,
	[MonthsInPeriod] [int] NULL,
	[FinancialStatementType] [nvarchar](25) NULL,
	[UnitsInFacility] [int] NULL,
	[PropertyID] [int] NOT NULL,
	[OperatingCash] [decimal](19, 2) NULL,
	[Investments] [decimal](19, 2) NULL,
	[ReserveForReplacementEscrowBalance] [decimal](19, 2) NULL,
	[AccountsReceivable] [decimal](19, 2) NULL,
	[CurrentAssets] [decimal](19, 2) NULL,
	[CurrentLiabilities] [decimal](19, 2) NULL,
	[TotalRevenues] [decimal](19, 2) NULL,
	[RentLeaseExpense] [decimal](19, 2) NULL,
	[DepreciationExpense] [decimal](19, 2) NULL,
	[AmortizationExpense] [decimal](19, 2) NULL,
	[TotalExpenses] [decimal](19, 2) NULL,
	[NetIncome] [decimal](19, 2) NULL,
	[ReserveForReplacementDeposit] [decimal](19, 2) NULL,
	[FHAInsuredPrincipalPayment] [decimal](19, 2) NULL,
	[FHAInsuredInterestPayment] [decimal](19, 2) NULL,
	[MortgageInsurancePremium] [decimal](19, 2) NULL,
	[DateInserted] [datetime] NOT NULL,
	[HUD_Project_Manager_ID] [int] NULL,
	[LDI_ID] [int] NOT NULL,
	[ReserveForReplacementBalancePerUnit] [decimal](19, 2) NULL,
	[WorkingCapital] [decimal](19, 2) NULL,
	[DebtCoverageRatio] [decimal](19, 2) NULL,
	[DaysCashOnHand] [decimal](19, 2) NULL,
	[DaysInAcctReceivable] [decimal](19, 2) NULL,
	[AvgPaymentPeriod] [decimal](19, 2) NULL,
	[WorkingCapitalScore] [decimal](19, 2) NULL,
	[DebtCoverageRatioScore] [decimal](19, 2) NULL,
	[DaysCashOnHandScore] [decimal](19, 2) NULL,
	[DaysInAcctReceivableScore] [decimal](19, 2) NULL,
	[AvgPaymentPeriodScore] [decimal](19, 2) NULL,
	[ScoreTotal] [decimal](19, 2) NULL,
	[ModifiedBy] [int] NULL,
	[OnBehalfOfBy] [int] NULL,
	[UserID] [int] NOT NULL,
	[Deleted_Ind] [bit] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Lender_DataUpload_Live] PRIMARY KEY CLUSTERED 
(
	[LDP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Lender_DataUpload_Live]  WITH CHECK ADD  CONSTRAINT [FK_Lender_DataUpload_Live_FHAInfo_iREMS] FOREIGN KEY([FHANumber])
REFERENCES [dbo].[FHAInfo_iREMS] ([FHANumber])
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live] CHECK CONSTRAINT [FK_Lender_DataUpload_Live_FHAInfo_iREMS]
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live]  WITH CHECK ADD  CONSTRAINT [FK_Lender_DataUpload_Live_HCP_Authentication] FOREIGN KEY([UserID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live] CHECK CONSTRAINT [FK_Lender_DataUpload_Live_HCP_Authentication]
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live]  WITH CHECK ADD  CONSTRAINT [FK_Lender_DataUpload_Live_LenderInfo] FOREIGN KEY([LenderID])
REFERENCES [dbo].[LenderInfo] ([LenderID])
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live] CHECK CONSTRAINT [FK_Lender_DataUpload_Live_LenderInfo]
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live]  WITH CHECK ADD  CONSTRAINT [FK_Lender_DataUpload_Live_LenderInfo1] FOREIGN KEY([LenderID])
REFERENCES [dbo].[LenderInfo] ([LenderID])
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live] CHECK CONSTRAINT [FK_Lender_DataUpload_Live_LenderInfo1]
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live]  WITH CHECK ADD  CONSTRAINT [FK_Lender_DataUpload_Live_ServicerInfo] FOREIGN KEY([Servicer_ID])
REFERENCES [dbo].[ServicerInfo] ([ServicerID])
GO

ALTER TABLE [dbo].[Lender_DataUpload_Live] CHECK CONSTRAINT [FK_Lender_DataUpload_Live_ServicerInfo]
GO

