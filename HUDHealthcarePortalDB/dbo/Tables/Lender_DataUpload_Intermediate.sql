﻿CREATE TABLE [dbo].[Lender_DataUpload_Intermediate](
	[ProjectName] [nvarchar](100) NULL,
	[ServiceName] [nvarchar](100) NULL,
	[FHANumber] [nvarchar](15) NULL,
	[LenderID] [int] NULL,
	[OperatorOwner] [nvarchar](25) NULL,
	[PeriodEnding] [nvarchar](25) NULL,
	[MonthsInPeriod] [nvarchar](5) NULL,
	[FinancialStatementType] [nvarchar](25) NULL,
	[UnitsInFacility] [int] NULL,
	[OperatingCash] [nvarchar](25) NULL,
	[Investments] [nvarchar](25) NULL,
	[ReserveForReplacementEscrowBalance] [decimal](19, 2) NULL,
	[AccountsReceivable] [decimal](19, 2) NULL,
	[CurrentAssets] [decimal](19, 2) NULL,
	[CurrentLiabilities] [decimal](19, 2) NULL,
	[TotalRevenues] [decimal](19, 2) NULL,
	[RentLeaseExpense] [decimal](19, 2) NULL,
	[DepreciationExpense] [decimal](19, 2) NULL,
	[AmortizationExpense] [decimal](19, 2) NULL,
	[TotalExpenses] [decimal](19, 2) NULL,
	[NetIncome] [decimal](19, 2) NULL,
	[ReserveForReplacementDeposit] [decimal](19, 2) NULL,
	[FHAInsuredPrincipalPayment] [decimal](19, 2) NULL,
	[FHAInsuredInterestPayment] [decimal](19, 2) NULL,
	[MortgageInsurancePremium] [decimal](19, 2) NULL,
	[PropertyID] [int] NULL,
	[LDI_ID] [int] IDENTITY(1,1) NOT NULL,
	[ReserveForReplacementBalancePerUnit] [decimal](19, 2) NULL,
	[WorkingCapital] [decimal](19, 2) NULL,
	[DebtCoverageRatio] [decimal](19, 2) NULL,
	[DaysCashOnHand] [decimal](19, 2) NULL,
	[DaysInAcctReceivable] [decimal](19, 2) NULL,
	[AvgPaymentPeriod] [decimal](19, 2) NULL,
	[WorkingCapitalScore] [decimal](19, 2) NULL,
	[DebtCoverageRatioScore] [decimal](19, 2) NULL,
	[DaysCashOnHandScore] [decimal](19, 2) NULL,
	[DaysInAcctReceivableScore] [decimal](19, 2) NULL,
	[AvgPaymentPeriodScore] [decimal](19, 2) NULL,
	[HasCalculated] [bit] NULL,
	[ScoreTotal] [decimal](19, 2) NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[UserID] [int] NOT NULL,
	[DataInserted] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Lender_DataUpload_Intermediate] PRIMARY KEY CLUSTERED 
(
	[LDI_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Lender_DataUpload_Intermediate] ADD  CONSTRAINT [DF_Lender_DataUpload_Intermediate_DataInserted]  DEFAULT (getutcdate()) FOR [DataInserted]
GO




