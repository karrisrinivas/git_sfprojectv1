namespace HUDHealthcarePortal.CustomModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    //naveen
    public partial class exceltodb : DbContext
    {
        public exceltodb()
            : base("name=exceltodb")
        {
        }

        public virtual DbSet<Loans_82817_New> Loans_82817_New { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Loans_82817_New>()
                .Property(e => e.amortized_unpaid_principal_bal)
                .HasPrecision(19, 4);
        }
    }
}
