﻿
using System.Web.Mvc;

namespace HUDHealthcarePortal.Filters
{
    /// <summary>
    /// Trims the model properties entered by the user in the UI views.
    /// </summary>
    public class TrimModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                return null;
            return valueResult.AttemptedValue.Trim();
        }
    }
}