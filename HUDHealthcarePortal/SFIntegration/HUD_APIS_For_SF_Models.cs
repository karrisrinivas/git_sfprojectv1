﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HUDHealthcarePortal.SFIntegration.HUDAPIModels
{
    public class abcClass
    {
        public string Name { get; set; }
        public string SirName { get; set; }
    }

    public class SFFHAReq_TaskUpdatePayoadToHud
    {
        public string AssignedToUserName { get; set; }
        public DateTime AssignedDate { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int IsCreditReviewAttachComplete { get; set; }
        public string CreditreviewComments { get; set; }
        public DateTime CreditReviewDate { get; set; }
        public string Portfolio_Name { get; set; }
        public int Portfolio_Number { get; set; }
        public int IsPortfolioComplete { get; set; }
        public string PortfolioComments { get; set; }
        public string FHANumber { get; set; }
        public int IsFhaInsertComplete { get; set; }
        public int RequestStatus { get; set; }
        public int IsReadyForApplication { get; set; }
        public int Property__c { get; set; }
    }

    public class UpdateDemoController : ApiController
    {
        public HttpResponseMessage Get(int id, [FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }
    }

    //payload that comes from SF for FirmCommittment from Internal User where it creates a similar record in HUD system as well
    //Model.TaskGuid, taskxref = Model.taskxrefId, pagetypeid = Model.pageTypeId from OPA From Data in TASK table
    public class SFReq_FirmCommittment
    {
        public string TaskGuid { get; set; }
        public string ProjectActionFormId { get; set; }
        //public string PageTypeId { get; set; }
        //public int FilesUploadedCount { get; set; }
        public string AssignedByUserName { get; set; }

        public string FCUploadedFilesData { get; set; }
    }

    public class SFReq_FirmCommFileUploadByIntUser
    {
        public string TaskGuid { get; set; }
        public string TaskxrefId { get; set; }
       

    }

    #region IMPORTANT Notes
    /*
     select taskinstanceid,MytaskId, * from HCP_Live_db_Prod.dbo.OPAForm where FhaNumber='666-00039'
select TaskXrefid, * from prod_taskxref where taskinstanceid='966B96B8-0C98-4851-8BF8-C659229C6191'
TaskXrefid=C7EF2068-C721-4E53-BA04-236A7CE2BE76
HudOrLndrFCTaskinstaneId => value is from SF where it is contained int the payload to SF when Lender has given Firmcommitment Response to SF
the payload contians text=agreeed, HudOrLndrFCTaskinstaneId, AgreementDate
    */
    #endregion

    public class SFReq_ProdProc_HudReviewComp
    {
        //, string associatedUserName, int associatedUserId
        public string HudOrLndrFCTaskinstaneId { get; set; }
        public string associatedUserName { get; set; }
        //public string associatedUserId { get; set; }

    }

    public class SFReq_ProdProc_HudFMUploadFIles
    {
        //, string associatedUserName, int associatedUserId
        public string ProjectActionFormId { get; set; }
        public string ProjectActionTaskinstanceid { get; set; }
        public string FolderKeyName { get; set; }
        public string userName { get; set; }
        public string Docid { get; set; }
        public string DocTypeId { get; set; }
        public string FileName { get; set; }
        public double FileSize { get; set; }
        public string RoleName { get; set; }

    }

}