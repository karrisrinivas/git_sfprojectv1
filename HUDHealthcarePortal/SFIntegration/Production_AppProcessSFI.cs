﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HUDHealthcarePortal.Controllers.Production;
using HUDHealthcarePortal.Model;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using HUDHealthcarePortal.Core;
using Model.Production;

namespace HUDHealthcarePortal.SFIntegration
{
    public class Production_AppProcessSFI
    {
        string _sfLenderId = string.Empty;
        static string _authToken = string.Empty;
        static string _instanceUrl = string.Empty;
        static string _logFilePath = @"D:\HUDPortal_SVN_Repositories\SFLogs\FHAReqSFLogs.txt";

        public Production_AppProcessSFI()
        {
            string LOGIN_ENDPOINT = "https://login.salesforce.com/services/oauth2/token?grant_type=password";
            string API_ENDPOINT = "/services/data";

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");


            #region TOKEN Generation
            String jsonResponse;
            using (var client = new HttpClient())
            {
                var request = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        {"grant_type", "password"},
                        {"client_id", "3MVG9GYWKbMgBvbwdIehxcSveFeWrAGkF.oXlao.KrviEcE6hePjOXXxwyIM_MKTzIzc5zhq1XakVkWKlD7lO"},
                        {"client_secret", "5E9918A700C2BEE67D2D8526C032FD83DCFC2E8FB881D454172BA5F378E27EE1"},
                        {"username", "dev@yitsol.com"},
                        {"password", "test123456"}
                    }
                );
                request.Headers.Add("X-PrettyPrint", "1");
                var response = client.PostAsync(LOGIN_ENDPOINT, request).Result;
                jsonResponse = response.Content.ReadAsStringAsync().Result;

                sbSFLogger.Append("\r\n>>>Hand Shake Done!");
            }

            //Console.WriteLine($"Response: {jsonResponse}");
            var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonResponse);
            _authToken = values["access_token"];
            _instanceUrl = values["instance_url"];
            #endregion

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }
            sbSFLogger.Append("\r\n>>>Token Generated! : " + _authToken);
        }

        public static void test1()
        {

        }

        //sfintegration
        public static string PushFHAReqToSF(FHANumberRequestViewModel model)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            string fhareqsfid = string.Empty;
            string hudLenderName = model.LenderName.Trim();//do not search by lender name, search by HUD Lender ID  "GREYSTONE SERVICING CORP INC"
            string hudLenderID = model.LenderId.ToString(); //73723=GREYSTONE SERVICING CORP INC from lenderinfo table
            string sfAccountType = "Lender";
            string sfLenderID = string.Empty;

            string LendersUserContactEmail = model.LenderContactEmail.Trim();
            string lenderUserLastName = UserPrincipal.Current.UserName;
            string lenderUserPhone = model.LenderContactPhone;
            string sfLenderUserID = string.Empty;
            StringBuilder sbSFLogger = new StringBuilder();
            string hudFHARequestID = string.Empty;



            #region SEARCCHING LENDERS Name in SF Account
            //get the lender acocunt if exists or not
            using (var client = new HttpClient())
            {
                sbSFLogger.Append("\r\n>>>Searach Initiated for Hud Lender Name (" + hudLenderName + ") with Hud Lenderid ( " + hudLenderID + ")");

                //do not search by Lender name, search by HUD Lender ID
                string restQuery = _instanceUrl + "/services/data/v45.0/query?q=SELECT+ID+FROM+ACCOUNT+WHERE+SourceSystemID__c='" + hudLenderID + "'+AND+TYPE='" + sfAccountType + "'";

                var request = new HttpRequestMessage(HttpMethod.Get, restQuery);
                request.Headers.Add("Authorization", "Bearer " + _authToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Add("X-PrettyPrint", "1");

                using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                {
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                        /////Note:need a parser to convert resultObj1 => resultObj
                        var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFAccontResModel>(responseData);
                        if (resultObj1.totalSize > 0 && resultObj1.records.Count > 0)
                        {
                            var tbl = resultObj1.records;
                            foreach (var rec in tbl)
                            {
                                if (rec.Id != null && !string.IsNullOrEmpty(rec.Id))
                                {
                                    sfLenderID = rec.Id.ToString();

                                    sbSFLogger.Append("\r\n>>>Existing Lender SFID:" + sfLenderID + " !");

                                    break;
                                }
                            }
                        }
                        else
                            sbSFLogger.Append("\r\n>>>No Records found");

                    }
                    else
                        sbSFLogger.Append("\r\n>>>Search Call was not success for some reason!\n");

                }//searching finished

            }
            #endregion

            #region CREATING Of LENDERS Name in SF Account
            if (sfLenderID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>Lender Account Details Saving Initiated in SF");

                using (var client = new HttpClient())
                {
                    string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Account";
                    client.BaseAddress = new Uri(restPostAccQuery);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                    Uri u = new Uri(restPostAccQuery);

                    SFLenderAccCreateObject obj2 = new SFLenderAccCreateObject();
                    //name-lender name, type-Lender, AccountNumber-returns in response,SourceSystemID__c='73723'
                    obj2.Name = hudLenderName;
                    obj2.Type = sfAccountType;
                    obj2.SourceSystemID__c = hudLenderID;

                    var json1 = new JavaScriptSerializer().Serialize(obj2);

                    HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                    sbSFLogger.Append("\r\n>>>Lender Account Create Payoad : " + json1.ToString());

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = u,
                        Content = c
                    };

                    using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                    {
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                            var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFLenderAccCreateResObj>(responseData);
                            if (resultObj1.success)
                            {
                                if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                {
                                    sfLenderID = resultObj1.id.ToString();

                                    sbSFLogger.Append("\r\n>>>Lender Account Details SAved and Account ID:" + sfLenderID);
                                }
                            }
                        }
                        else
                            sbSFLogger.Append("\r\n>>>Some thing went wrong while saving Lender Account Details in SF!");

                    }

                }

            }
            #endregion

            #region SEARCCHING LENDERS USER Contact Email in SF CONTACT
            //get the lender acocunt if exists or not
            using (var client = new HttpClient())
            {
                sbSFLogger.Append("\r\n>>>Lender Users Contact Details Search Initiated in SF for :" + LendersUserContactEmail);

                string restQuery = _instanceUrl + "/services/data/v45.0/query?q=SELECT+ID,AccountId,Email+FROM+Contact+WHERE+AccountId='" + sfLenderID + "'+AND+Email='" + LendersUserContactEmail + "'";

                var request = new HttpRequestMessage(HttpMethod.Get, restQuery);
                request.Headers.Add("Authorization", "Bearer " + _authToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Add("X-PrettyPrint", "1");

                using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                {
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                        /////Note:need a parser to convert resultObj1 => resultObj
                        var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFAccontResModel>(responseData);
                        if (resultObj1.totalSize > 0 && resultObj1.records.Count > 0)
                        {
                            var tbl = resultObj1.records;
                            foreach (var rec in tbl)
                            {
                                if (rec.Id != null && !string.IsNullOrEmpty(rec.Id))
                                {
                                    sfLenderUserID = rec.Id.ToString();

                                    sbSFLogger.Append("\r\n>>>Lender Users UserID-SFID:" + sfLenderUserID);

                                    break;
                                }
                            }
                        }
                        else
                            sbSFLogger.Append("\r\n>>>Did not find Lenders Users Contact in SF");

                    }
                    else
                        sbSFLogger.Append("\r\n>>>Some things went wrong while searching Lenders Contact in SF System");

                }//searching finished

            }
            #endregion

            #region CREATING LENDERS USSERs User Contact in SF CONTACT
            if (sfLenderUserID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>Lenders Users Contact Saving in SF Initiated");

                using (var client = new HttpClient())
                {
                    string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Contact";
                    client.BaseAddress = new Uri(restPostAccQuery);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                    Uri u = new Uri(restPostAccQuery);

                    //AccountId = sfLenderID, Email - lenders user email, Phone-lenders users phone, Name-lenders users Last name(fullname)
                    SFLendersUsersCreateObject obj2 = new SFLendersUsersCreateObject();
                    obj2.AccountId = sfLenderID;
                    obj2.Email = LendersUserContactEmail;
                    obj2.Phone = lenderUserPhone;
                    obj2.LastName = lenderUserLastName;
                    //obj2.SourceSystemID__c = "3318";//HUD System Lender User ID 

                    var json1 = new JavaScriptSerializer().Serialize(obj2);

                    HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                    sbSFLogger.Append("\r\n>>>Lenders Users Contact payload : " + json1.ToString());

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = u,
                        Content = c
                    };

                    using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                    {
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                            var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFLenderAccCreateResObj>(responseData);
                            if (resultObj1.success)
                            {
                                if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                {
                                    sfLenderUserID = resultObj1.id.ToString();

                                    sbSFLogger.Append("\r\n>>>Lenders Users Contact Saved in SF system with AccountID:" + sfLenderUserID);
                                }
                            }
                        }
                        else
                            sbSFLogger.Append("\r\n>>>Something went wrong while saving Lenders Users Contact in SF System");

                    }

                }

            }
            #endregion

            #region Creating FHA Request
            if (!sfLenderUserID.Equals(string.Empty) && !sfLenderID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>Saving FHA Requestdetails into SF System initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/FHA_Request__c";
                        client.BaseAddress = new Uri(restPostAccQuery);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                        Uri u = new Uri(restPostAccQuery);

                        ////basic submission payload:
                        //{
                        //    "FHA_Number__c":"00aa8662-7a4e-4e55-ac85-320b83a2ff07","Name":"testporjFHANumber1","Comments__c":"Hello world"
                        //        ,"Lender__c":"0014T000004HkkgQAC","Is_New_Or_Exist_Or_NAPortfolio__c":true,"Skilled_Nursing_Beds__c":"1"
                        //        ,"Skilled_Nursing_Units__c":"1","Is_Master_Lease_Proposed__c":false,"Status__c":"1","Loan_Amount__c":"100"
                        //        ,"Proposed_Interest_Rate__c":"2","Lender_Contact__c":"0034T000006Lw0aQAC","Project_Type__c":"6"
                        //        ,"Current_Loan_Type__c":"0","Loan_Type__c":"1","Borrower_Type__c":"2"
                        //}

                        var modelForDomains = new FHANumberRequestViewModel();
                        FHARequestController.SetModelValues(modelForDomains);


                        SFFHAReqCreateObj obj2 = new SFFHAReqCreateObj();

                        //obj2.FHA_Number__c = model.FHANumberRequestId.ToString();
                        obj2.Name = model.ProjectName;
                        obj2.Comments__c = model.Comments;
                        obj2.Lender__c = sfLenderID;//---------------------------to pass SF Lender ID----------------

                        //model.IsNewOrExistOrNAPortfolio, Model.AllPortfolioStatus
                        if (model.IsNewOrExistOrNAPortfolio.Equals(1))
                            obj2.Is_New_Or_Exist_Or_NAPortfolio__c = true;
                        else
                            obj2.Is_New_Or_Exist_Or_NAPortfolio__c = false;

                        if (model.IsMidLargePortfolio.Equals(true))
                            obj2.Is_Mid_Large_Portfolio__c = true;//mediuim or big portfolio
                        else
                            obj2.Is_Mid_Large_Portfolio__c = false;//small

                        obj2.Skilled_Nursing_Beds__c = model.SkilledNursingBeds.ToString();
                        obj2.Skilled_Nursing_Units__c = model.SkilledNursingUnits.ToString();

                        if (model.IsMasterLeaseProposed.Equals(1))
                            obj2.Is_Master_Lease_Proposed__c = true;
                        else
                            obj2.Is_Master_Lease_Proposed__c = false;

                        obj2.Status__c = model.RequestStatus.ToString();
                        obj2.Loan_Amount__c = model.LoanAmount.ToString();
                        obj2.Proposed_Interest_Rate__c = model.ProposedInterestRate.ToString();
                        obj2.Lender_Contact__c = sfLenderUserID;//------------------------------

                        //obj2.Project_Type__c = model.ProjectTypeId.ToString();
                        foreach (var item in modelForDomains.AllProjectTypes)
                            if (item.Value.Equals(model.ProjectTypeId.ToString()))
                            {
                                obj2.Project_Type__c = item.Text;
                                break;
                            }


                        foreach (var item in modelForDomains.AllMortgageInsuranceTypes)
                            if (item.Value.Equals(model.LoanTypeId.ToString()))
                            {
                                obj2.Current_Loan_Type__c = item.Text;
                                break;
                            }



                        obj2.Loan_Type__c = model.LoanTypeId.ToString();

                        foreach (var item in modelForDomains.AllBorrowerTypes)
                            if (item.Value.Equals(model.BorrowerTypeId.ToString()))
                            {
                                obj2.Borrower_Type__c = item.Text;
                                break;
                            }

                        obj2.SourceSystemID__c = model.TaskinstanceId.ToString();//TaskInsanceID

                        foreach (var item in modelForDomains.AllCmsStarRatings)
                            if (item.Value.Equals(model.CMSStarRating.ToString()))
                            {
                                obj2.CMS_Star_Rating__c = item.Text;
                                break;
                            }

                        //obj2.SourceSystemID__c = "232";

                        var json1 = new JavaScriptSerializer().Serialize(obj2);

                        HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                        sbSFLogger.Append("\r\n>>>SF FHA Request Payload :" + json1.ToString());

                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = u,
                            Content = c
                        };

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFLenderAccCreateResObj>(responseData);
                                if (resultObj1.success)
                                {
                                    if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                    {
                                        fhareqsfid = resultObj1.id;

                                        sbSFLogger.Append("\r\n>>>FHA Request in HUD with TaskInstanceID: " + model.TaskinstanceId.ToString());
                                        sbSFLogger.Append("\r\n>>>FHA Request Details saved into SF System with FHARequest SFID: " + fhareqsfid);
                                    }
                                }
                            }
                            else
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving FHA Request details into SF System");

                        }

                    }
                }
                catch (Exception ex)
                {

                }

            }
            #endregion

            sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }

            return fhareqsfid;
        }

        //to push Task File Data Paylod to Sales Force
        public static void postTaskFileData(TaskFileModel taskFileModel, string TaskFileOwnerID, string Name, string opaformdata_sfid, string FolderKey)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
            sbSFLogger.Append("\r\n>>>Production Application Task File Data Integration initiated");

            //Note: assuming that Lender details and Lender User Details are already in SF system
            //0014T000004HkkgQAC=sflenderid
            //0034T000006NKfjQAG=sflenderuserid
            //Task File Owner ID=0054T000000F4qfQAC
            TaskFileOwnerID = "0054T000000F4qfQAC";
            Name = "Application Process";
            string sfTaskFileRef = string.Empty;

            #region Creating Task File Data Request
            if (!TaskFileOwnerID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>Task File Data Posting initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Task_File__c";
                        client.BaseAddress = new Uri(restPostAccQuery);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                        Uri u = new Uri(restPostAccQuery);

                        SFTaskFileDataModel obj = new SFTaskFileDataModel();

                        obj.FileName__c = taskFileModel.FileName;
                        obj.SourceSystemId__c = taskFileModel.TaskInstanceId.ToString();//???
                        obj.RoleName__c = taskFileModel.RoleName;
                        obj.DocTypeID__c = taskFileModel.DocTypeID;
                        obj.DocId__c = taskFileModel.DocId;
                        obj.API_upload_status__c = taskFileModel.API_upload_status;
                        obj.Folder_Grid__c = "Section 8: Real Estate";
                        obj.Production_Application_Request__c = opaformdata_sfid;

                        var json1 = new JavaScriptSerializer().Serialize(obj);

                        HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                        sbSFLogger.Append("\r\n>>>Task File Data Payload :" + json1.ToString());

                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = u,
                            Content = c
                        };

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFResObj>(responseData);
                                if (resultObj1.success)
                                {
                                    if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                    {
                                        sfTaskFileRef = resultObj1.id;

                                        sbSFLogger.Append("\r\n>>>Task file associated TaskInstanceID: " + taskFileModel.TaskInstanceId.ToString());
                                        sbSFLogger.Append("\r\n>>>Task File Saved SFID: " + sfTaskFileRef);
                                    }
                                }
                            }
                            else
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving FHA Request details into SF System");

                        }

                    }
                }
                catch (Exception ex)
                {

                }

            }
            #endregion

            sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }
        }

        ////to push Task File Name Change Data Paylod to Sales Force
        //public static void postTaskFileNameChangeData(TaskFileModel taskFileModel, string TaskFileOwnerID, string Name)
        //{
        //    StringBuilder sbSFLogger = new StringBuilder();
        //    sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
        //    sbSFLogger.Append("\r\n>>>Production Application Task File Name Change initiated");

        //    #region SF INTEGRATION META DATA
        //    string LOGIN_ENDPOINT = "https://login.salesforce.com/services/oauth2/token?grant_type=password";
        //    string API_ENDPOINT = "/services/data";
        //    string Username = string.Empty;
        //    string Password = string.Empty;
        //    string Token = string.Empty;
        //    string ClientId = string.Empty;
        //    string ClientSecret = string.Empty;
        //    string _authToken = string.Empty;
        //    string _instanceUrl = string.Empty;
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
        //    #endregion

        //    #region TOKEN Generation
        //    String jsonResponse;
        //    using (var client = new HttpClient())
        //    {
        //        var request = new FormUrlEncodedContent(new Dictionary<string, string>
        //            {
        //                {"grant_type", "password"},
        //                {"client_id", "3MVG9GYWKbMgBvbwdIehxcSveFeWrAGkF.oXlao.KrviEcE6hePjOXXxwyIM_MKTzIzc5zhq1XakVkWKlD7lO"},
        //                {"client_secret", "5E9918A700C2BEE67D2D8526C032FD83DCFC2E8FB881D454172BA5F378E27EE1"},
        //                {"username", "dev@yitsol.com"},
        //                {"password", "test1234"}
        //            }
        //        );
        //        request.Headers.Add("X-PrettyPrint", "1");
        //        var response = client.PostAsync(LOGIN_ENDPOINT, request).Result;
        //        jsonResponse = response.Content.ReadAsStringAsync().Result;

        //        sbSFLogger.Append("\r\n>>>Hand Shake Done!");
        //    }

        //    //Console.WriteLine($"Response: {jsonResponse}");
        //    var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonResponse);
        //    _authToken = values["access_token"];
        //    _instanceUrl = values["instance_url"];

        //    sbSFLogger.Append("\r\n>>>Token Generated! : " + _authToken);
        //    #endregion

        //    //Note: assuming that Lender details and Lender User Details are already in SF system
        //    //0014T000004HkkgQAC=sflenderid
        //    //0034T000006NKfjQAG=sflenderuserid
        //    //Task File Owner ID=0054T000000F4qfQAC
        //    TaskFileOwnerID = "0054T000000F4qfQAC";
        //    Name = "Application Process";
        //    string sfTaskFileNameChangeRef = string.Empty;

        //    #region Creating FHA Request
        //    if (!TaskFileOwnerID.Equals(string.Empty))
        //    {
        //        sbSFLogger.Append("\r\n>>>Task File Data Posting initiated");

        //        try
        //        {

        //            using (var client = new HttpClient())
        //            {
        //                //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
        //                string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Task_File__c";
        //                client.BaseAddress = new Uri(restPostAccQuery);
        //                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
        //                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //                client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

        //                Uri u = new Uri(restPostAccQuery);

        //                SFTaskFileNameChangeDataModel obj = new SFTaskFileNameChangeDataModel();
        //                //FileName, DocTyeID

        //                obj.FileName__c = taskFileModel.FileName;
        //                obj.SourceSystemId__c = taskFileModel.TaskFileId.ToString();  //Primary Key in SF System                     
        //                obj.DocTypeID__c = taskFileModel.DocTypeID;
        //                obj.DocId__c = taskFileModel.DocId;                        

        //                var json1 = new JavaScriptSerializer().Serialize(obj);

        //                HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

        //                sbSFLogger.Append("\r\n>>>Task FileName change Data Payload :" + json1.ToString());

        //                HttpRequestMessage request = new HttpRequestMessage
        //                {
        //                    Method = HttpMethod.Post,
        //                    RequestUri = u,
        //                    Content = c
        //                };

        //                using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
        //                {
        //                    if (responseMessage.IsSuccessStatusCode)
        //                    {
        //                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;

        //                        var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFResObj>(responseData);
        //                        if (resultObj1.success)
        //                        {
        //                            if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
        //                            {
        //                                sfTaskFileNameChangeRef = resultObj1.id;

        //                                sbSFLogger.Append("\r\n>>>Task File Name Change Data saved into SF System with ID: " + sfTaskFileNameChangeRef);
        //                            }
        //                        }
        //                    }
        //                    else
        //                        sbSFLogger.Append("\r\n>>>Something went wrong while saving FHA Request details into SF System");

        //                }

        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //    }
        //    #endregion


        //}

        //to push Property details to sales force
        public static string postPropertyData(Dictionary<string, object> PropertyDic, string TaskFileOwnerID, string Name)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            string sfid = "a054T000003l9ECQAY";
            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
            sbSFLogger.Append("\r\n>>>Production Application OPA saving initiated");

            //Note: assuming that Lender details and Lender User Details are already in SF system
            //0014T000004HkkgQAC=sflenderid
            //0034T000006NKfjQAG=sflenderuserid
            //Task File Owner ID=0054T000000F4qfQAC
            TaskFileOwnerID = "0034T000006NKfjQAG";
            Name = "Application Process";
            string sfTaskFileNameChangeRef = string.Empty;

            #region Creating Property Data Request
            if (!TaskFileOwnerID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>Property Data Posting initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Property__c";
                        client.BaseAddress = new Uri(restPostAccQuery);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                        Uri u = new Uri(restPostAccQuery);

                        PropertyData obj = new PropertyData();

                        foreach (var item in PropertyDic)
                        {
                            if (item.Key.Equals("PropertyName"))
                                obj.Name = item.Value.ToString();
                            if (item.Key.Equals("PropertyAddress.ZIP"))
                                obj.PostalCode__c = item.Value.ToString();
                            if (item.Key.Equals("PropertyAddress.StateCode"))
                                obj.StateCode__c = item.Value.ToString();
                            if (item.Key.Equals("PropertyAddress.AddressLine1"))
                                obj.Street__c = item.Value.ToString();
                            if (item.Key.Equals("PropertyAddress.City"))
                                obj.City__c = item.Value.ToString();


                            obj.Country__c = "United States";
                            obj.CountryCode__c = "USA";
                        }


                        var json1 = new JavaScriptSerializer().Serialize(obj);

                        HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                        sbSFLogger.Append("\r\n>>>Property Data Payload :" + json1.ToString());

                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = u,
                            Content = c
                        };

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFResObj>(responseData);
                                if (resultObj1.success)
                                {
                                    if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                    {
                                        sfid = resultObj1.id;

                                        sbSFLogger.Append("\r\n>>>FHA Requests Property SFID : " + sfid);
                                    }
                                }
                            }
                            else
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving FHA Request details into SF System");

                        }

                    }
                }
                catch (Exception ex)
                {

                }

            }
            #endregion

            sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }

            return sfid;

        }

        //to push OPA Form  to Sales Force
        public static string postOPAData(OPAViewModel AppProcessViewModel, string TaskFileOwnerID, string Name, string projTypeName, string propertysfid)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            string opaform_sfid = "a054T000003l9ECQAY";
            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
            sbSFLogger.Append("\r\n>>>Production Application OPA saving initiated");

            //Note: assuming that Lender details and Lender User Details are already in SF system
            //0014T000004HkkgQAC=sflenderid
            //0034T000006NKfjQAG=sflenderuserid
            //Task File Owner ID=0054T000000F4qfQAC
            TaskFileOwnerID = "0054T000000F4qfQAC";
            Name = "Application Process";
            string sfTaskFileNameChangeRef = string.Empty;

            #region Creating OPA/Production-Constrution-Single Stage Request
            if (!TaskFileOwnerID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>OPA Form Data Posting initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Production_Application_Request__c";
                        client.BaseAddress = new Uri(restPostAccQuery);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                        Uri u = new Uri(restPostAccQuery);

                        OPAFormModel obj = new OPAFormModel();
                        //FileName, DocTyeID
                        //Noe: TaskInstanceId, SequenceId, AssignedBy,StartTime, Notes(servicer comments), TaskStepId, PageTypeId, FhaNumber

                        //obj.FHA_Number__c = AppProcessViewModel.FhaNumber;//234-46527 fha should alrady exits in SF system
                        obj.FHA_Number__c = AppProcessViewModel.FhaNumber;

                        obj.SourceSystemID__c = AppProcessViewModel.TaskInstanceId.ToString();//MaheshOPAFormID???


                        //obj.Servicer_Submission_Date__c = DateTime.Now.ToShortDateString();//SF format 2019-10-14  yyyy-mm-day
                        obj.Servicer_Submission_Date__c = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();

                        obj.Name = AppProcessViewModel.PropertyName;
                        //obj.ProjectActionTypeId__c = AppProcessViewModel.ProjectActionTypeId.ToString();
                        obj.Project_Number__c = AppProcessViewModel.PropertyId.ToString();
                        obj.Property__c = propertysfid;

                        var json1 = new JavaScriptSerializer().Serialize(obj);

                        HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                        sbSFLogger.Append("\r\n>>>Task FileName change Data Payload :" + json1.ToString());

                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = u,
                            Content = c
                        };

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFResObj>(responseData);
                                if (resultObj1.success)
                                {
                                    if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                    {
                                        opaform_sfid = resultObj1.id;

                                        sbSFLogger.Append("\r\n>>>OPAFORM Submit data for Property SFID: " + propertysfid);
                                        sbSFLogger.Append("\r\n>>>Project Action FormID: " + AppProcessViewModel.ProjectActionFormId.ToString());
                                        sbSFLogger.Append("\r\n>>>Project Action TaskInstanceID: " + AppProcessViewModel.TaskInstanceId.ToString());
                                        sbSFLogger.Append("\r\n>>>OPA From Submission SFID: " + opaform_sfid);
                                    }
                                }
                            }
                            else
                            {
                                sbSFLogger.Append("\r\n>>>OPAFORM Submit data for Property SFID: " + propertysfid);
                                sbSFLogger.Append("\r\n>>>Project Action FormID: " + AppProcessViewModel.ProjectActionFormId.ToString());
                                sbSFLogger.Append("\r\n>>>Project Action TaskInstanceID: " + AppProcessViewModel.TaskInstanceId.ToString());
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving FHA Request details into SF System");
                            }

                        }

                    }
                }
                catch (Exception ex)
                {

                }

            }
            #endregion

            sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }

            return opaform_sfid;

        }

        //to push Lender Firm Commitment Response to Sales Force
        public static string postLenderFMResponse(List<TaskModel> TaskList, string TaskFileOwnerID, string Name)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
            sbSFLogger.Append("\r\n>>>Production Application OPA saving initiated");

            //Note: assuming that Lender details and Lender User Details are already in SF system
            //0014T000004HkkgQAC=sflenderid
            //0034T000006NKfjQAG=sflenderuserid
            //Task File Owner ID=0054T000000F4qfQAC
            TaskFileOwnerID = "0054T000000F4qfQAC";
            Name = "Application Process";
            string sfTaskFileNameChangeRef = string.Empty;

            #region Creating OPA/Production-Constrution-Single Stage Request
            if (!TaskFileOwnerID.Equals(string.Empty))
            {
                sbSFLogger.Append("\r\n>>>OPA Form Data Posting initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Production_Application_Request__c";
                        client.BaseAddress = new Uri(restPostAccQuery);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _authToken);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-PrettyPrint", "1");

                        Uri u = new Uri(restPostAccQuery);

                        OPAFormModel obj = new OPAFormModel();
                        //FileName, DocTyeID
                        //Noe: TaskInstanceId, SequenceId, AssignedBy,StartTime, Notes(servicer comments), TaskStepId, PageTypeId, FhaNumber

                        //obj.FHA_Number__c = AppProcessViewModel.FhaNumber;
                        //obj.SourceSystemID__c = AppProcessViewModel.TaskInstanceId.ToString();
                        //obj.Servicer_Submission_Date__c = AppProcessViewModel.ServicerSubmissionDate.ToString();
                        //obj.Project_Action_StartDate__c = AppProcessViewModel.ProjectActionDate.ToString();
                        ////obj.ProjectActionTypeId__c = AppProcessViewModel.ProjectActionTypeId.ToString();
                        //obj.ProjectActionTypeId__c = projTypeName;
                        //obj.AEComments__c = AppProcessViewModel.AEComments;
                        //obj.Servicer_Comments__c = AppProcessViewModel.ServicerComments;
                        //obj.Is_Address_Change__c = AppProcessViewModel.IsAddressChange.ToString();

                        var json1 = new JavaScriptSerializer().Serialize(obj);

                        HttpContent c = new StringContent(json1, Encoding.UTF8, "application/json");

                        sbSFLogger.Append("\r\n>>>Task FileName change Data Payload :" + json1.ToString());

                        HttpRequestMessage request = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = u,
                            Content = c
                        };

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFResObj>(responseData);
                                if (resultObj1.success)
                                {
                                    if (resultObj1.id != null && !string.IsNullOrEmpty(resultObj1.id))
                                    {
                                        sfTaskFileNameChangeRef = resultObj1.id;

                                        sbSFLogger.Append("\r\n>>>Production request for OPAForm data: " + sfTaskFileNameChangeRef);
                                    }
                                }
                            }
                            else
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving Production Request for OPAFOrm data SF System");

                        }

                    }
                }
                catch (Exception ex)
                {

                }



            }
            #endregion

            sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(_logFilePath, true))
            {
                file.WriteLine(sbSFLogger.ToString());
            }

            return sfTaskFileNameChangeRef;
        }

        //get the SFID from OPAFOrm data (Production Request) by querying with TaskInstanceId
        public static string getOPAFormSF_ByTaskInstanceId(TaskFileModel taskFileModel, string FhaNumber)
        {
            Production_AppProcessSFI pap = new Production_AppProcessSFI();
            pap = null;

            StringBuilder sbSFLogger = new StringBuilder();
            sbSFLogger.Append("\r\n**********************************" + DateTime.Now.ToString() + "**********************************************");
            sbSFLogger.Append("\r\n>>>getting OPAFOrm sfid by taskinstanceid and fha number initiated");

            string OpaFormSfid = string.Empty;

            #region Creating Task File Data Request
            if (true)
            {
                sbSFLogger.Append("\r\n>>>OPAForm data query initiated");

                try
                {

                    using (var client = new HttpClient())
                    {
                        //https://na136.salesforce.com/services/data/v45.0/sobjects/FHA_Request__c
                        //string restPostAccQuery = _instanceUrl + "/services/data/v45.0/sobjects/Task_File__c";

                        string restPostAccQuery = _instanceUrl + "/services/data/v45.0/query?q=SELECT+ID+FROM+Production_Application_Request__c+WHERE+SourceSystemID__c='" + taskFileModel.TaskInstanceId.ToString() + "'+AND+FHA_Number__c='" + FhaNumber + "'";

                       
                        var request = new HttpRequestMessage(HttpMethod.Get, restPostAccQuery);
                        request.Headers.Add("Authorization", "Bearer " + _authToken);
                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        request.Headers.Add("X-PrettyPrint", "1");

                        using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                                //var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                //Employee resultObj = new Employee();

                                /////Note:need a parser to convert resultObj1 => resultObj
                                var resultObj1 = Newtonsoft.Json.JsonConvert.DeserializeObject<SFAccontResModel>(responseData);
                                var tbl = resultObj1.records;
                                foreach (var rec in tbl)
                                {
                                    OpaFormSfid = rec.Id.ToString();

                                    sbSFLogger.Append("\r\n>>>Lenders Firm Commitment Response process: FHA Number searched in SF System: " + FhaNumber);
                                    sbSFLogger.Append("\r\n>>>Lenders Firm Commitment Response process: SF OPA SFID for the give FHANUMBER: " + OpaFormSfid);
                                }

                            }
                            else
                                sbSFLogger.Append("\r\n>>>Something went wrong while saving Production Request for OPAFOrm data SF System");

                        }
                        
                    }
                }
                catch (Exception ex)
                {

                }

                sbSFLogger.Append("\r\n********************************************END**********************************************\r\n");

                using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(_logFilePath, true))
                {
                    file.WriteLine(sbSFLogger.ToString());
                }

                return OpaFormSfid;

            }
            #endregion


        }


    }
}