﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HUDHealthcarePortal.SFIntegration
{
    public class SFResObj
    {
        public string id { get; set; }
        public bool success { get; set; }
        public List<object> errors { get; set; }
    }

    public class Attributes
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    public class Record
    {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
    }

    public class SFAccontResModel
    {
        public int totalSize { get; set; }
        public bool done { get; set; }
        public List<Record> records { get; set; }
    }

    public class SFLenderAccCreateObject
    {
        //name-lender name, type-Lender, AccountNumber-returns in response,SourceSystemID__c='73723'(greystone us inc ec)
        public string SourceSystemID__c { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class SFLenderAccCreateResObj
    {
        public string id { get; set; }
        public bool success { get; set; }
        public List<object> errors { get; set; }
    }

    public class SFLendersUsersCreateObject
    {
        ////AccountId=sfLenderID, Email-lenders user email, Phone-lenders users phone, Name-lenders users Last name(fullname)
        public string AccountId { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string SourceSystemID__c { get; set; }
    }

    public class SFFHAReqCreateObj
    {
        public string FHA_Number__c { get; set; }
        public string Name { get; set; }
        public string Comments__c { get; set; }
        public string Lender__c { get; set; }
        public bool Is_New_Or_Exist_Or_NAPortfolio__c { get; set; }
        public bool Is_Mid_Large_Portfolio__c { get; set; }
        public string Skilled_Nursing_Beds__c { get; set; }
        public string Skilled_Nursing_Units__c { get; set; }
        public bool Is_Master_Lease_Proposed__c { get; set; }
        public string Status__c { get; set; }
        public string Loan_Amount__c { get; set; }
        public string Proposed_Interest_Rate__c { get; set; }
        public string Lender_Contact__c { get; set; }
        public string Project_Type__c { get; set; }
        public string Current_Loan_Type__c { get; set; }
        public string Loan_Type__c { get; set; }
        public string Borrower_Type__c { get; set; }
        public string SourceSystemID__c { get; set; }//to have task instance id for all to and fro queries as a identity key                                                     
        public string CMS_Star_Rating__c { get; set; }
    }


    //Folder_Grid__c???Production_Application_Request__c->opaform sfid, 
    //need an API to get SFID from SF_OPA table for a given OPA Data, what input/parameters are required into SF to get ealrier created SFID
    public class SFTaskFileDataModel
    {
        //public string OwnerId { get; set; }
        //public string Name { get; set; }
        //public string FileId__c { get; set; }
        public string FileName__c { get; set; }
        public string SourceSystemId__c { get; set; }
        //public string UploadTime__c { get; set; }
        //public string FileType__c { get; set; }
        //public string SystemFileName__c { get; set; }
        //public double FileSize__c { get; set; }
        public string RoleName__c { get; set; }
        public string DocTypeID__c { get; set; }
        public string DocId__c { get; set; }
        //public int Version__c { get; set; }
        public string API_upload_status__c { get; set; }
        public string Folder_Grid__c { get; set; }
        public string Production_Application_Request__c { get; set; }///opaform sfid
       
    }

    public class SFTaskFileNameChangeDataModel
    {
        
        public string FileName__c { get; set; }
        public string SourceSystemId__c { get; set; }       
        public string DocTypeID__c { get; set; }
        public string DocId__c { get; set; }
        
    }

    #region IMPORTANAT Notes
    /*
     mandatory attributes to OPAData, if not difficult to query while attaching files during Firmcommitment attachments, 
     there is OPA context only one time whn lender is about to submit OPA Data along with task file,other wise no OPA Context
--FhaNumber: "666-00039
--TaskInstanceId: {966b96b8-0c98-4851-8bf8-c659229c61
Project_Number__c???
Name???

    */
    #endregion
    public class OPAFormModel
    {

        public string FHA_Number__c { get; set; }
        public string SourceSystemID__c { get; set; }        
        public string Servicer_Submission_Date__c { get; set; }       
        public string Name { get; set; }
        public string Project_Number__c { get; set; }
        public string Property__c { get; set; }
        
    }

    public class LenderFrmCommtResponse
    {

        public string FHA_Number__c { get; set; }
        public string SourceSystemID__c { get; set; }
        //public string HudTaskIDID__c { get; set; }
        public string Servicer_Submission_Date__c { get; set; }
        public string Project_Action_StartDate__c { get; set; }
        public string ProjectActionTypeId__c { get; set; }
        public string AEComments__c { get; set; }
        public string Servicer_Comments__c { get; set; }
        public string Is_Address_Change__c { get; set; }

    }

    public class PropertyData
    {

        public string Name { get; set; }
        public string PostalCode__c { get; set; }
        //public string HudTaskIDID__c { get; set; }
        public string StateCode__c { get; set; }
        public string Street__c { get; set; }
        public string Country__c { get; set; }
        public string CountryCode__c { get; set; }
        public string City__c { get; set; }        

    }

}