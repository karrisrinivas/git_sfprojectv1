﻿using System.Web.Mvc;

namespace HUDHealthcarePortal.Framework
{
    public class XsltViewEngine : VirtualPathProviderViewEngine
    {
        public XsltViewEngine()
        {
            ViewLocationFormats = new[] 
            { 
                "~/Xslt/{1}/{0}.xsl", "~/Xslt/Shared/{0}.xsl", 
                "~/Xslt/{1}/{0}.xslt", "~/Xslt/Shared/{0}.xslt"
            };
            PartialViewLocationFormats = ViewLocationFormats;
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            return new XsltView(partialPath);
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            return new XsltView(viewPath);
        }
    }
}
