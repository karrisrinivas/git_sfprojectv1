﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

//sfintegration
namespace HUDHealthcarePortal.Controllers.HelpDesk
{
    public class TestDemo1Controller : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/default6/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFFHAReq_TaskUpdatePayoadToHud value)
        {
            _taskManager = new TaskManager();

            //if (value.TaskType.Equals("FHAInsert"))
            //    _taskManager.SFINT_FHA_Insert_TaskAck("FHAInsert", value.FHANumber, value.TaskInstanceId);

            //if (value.TaskType.Equals("Portfolio"))
            //    _taskManager.SFINT_FHA_Insert_TaskAck("Portfolio", value.FHANumber, value.TaskInstanceId);

            //Identify if ithe payload is //FHAInsert/Portfolio

            int retcode = _taskManager.SFINT_FHA_Insert_TaskAck(value.AssignedTo, value.AssignedDate, value.TaskInstanceId, value.IsCreditReviewAttachComplete
                                                                ,value.CreditreviewComments, value.CreditReviewDate, value.Portfolio_Name
                                                                ,value.Portfolio_Number, value.IsPortfolioComplete, value.PortfolioComments
                                                                ,value.FHANumber, value.IsFhaInsertComplete, value.RequestStatus, value.IsReadyForApplication
                                                                , value.Property__c);


            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // PUT api/default6/5
        public HttpResponseMessage Put(int id, [FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // DELETE api/default6/5
        public void Delete(int id)
        {
        }
    }
}
