﻿using HUDHealthcarePortal.Core;
using Model;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HUDHealthcarePortal.Controllers.Production;
using HUDHealthcarePortal.SFIntegration.HUDAPIModels;
using System.Web.Mvc;
using HUDHealthcarePortal.Model;
using System.Web.Security;
using Core.Utilities;
using HUDHealthcarePortal.Controllers;
using HUDHealthCarePortal.Controllers;

//sfintegration
namespace HUDHealthcarePortal.Controllers.HelpDesk
{
    //FHA Request Process
    public class TestDemo1Controller : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/default6/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFFHAReq_TaskUpdatePayoadToHud value)
        {
            _taskManager = new TaskManager();           


            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // PUT api/default6/5
        public HttpResponseMessage Put(int id, [FromBody] abcClass value)
        {
            return Request.CreateResponse(HttpStatusCode.OK, value);
        }

        // DELETE api/default6/5
        public void Delete(int id)
        {
        }
    }

    //creating FHA Number, Assignment of Request, Credit Review complete, Portfolio Review complete request form SF by HUD
    public class ProdAppProc_SFFHAInsertController : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "Karri", "FHA Insert with Reviews and Assignment done" };
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFFHAReq_TaskUpdatePayoadToHud value)
        {
            FHARequestController fharc = new FHARequestController();

            
            int retcode = fharc.PushFHAReqAutoCompletion(value.AssignedToUserName, value.AssignedDate, value.TaskInstanceId, value.IsCreditReviewAttachComplete
                                                                , value.CreditreviewComments, value.CreditReviewDate, value.Portfolio_Name
                                                                , value.Portfolio_Number, value.IsPortfolioComplete, value.PortfolioComments
                                                                , value.FHANumber, value.IsFhaInsertComplete, value.RequestStatus, value.IsReadyForApplication
                                                                , value.Property__c);

            ////always SF should pass the UserName
            //int retcode = _taskManager.SFINT_FHA_Insert_TaskAck(value.AssignedTo, value.AssignedDate, value.TaskInstanceId, value.IsCreditReviewAttachComplete
            //                                                    , value.CreditreviewComments, value.CreditReviewDate, value.Portfolio_Name
            //                                                    , value.Portfolio_Number, value.IsPortfolioComplete, value.PortfolioComments
            //                                                    , value.FHANumber, value.IsFhaInsertComplete, value.RequestStatus, value.IsReadyForApplication
            //                                                    , value.Property__c);

            if (!retcode.Equals(1))
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, value);
            else
                return Request.CreateResponse(HttpStatusCode.OK, value);
        }

    }

    //Production Task File, OPA data, Firm Committment, Closing Request
    public class ProdAppProc_FrmComtController : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "Karri", "Creating Firmcommitment in HUD" };
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFReq_FirmCommittment value)
        {
            string retMsg = string.Empty;

            var re = Request;
            var headers = re.Headers;
            string token;

            if(headers.Contains("username"))
            {
                token = headers.GetValues("username").First();
            }


            //ProductionMyTaskController pmc = new ProductionMyTaskController();

            //ActionResult result = pmc.GetTaskDetail(new Guid("DE10F392-D8D3-46B0-A903-6C5F1E1889D4"));

             

            //FirmCommitmentSentFromSFCalledByHUDAPI
            //if (value.FilesUploadedCount < 1)
            //    value.FilesUploadedCount = 1;
            if (value.AssignedByUserName == null || string.IsNullOrEmpty(value.AssignedByUserName))
                value.AssignedByUserName = "c3director@yahoo.com";

            //Note:Uploaded files data to be processed first, the uploaded files data by Internal user like C3 users will be embeded as string 
            //delimited in the same FirmCommitment payload from SF

            //DocId,DocTypeID,FileId(File Count Index),FileName,FileSize,TaskInstanceId
            //9068670~2082~0~r4rfile2.PDF~5284~6b57046d - 4d0c - 42a6 - 9385 - 263b92e752f1

            if (value.FCUploadedFilesData != null && !string.IsNullOrEmpty(value.FCUploadedFilesData))
            {
                string[] filesplitseperator = new string[] { "&&&"};
                char[] filedataseperator = new char[] { '~' };

                string[] FileData = value.FCUploadedFilesData.Split(filesplitseperator, StringSplitOptions.None);
               
                if (FileData.Length > 0) 
                {
                    foreach(var data in FileData)
                    {
                        string[] FileAttribs = data.ToString().Split(filedataseperator, StringSplitOptions.None);
                        if (FileAttribs.Length > 0)
                        {
                            ProductionApplicationController obj1 = new ProductionApplicationController();

                            retMsg = obj1.HudFirmcommtimentFiles(new Guid(value.ProjectActionFormId), new Guid(value.TaskGuid), null, value.AssignedByUserName
                                                                        , FileAttribs[0], FileAttribs[1], FileAttribs[3], Convert.ToDouble( FileAttribs[4])
                                                                        ,null, FileAttribs[2].ToString());

                           
                        }
                    }

                    if (retMsg == "success")
                    {
                        retMsg = string.Empty;

                        ProductionApplicationController obj = new ProductionApplicationController();

                        //payload that comes from SF for FirmCommittment from Internal User where it creates a similar record in HUD system as well
                        //Model.TaskGuid, taskxref = Model.taskxrefId, pagetypeid = Model.pageTypeId from OPA From Data in TASK table
                        retMsg = obj.HUDUserFirmcommitmentFromSF(value.TaskGuid, value.AssignedByUserName, 1);

                        if (retMsg != null && !retMsg.Contains("Failure"))
                            return Request.CreateResponse(HttpStatusCode.OK, value);
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, value);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, value);

                }

               
            }


            return Request.CreateResponse(HttpStatusCode.OK, value);

        }

    }

    public class ProdAppProc_HUDReviewCompController : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "HUD", "HUD Reiew Complete Request" };
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFReq_ProdProc_HudReviewComp value)
        {
            //ProductionMyTaskController obj1 = new ProductionMyTaskController();
            ////7d038d3e-bccd-44d6-af9b-da03fe2f19ef
            //JsonResult result = (JsonResult)obj1.GetTaskDetail(new Guid("7d038d3e-bccd-44d6-af9b-da03fe2f19ef"));
            //OPAViewModel opavm = result[0].
            //value.associatedUserId = "4450";
           
            
            //value.associatedUserName = "c3director@yahoo.com";
            ProductionApplicationController obj = new ProductionApplicationController();
            string retMsg = obj.CompleteFirmcommitmentReviewFromSF(new Guid(value.HudOrLndrFCTaskinstaneId), value.associatedUserName);

            if (retMsg != null && retMsg.Contains("Success"))
                return Request.CreateResponse(HttpStatusCode.OK, value);
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, value);
        }

    }

    //for creating taskfile data sent from SF by HUD during Firmcommitment Request
    public class ProdAppProc_FCUploadFilesController : ApiController
    {
        ITaskManager _taskManager = null;

        // GET api/default6
        public IEnumerable<string> Get()
        {
            return new string[] { "HUD", "HUD Firmcommitment Files Uploading Request" };
        }

        // POST api/default6
        public HttpResponseMessage Post([FromBody] SFReq_ProdProc_HudFMUploadFIles value)
        {
            //should send OPAFORM-ProjectActionFormId, other task file meta data

            ProductionApplicationController obj = new ProductionApplicationController();

            //string retMsg1 = obj1.HudFirmcommtimentFiles(new Guid("BE8E6764-F841-4BF1-BE47-A809023F1212"), "WorkProductClosing", "c3director@yahoo.com"
            //                                            , "9068539", "2082", "karrifile3.PDF", 5284
            //                                            , "ProductionWLM");

            //
            //string retMsg1 = obj1.HudFirmcommtimentFiles(new Guid("BE8E6764-F841-4BF1-BE47-A809023F1212"), null, "c3director@yahoo.com"
            //                                            , "9068539", "2082", "karrifile2.PDF", 5284
            //                                            , "ProductionWLM");


            string retMsg = obj.HudFirmcommtimentFiles(new Guid(value.ProjectActionFormId), new Guid(value.ProjectActionTaskinstanceid), value.FolderKeyName, value.userName
                                                        , value.Docid, value.DocTypeId, value.FileName, value.FileSize
                                                        , value.RoleName);

            if (retMsg != null && retMsg.Contains("Success"))
                return Request.CreateResponse(HttpStatusCode.OK, "Done");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Done");
        }

    }



}
