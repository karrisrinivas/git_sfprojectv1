﻿/// <reference path="../app.js" />
portfolioSummaryModule.controller('portfolioSummaryModuleCtrl', ['$scope', '$http', 'portfolioSummaryModuleSvc',
    function ($scope, $http, portfolioSummaryModuleSvc) {
        $scope.isSubMode = false;
        $scope.orderBy = { field: 'Portfolio_Name', asc: true };
        // get quarter token
        var yearValue = $('#yearsList').val();
        var quarterValue = $('#quartersList').val();
        var qrtToken = yearValue + quarterValue;
        load(qrtToken);
        function load(quarterToken) {
            portfolioSummaryModuleSvc.getPortfolioSummaries(quarterToken).success(function (data) {
                $scope.Portfolios = data;
                angular.forEach($scope.Portfolios, function(portfolio) {
                    portfolio.isSubMode = false;
                    portfolio.isDetailLoaded = false;
                });
            });
        };
        // expose load function in scope
        $scope.load = load;
        $scope.quarterToken = qrtToken;
        $scope.toggleSubMode = function (portfolio) {
            var yearVal = $('#yearsList').val();
            var quarterVal = $('#quartersList').val();
            var qrtTokenVal = yearVal + quarterVal;
            portfolio.isSubMode = !portfolio.isSubMode;
            if (!portfolio.isDetailLoaded) {
                portfolioSummaryModuleSvc.getPortfolioDetail(portfolio.Portfolio_Number, qrtTokenVal).success(function (data) {
                    portfolio.Details = data;
                    portfolio.isDetailLoaded = true;
                });
            }
        };

        $scope.setOrderBy = function (field) {
            var asc = $scope.orderBy.field === field ? !$scope.orderBy.asc : true;
            $scope.orderBy = { field: field, asc: asc };
        };

        $scope.scoreStyle = function (scoreValue) {
            if (scoreValue == null || scoreValue == "")
            //return "#FFFFFF";
                return;
            var scoreColor = scoreValue >= 20 ? "#FF7C80" : scoreValue >= 10 ? "#FFFF00" : scoreValue >= 0 ? "#92D050" : "#FFFFFF";
            return scoreColor;
        }
    }
]);