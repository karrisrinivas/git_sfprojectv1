﻿var formHelper = {
    checkFileType: function (fileTypes) {

        var fileName = $('#UploadFile').val();
        // change function called twice on change of file.
        if (fileName != null && fileName != "") {
            if (fileTypes != "") {
                var fileTypeCheck = fileTypes + ',';
                var fileNameSegments = fileName.match(/\.([^\.]+)$/);
                var ext = "";
                // if file has an extension.
                if (fileNameSegments != null) {
                    ext = "." + fileNameSegments[1] + ',';
                    ext = ext.toLowerCase();
                }
                if (fileNameSegments == null || fileTypeCheck.indexOf(ext, 0) == -1) {
                    $('#UploadFile').val(null);
                    $('#BaseFileName').val(null);
                    $('#FullyQualifiedFileName').val(null);
                    document.getElementById('BaseFileName').title = "";
                    alert("The file is not in the expected format. Please make sure the file you are attaching is in accepted format (" + fileTypes + ").");
                    return false;
                }
            }
            $('#BaseFileName').val($('#UploadFile').val().split('\\').pop());
            $('#FullyQualifiedFileName').val($('#UploadFile').val());
            document.getElementById('BaseFileName').title = $('#FullyQualifiedFileName').val();
        }
    }
}