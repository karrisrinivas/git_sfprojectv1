namespace EntityObject.Entities.HCP_intermediate
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Core;

    public partial class HCP_intermediate : DbContext
    {
        //karri;Hareesh;Naveen:#121; donno why the value given public static class WebUiConstants is not loaded
        //into context of DB, hence the confing value is requied to be given here
        //if wrong value given here then some how loading .\sqlexpress....local db here
        public HCP_intermediate()
            //: base("HCP_intermediate")//karrinew;removed hardcoded string 
            : base(WebUiConstants.IntermediateConnectionName)
        {
        }

        public virtual DbSet<Lender_DataUpload_Intermediate> Lender_DataUpload_Intermediate { get; set; }
        public virtual DbSet<vwComment> vwComments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.OperatingCash)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.Investments)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.ReserveForReplacementEscrowBalance)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.AccountsReceivable)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.CurrentAssets)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.CurrentLiabilities)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.TotalRevenues)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.RentLeaseExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DepreciationExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.AmortizationExpense)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.TotalExpenses)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.NetIncome)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.ReserveForReplacementDeposit)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.FHAInsuredPrincipalPayment)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.FHAInsuredInterestPayment)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.MortgageInsurancePremium)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.ReserveForReplacementBalancePerUnit)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.WorkingCapital)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DebtCoverageRatio)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DaysCashOnHand)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DaysInAcctReceivable)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.AvgPaymentPeriod)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.WorkingCapitalScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DebtCoverageRatioScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DaysCashOnHandScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.DaysInAcctReceivableScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.AvgPaymentPeriodScore)
                .HasPrecision(19, 2);

            modelBuilder.Entity<Lender_DataUpload_Intermediate>()
                .Property(e => e.ScoreTotal)
                .HasPrecision(19, 2);
        }
    }
}
