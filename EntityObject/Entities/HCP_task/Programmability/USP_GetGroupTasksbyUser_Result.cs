﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
    public class USP_GetGroupTasksbyUser_Result
    {
        public int GroupTaskid { get; set; }
        public string ProjectActionName { get; set; }
        public string Status { get; set; }
        public string role { get; set; }
        public int InUse { get; set; }
        public string UnLock { get; set; }
        public DateTime? ServicerSubmissionDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public string InUseuser { get; set; }
    }
}
