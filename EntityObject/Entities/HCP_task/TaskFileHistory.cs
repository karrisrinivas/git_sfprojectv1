﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace EntityObject.Entities.HCP_task
{
    [Table("TaskFileHistory")]
    public class TaskFileHistory
    {
        [Key]
        public Guid TaskFileHistoryId { get; set;}
        public Guid ParentTaskFileId { get; set;}
        public Guid ChildTaskFileId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public int TransactionId { get; set;}
        public DateTime CreatedOn { get; set;}
        public int CreatedBy { get; set;}
    }
}
