﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("Prod_GroupTasks")]
    public class Prod_GroupTasks
    {
        [Key]
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int RequestStatus { get; set; }
        public int InUse { get;set; }
        public int PageTypeId { get; set; }
        public int CreatedBy {get; set; }
        public DateTime CreatedOn {get; set; }
        public int ModifiedBy {get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ServicerComments { get; set; }    
        public bool IsDisclaimerAccepted { get; set; }

    }
}
