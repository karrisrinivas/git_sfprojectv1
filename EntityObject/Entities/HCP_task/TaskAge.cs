﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    public class TaskAge
    {
        [Key]
        public int TaskAgeId { get; set; }
        public string TaskAgeInterval { get; set; }
        public string TaskAgeIntervalText { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public bool DeletedInd { get; set; }
    }
}
