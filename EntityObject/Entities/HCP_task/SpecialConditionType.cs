﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("SpecialConditionType")]
	public partial class SpecialConditionType
	{
		[Key]
		public int SpecialConditionTypeId { get; set; }
		public string SpecialCondition { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}
