﻿
namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


     [Table("Prod_ViewType")]
    public partial class Prod_ViewType
    {
         
          [Key]
          public int ViewTypeId { get; set; }
          public string ViewType { get; set; }
          public int MyProperty { get; set; }
          public DateTime Modifiedon { get; set; }

    }
}
