﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("Prod_Form290Task")]
    public partial class  Prod_Form290Task
    {   
        [Key]
        public int Form290TaskID { get; set; }
        public Guid ClosingTaskInstanceID { get; set; }
        public Guid TaskInstanceID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedByUserName { get; set; }
        public int Status { get; set; }


    }
}
