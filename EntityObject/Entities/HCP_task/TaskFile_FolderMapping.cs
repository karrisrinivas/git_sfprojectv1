﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("TaskFile_FolderMapping")]
    public partial class TaskFile_FolderMapping
       
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int fileFolderId { get; set; }
        public Guid TaskFileId { get; set; }
        public int FolderKey { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
