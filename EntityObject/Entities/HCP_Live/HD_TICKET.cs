﻿//karri:#149, #214

namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    //KARRINEW//should be partial class as it is xtended to main class
    [Table("HD_TICKET")]
    public partial class HD_TICKET
    {
        [Key]
        public int TICKET_ID { get; set; }
        public int? STATUS { get; set; }//open or closed etc
        //[StringLength(255)]//title
        public string ISSUE { get; set; }//MAPS TO TITLE ON THE UI
        //[Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }
        public int? CATEGORY { get; set; }//type of issue

        public int? SUBCATEGORY { get; set; }//type of issue

        public int? PRIORITY { get; set; }
        //[Column(TypeName = "text")]
        public string RECREATION_STEPS { get; set; }
        //[Column(TypeName = "text")]
        public string CONVERSATION { get; set; }
        public int? CREATED_BY { get; set; }
        public DateTime CREATED_ON { get; set; }
        public int? ASSIGNED_TO { get; set; }
        public DateTime? ASSIGNED_DATE { get; set; }
        public int? LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_ON { get; set; }
        public int? ONACTION { get; set; }//hdnew
        public string ROLENAME { get; set; }

        //hudenhancements3
        public string FHANumber { get; set; }
        public string PropertyName { get; set; }
        public int? PropertyId { get; set; }

        //public virtual HCP_Authentication HCP_Authentication { get; set; }

        //public virtual HCP_Authentication HCP_Authentication1 { get; set; }

        //public virtual HCP_Authentication HCP_Authentication2 { get; set; }
    }
}
