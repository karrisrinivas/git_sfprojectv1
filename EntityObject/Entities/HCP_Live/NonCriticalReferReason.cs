﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
namespace EntityObject.Entities.HCP_live
{
    public class NonCriticalReferReason
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReferId { get; set; }
        public Guid NonCriticalRepairsRequestID { get; set; }
        public int RuleId { get; set; }
    }
}
