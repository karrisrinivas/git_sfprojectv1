﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_ProjectType")]
    public  class Prod_ProjectType
    {
        [Key]
        public int ProjectTypeId { get; set;}
        public string ProjectTypeName { get; set;}
        public bool IsotherloanType { get; set; }
        public int PageTypeId { get; set; }
    }
}
