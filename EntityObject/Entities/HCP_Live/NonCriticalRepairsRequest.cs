﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace EntityObject.Entities.HCP_live
{
     public sealed partial class   NonCriticalRepairsRequest
    {
         public NonCriticalRepairsRequest()
         {
             this.NonCriticalReferReasons = new HashSet<NonCriticalReferReason>();
         }
         
         [Key]
          [DatabaseGenerated(DatabaseGeneratedOption.None)]
         public Guid  NonCriticalRepairsRequestID { get; set; }
       
         public int PropertyID { get; set; }
         public int LenderID { get; set; }
         public string FHANumber { get; set; }
       

         public DateTime? SubmittedDate { get; set; }
         public DateTime? ApprovedDate { get; set; }
         public decimal PaymentRequested { get; set; }
         public bool? IsFinalDraw { get; set; }
         public bool? IsScopeOfWorkChanged { get; set; }
         public bool? IsAdvance { get; set; }
        [StringLength(260)]
        public string FileName92464 { get; set; }
        [StringLength(260)]
        public string FileName92117 { get; set; }

        [StringLength(260)]
        public string FileNamePCNA { get; set; }
        public string ScopeCertificationFileName { get; set; }
         public string FinalInspectionReport { get; set; }
         public string ContractFileName { get; set; }
         public string DefaultRequestExtensionFileName { get; set; }
         public int NumberDraw { get; set; }
         public bool? CanProvidePhotosInvoices { get; set; }
         public int RequestStatus { get; set; }
         public decimal? ApprovedAmount { get; set; }
         public string Reason { get; set; }
         public string ServicerRemarks { get; set; }
         public DateTime? CreatedOn { get; set; }
         public DateTime? ModifiedOn { get; set; }
         public int? CreatedBy { get; set; }
         public int? ModifiedBy { get; set; }
         public int? TaskId { get; set; }
         public ICollection<NonCriticalReferReason> NonCriticalReferReasons { get; set; }
         public bool? IsLenderDelegate { get; set; }
    }
}
