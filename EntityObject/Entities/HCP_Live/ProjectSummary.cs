﻿namespace EntityObject.Entities.HCP_live
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ProjectSummary
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ProjectSummaryID;
        public int WorkloadManagerId;
        public string WorkloadManager;
        public int AccountExecutiveId;
        public string AccountExecutive;
        public int LenderId;
        public string LenderName;
        public string ReportLevel;
        public int GroupOwnerId;
        public string GroupOwnerName;
        public int Total_Projects;
        public int TotalUnpaidPrincipleBalance;
        public decimal DebtCoverageRatio;
        public decimal WorkingCapital;
        public decimal DaysCashOnHand;
        public decimal DaysInAcctReceivable;
        public decimal AvgPaymentPeriod;
        public decimal UnpaidPrincipleBalance;
    }
}
