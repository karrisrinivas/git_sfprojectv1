namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FHAInfo_iREMS
    {
        public FHAInfo_iREMS()
        {
            HUD_Project_Manager_Portfolio = new HashSet<HUD_Project_Manager_Portfolio>();
            Lender_DataUpload_Live = new HashSet<Lender_DataUpload_Live>();
            PropertyID_FHANumber_iREMS = new HashSet<PropertyID_FHANumber_iREMS>();
        }

        [Key]
        [StringLength(15)]
        public string FHANumber { get; set; }

        [Required]
        [StringLength(100)]
        public string FHADescription { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual ICollection<HUD_Project_Manager_Portfolio> HUD_Project_Manager_Portfolio { get; set; }

        public virtual ICollection<Lender_DataUpload_Live> Lender_DataUpload_Live { get; set; }

        public virtual ICollection<PropertyID_FHANumber_iREMS> PropertyID_FHANumber_iREMS { get; set; }
    }
}
