using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using EntityObject.Models.Mapping;

namespace EntityObject.Models
{
    public partial class HCP_Intermediate_dbContext : DbContext
    {
        static HCP_Intermediate_dbContext()
        {
            Database.SetInitializer<HCP_Intermediate_dbContext>(null);
        }

        public HCP_Intermediate_dbContext()
            : base("Name=HCP_Intermediate_dbContext")
        {
        }

        public DbSet<Lender_DataUpload_Intermediate> Lender_DataUpload_Intermediate { get; set; }
        public DbSet<vwComment> vwComments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Lender_DataUpload_IntermediateMap());
            modelBuilder.Configurations.Add(new vwCommentMap());
        }
    }
}
