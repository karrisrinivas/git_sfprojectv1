﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BusinessService.Interfaces;
using Core.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Model;

namespace BusinessService
{
    /// <summary>
    /// Jan 8, 2015, change Days in period using month in periond X 30 instead of period ending date
    /// </summary>
    public class ScoreManagerV3 : IScoreManager
    {
        // can swap out score agent for different score matrices
        private IScoreAgent scoreAgent;
        public ScoreManagerV3(IScoreAgent agent)
        {
            scoreAgent = agent;
            scoreAgent.InitializeScoreMatrix();
        }

        public void GetScores(FundamentalFinancial financial)
        {
            IEnumerable<Type> interfacesInTheClass = this.GetType().GetNestedTypes()
                .Where(p => p.GetInterface(typeof(IUploadedLoanVisitor).ToString()) != null);
            Type[] types = this.GetType().GetNestedTypes();
            foreach (KeyValuePair<DerivedFinancial, Func<decimal?, decimal?>> item in scoreAgent.ScoreMatrix)
            {
                if (!financial.DerivedFinDict.ContainsKey(item.Key))
                {
                    // use reflection to create the right interface
                    foreach (Type type in interfacesInTheClass)
                    {
                        MemberInfo info = type.GetField("VisitorType") as MemberInfo ??
                                    type.GetProperty("VisitorType") as MemberInfo;
                        if (info != null)
                        {
                            var visitor = Activator.CreateInstance(type);
                            //FillDerived((IUploadedLoanVisitor)visitor, financial);
                            financial.FillDerived((IUploadedLoanVisitor)visitor);
                        }
                    }
                }
            }

        }

        public class DebtCoverageRatio2Visitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    if (uploadedData.FHAInsuredPrincipalInterestPayment + uploadedData.MortgageInsurancePremium != 0)
                    {
                        _derivedVal = (uploadedData.TotalRevenues - uploadedData.TotalExpenses) / (uploadedData.FHAInsuredPrincipalInterestPayment + uploadedData.MortgageInsurancePremium);
                    }
                    else
                    {
                        _derivedVal = 0;
                    }

                    fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
                }
            }


            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DebtCoverageRatio2; }
            }
        }

        public class AverageDailyRateVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null && uploadedData.ActualNumberOfResidentDays != 0)
                    _derivedVal = uploadedData.TotalRevenues / uploadedData.ActualNumberOfResidentDays;
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.AverageDailyRate; }
            }
        }

        public class NOIVisitor : IUploadedLoanVisitor
        {
            private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                var uploadedData = fundamentalFin as ExcelUploadView_Model;
                if (uploadedData != null)
                {
                    _derivedVal = uploadedData.TotalRevenues - uploadedData.TotalExpenses;
                }
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, _derivedVal);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.NOI; }
            }
        }

        public class DebtCoverageRatioVisitor : IUploadedLoanVisitor
        {
            //private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }


            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DebtCoverageRatio; }
            }
        }

        public class WorkingCapitalVisitor : IUploadedLoanVisitor
        {
            //private decimal? _derivedVal;
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.WorkingCapital; }
            }
        }

        public class DaysCashOnHandVisitor : IUploadedLoanVisitor
        {
            public void Visit(FundamentalFinancial fundamentalFin)
            {

                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DaysCashOnHand; }
            }
        }

        public class DaysInAccountReceivableVisitor : IUploadedLoanVisitor
        {
            public void Visit(FundamentalFinancial fundamentalFin)
            {

                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.DaysInAccountReceivable; }
            }
        }

        public class AvgPaymentPeriodVisitor : IUploadedLoanVisitor
        {
            public void Visit(FundamentalFinancial fundamentalFin)
            {

                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }

            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.AvgPaymentPeriod; }
            }
        }

        public class ReserveForReplaceBalUnitVisitor : IUploadedLoanVisitor
        {
            public void Visit(FundamentalFinancial fundamentalFin)
            {
                fundamentalFin.DerivedFinDict.Add(this.VisitorType, 0);
            }


            public DerivedFinancial VisitorType
            {
                get { return DerivedFinancial.ReserveForReplaceBalUnit; }
            }
        }
    }
}


