﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Core;
using Model;

namespace HUDHealthcarePortal.BusinessService
{
    public class ManagementReportManager : IManagementReportManager
    {       
        private UnitOfWork _unitOfWorkIntermediate;
        private UnitOfWork _unitOfWorkLive;


        public ManagementReportManager()
        {
            _unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            _unitOfWorkLive = new UnitOfWork(DBSource.Live);
        }

        public ReportModel GetHighRiskProjectReport(HUDRole hudRole)
        {
            var highRiskProjectReportRepository = new ManagementReportRepository(_unitOfWorkLive,
                ReportType.HighRiskProjectReport, hudRole);
            return highRiskProjectReportRepository.ManagementReportDataModel;
        }
    }
}
