﻿using BusinessService.Interfaces.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
namespace BusinessService.Production
{
    public class Prod_RestfulWebApiDownload : IProd_RestfulWebApiDownload
    {
        private IProd_RestfulWebApiDownloadRepository WebApiDocumentDownloadRepository;
        public Prod_RestfulWebApiDownload()
        {

            WebApiDocumentDownloadRepository = new Prod_RestfulWebApiDownloadRepository();
        }
        public Stream DownloadDocumentUsingWebApi(string JsonString, string token, string filename)
        {
            return WebApiDocumentDownloadRepository.DownloadDocumentUsingWebApi(JsonString, token, filename);
        }


        public byte[] DownloadByteFile(string JsonString, string token, string filename)
        {
            return WebApiDocumentDownloadRepository.DownloadByteFile(JsonString, token, filename);
        }
    }
}
