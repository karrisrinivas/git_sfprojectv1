﻿using BusinessService.Interfaces.Production;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_RestfulWebApiAddPropertyInfo : IProd_RestfulWebApiAddPropertyInfo
    {
        private IProd_RestfulWebApiAddPropertyInfoRepository WebApiPropertyInfoRepository;
        public Prod_RestfulWebApiAddPropertyInfo()
        {

            WebApiPropertyInfoRepository = new Prod_RestfulWebApiAddPropertyInfoRepository();
        }

      
        public RestfulWebApiResultModel AddPropertyInfoUsingWebApi(string jsonstring, string token)
        {
            return WebApiPropertyInfoRepository.AddPropertyInfoUsingWebApi(jsonstring, token);
        }

    }
}
