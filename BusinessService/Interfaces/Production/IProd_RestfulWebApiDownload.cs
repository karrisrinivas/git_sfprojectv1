﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_RestfulWebApiDownload
    {
        Stream DownloadDocumentUsingWebApi(string JsonString, string token, string filename);
        Byte[] DownloadByteFile(string JsonString, string token, string filename);
    }
}
