﻿using HUDHealthcarePortal.Core;
using System;

namespace HUDHealthcarePortal.BusinessService
{
    public static class TimezoneManager
    {
        public static DateTime GetPreferredTimeFromUtc(DateTime utcDt)
        {
            var timeZoneId = UserPrincipal.Current.Timezone;
            if (!string.IsNullOrEmpty(timeZoneId))
            {
                TimeZoneInfo destTimezone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(utcDt, destTimezone);
            }
            else
            {
                return utcDt;
            }
        }

        public static string FormatDate(DateTime? Dt,string format)
        {
            if (Dt.HasValue)
                return string.Format(format, Dt);
            else
                return null;

        }

        public static DateTime? ToMyTimeFromUtc(this DateTime? utcDt)
        {
            if (utcDt.HasValue)
                return GetPreferredTimeFromUtc(utcDt.Value);
            else
                return null;
        }

        public static DateTime ToMyTimeFromUtc(this DateTime utcDt)
        {
            return GetPreferredTimeFromUtc(utcDt);
        }

        public static DateTime GetPreferredTimeFromUtc(DateTime utcDt, string timeZoneId)
        {
            if (!string.IsNullOrEmpty(timeZoneId))
            {
                TimeZoneInfo destTimezone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(utcDt, destTimezone);
            }
            else
            {
                return utcDt;
            }
        }

        public static DateTime GetUtcTimeFromPreferred(DateTime myDt)
        {
            var timeZoneId = UserPrincipal.Current.Timezone;
            if (!string.IsNullOrEmpty(timeZoneId))
            {
                TimeZoneInfo myTimezone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                return TimeZoneInfo.ConvertTimeToUtc(myDt, myTimezone);
            }
            else
            {
                return myDt;
            }
        }

        public static string ShortTimeZoneFormat(string timeZoneStandardName)
        {
            string[] TimeZoneElements = timeZoneStandardName.Split(' ');
            string shortTimeZone = String.Empty;
            foreach (string element in TimeZoneElements)
            {
                //copies the first element of each word
                shortTimeZone += element[0];
            }
            return shortTimeZone;
        }
    }
}
