﻿
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE  PROCEDURE [dbo].[USP_GetFilteredProductionTasks](
   @productionType int,
   @dateFrom datetime,
   @dateTo datetime,
   @lenderId int,
   @loanType int

)
AS
    BEGIN

	-- Query to get completed FHA numbers but portfolio and creditreview tasks not assigned!

	declare  @ParentCompletedFHA table 
	(
	 TaskInstanceID uniqueidentifier  
	)

	insert into @ParentCompletedFHA  select distinct task.TaskInstanceId from Task task join Prod_TaskXref xref on task.TaskInstanceId = xref.TaskInstanceId
                                    where task.PageTypeId=4 and task.TaskStepId = 19 and xref.AssignedTo is NULL
                                    group by task.TaskInstanceId	
	------------------------------------------------
	if(@productionType=4)

	   begin
	   --if Lender and loan types inot specified returns all lenders and all loan types
	   if(@lenderId=0 AND @loanType=0)
	      begin
		  (
			SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
			where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo)
			)
			union
			(
			SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
			where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (task.PageTypeId = @productionType ) and (StartTime between @dateFrom and @dateTo)
			)
		end   	  
	 Else if(@lenderId=0)
	   begin
		     SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
          where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.LoanTypeId=@loanType

		  union
		  SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
			where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (task.PageTypeId = @productionType )and (StartTime between @dateFrom and @dateTo) and fhaRequest.LoanTypeId=@loanType
	  end
	 else
	   begin
	     if(@loanType=0)
		   begin
		      SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
          where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.LenderId=@lenderId
		  union
			(
			SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
			where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (task.PageTypeId = @productionType ) and (StartTime between @dateFrom and @dateTo) and fhaRequest.LenderId=@lenderId
			)
		   end
		  else
		    begin
			   SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
          where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) 
		         and fhaRequest.LoanTypeId=@loanType and fhaRequest.LenderId=@lenderId
            union
			(
			SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.LoanTypeId,fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
			where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (task.PageTypeId = @productionType ) and (StartTime between @dateFrom and @dateTo) 
		         and fhaRequest.LoanTypeId=@loanType and fhaRequest.LenderId=@lenderId
			)

		    end
	   end
		
		
  end
  Else if(@productionType>4)
  begin
        if(@lenderId=0)
	       begin 
      SELECT [TaskId],task.TaskInstanceId , '(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
                 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,opa.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status
				 ,pageType.PageTypeDescription as Type,DATEDIFF(day,task.StartTime,GETDATE()) AS duration,PA.ProjectTypename as LoanType,fhaRequest.LoanAmount as LoanAmountInt
              FROM [$(DatabaseName)].[dbo].[Task] task
			   join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			   join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
                join [$(LiveDB)].dbo.OPAForm opa
                on task.TaskInstanceId = opa.TaskInstanceId
				 join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
				 left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
				 on PA. ProjecttypeId=opa.Projectactiontypeid
        where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo)
		end
	 else
	  begin
	  SELECT [TaskId],task.TaskInstanceId , '(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
                 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,opa.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status
				 ,pageType.PageTypeDescription as Type,DATEDIFF(day,task.StartTime,GETDATE()) AS duration,PA.ProjectTypename as LoanType,fhaRequest.LoanAmount as LoanAmountInt
              FROM [$(DatabaseName)].[dbo].[Task] task
			   join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			   join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
                join [$(LiveDB)].dbo.OPAForm opa
                on task.TaskInstanceId = opa.TaskInstanceId
				 join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
				 left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
				 on PA. ProjecttypeId=opa.Projectactiontypeid
        where (task.PageTypeId = @productionType ) and task.TaskStepId=17 and lender.LenderID=@lenderId and (StartTime between @dateFrom and @dateTo)
	  end
  end
  
  Else 
    begin 
	if(@lenderId = 0 and @loanType = 0)
	
	 begin 
		  
	     SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
				join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
         where (task.PageTypeId = 4) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) 	
		 union (
		 SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
				join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
		 where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (StartTime between @dateFrom and @dateTo)
		 )	 	
        union
           (
             SELECT [TaskId],task.TaskInstanceId ,'(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName ,[SequenceId],[AssignedBy],[AssignedTo] ,[DueDate]
                     ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,opa.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status
					 ,pageType.PageTypeDescription as Type,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,PA.ProjectTypename as LoanType,fhaRequest.LoanAmount as LoanAmountInt
              FROM [$(DatabaseName)].[dbo].[Task] task
			   join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			   join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
                   join [$(LiveDB)].dbo.OPAForm opa
                   on task.TaskInstanceId = opa.TaskInstanceId
				   join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
				   left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
				 on PA. ProjecttypeId=opa.Projectactiontypeid
             where (task.PageTypeId >4 ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo)
		 )
	  end
	  Else if(@lenderId=0)
	       begin 
		  
	     SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
				join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
         where (task.PageTypeId = 4) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.ProjectTypeId = @loanType
		 union (
		 SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			 fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
				join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId = fhaRequest.ProjectTypeId
		 where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (StartTime between @dateFrom and @dateTo) and fhaRequest.ProjectTypeId = @loanType
		 )	 	
        union
           (
             SELECT [TaskId],task.TaskInstanceId ,'(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName ,[SequenceId],[AssignedBy],[AssignedTo] ,[DueDate]
                     ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,opa.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status
					 ,pageType.PageTypeDescription as Type,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,PA.ProjectTypename as LoanType,fhaRequest.LoanAmount as LoanAmountInt
              FROM [$(DatabaseName)].[dbo].[Task] task
			   join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			   join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
                   join [$(LiveDB)].dbo.OPAForm opa
                   on task.TaskInstanceId = opa.TaskInstanceId
				   join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
				   left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
				 on PA. ProjecttypeId=opa.Projectactiontypeid
             where (task.PageTypeId >4 ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.ProjectTypeId = @loanType
		 )
	  end
   else
	  begin 
	  (
		SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId =fhaRequest.ProjectTypeId
           where (task.PageTypeId = 4) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.LenderId=@lenderId
		   		  
		   )
		   union
		   (
		      
			  SELECT  [TaskId],task.TaskInstanceId,fhaRequest.ProjectName as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
			 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,
			fhaRequest.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status,pageType.PageTypeDescription as Type
			 ,DATEDIFF(day,task.StartTime,GETDATE()) AS Duration,loanType.ProjectTypeName as LoanType,fhaRequest.LoanAmount as LoanAmountInt
			 FROM [$(DatabaseName)].[dbo].[Task] task
			 join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			 join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
			join [$(LiveDB)].[dbo].[Prod_FHANumberRequest] fhaRequest
			join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
			
			on task.TaskInstanceId = fhaRequest.TaskinstanceId
			join [$(LiveDB)].dbo.Prod_ProjectType loanType on loanType.ProjectTypeId =fhaRequest.ProjectTypeId
			  where task.TaskInstanceID in (select TaskInstanceID from @ParentCompletedFHA) and (StartTime between @dateFrom and @dateTo) and fhaRequest.LenderId=@lenderId
		   )
       union
          (
             SELECT [TaskId],task.TaskInstanceId , '(' +opa.fhaNumber +')'+ PA.ProjectTypename as TaskName,[SequenceId] ,[AssignedBy],[AssignedTo] ,[DueDate]
                 ,[StartTime],[Notes],task.[TaskStepId],[IsReassigned],task.[PageTypeId],lender.Lender_Name as Lender,opa.ModifiedOn as ModofiedOn,taskStep.TaskStepNm as Status
				 ,pageType.PageTypeDescription as Type,DATEDIFF(day,task.StartTime,GETDATE()) AS duration,PA.ProjectTypename as LoanType,fhaRequest.LoanAmount as LoanAmountInt
              FROM [$(DatabaseName)].[dbo].[Task] task
			   join TaskStep taskStep on taskStep.TaskStepId= task.TaskStepId
			   join [$(LiveDB)].dbo.HCP_PageType pageType on pageType.PageTypeId = task.PageTypeId
                join [$(LiveDB)].dbo.OPAForm opa
                on task.TaskInstanceId = opa.TaskInstanceId
				join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
          on opa.fhaNumber=fhaRequest.fhanumber
	      left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
				 left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
				 on PA. ProjecttypeId=opa.Projectactiontypeid
             where (task.PageTypeId >4 ) and task.TaskStepId=17 and (StartTime between @dateFrom and @dateTo) and fhaRequest.LenderId=@lenderId
		  )
	     end
	end
	    		
 
  
  
  end

