﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_FileNameByFolderKeyForDropDown]
(
@FolderKey int
)
AS
/**
08/20/2018	rganapathy			Closing checklist updated and inactivated some documents for FolderKey=30


**/
BEGIN
-- getting details from  document tabbele and FolderStructure based on Folder Key
IF(@FolderKey = 30)
Begin 
print @FolderKey
select * from (
SELECT   f.ShortDocTypeName
		,f.DocumentType
		,f.DocumentTypeId
	    ,f.FolderKey 
		,f.Sorting
 from [dbo].[DocumentTypes] f  join [dbo].[Prod_FolderStructure] sb  
		on f.FolderKey=sb.FolderKey where f.FolderKey=@FolderKey and ShortDocTypeName not in 
	   ('1','1.a.','1.b.','6','7','8','9','10','11') --Inactivated documents for closing

 union  
 -- getting details from  document tabbele and SubFolderStructure based on Folder Key

SELECT   f.ShortDocTypeName
		,f.DocumentType
		,f.DocumentTypeId
		,f.FolderKey 
		,f.Sorting

 from [dbo].[DocumentTypes] f 
 join [dbo].[Prod_SubFolderStructure] sb on f.FolderKey= sb.ParentKey 
 --where f.FolderKey=@FolderKey) x 
 where f.FolderKey=(select ParentKey from [dbo].[Prod_SubFolderStructure] where FolderKey= @FolderKey)
		and ShortDocTypeName not in ('1','1.a.','1.b.','6','7','8','9','10','11') --Inactivated documents for closing
 ) x 
 order by Sorting asc
-- this finction seperate special caractors after that doing order by used cast because some of them or no comming in order  
--order by cast(dbo.usp_HCP_Prod_SeparateSpecialCharactersFromColum(SUBSTRING( ShortDocTypeName, 1, 4 ), '^0-9') as int) asc

End
Else
Begin
select * from (
SELECT   f.ShortDocTypeName
		,f.DocumentType
		,f.DocumentTypeId
	    ,f.FolderKey 
		,f.Sorting
 from [dbo].[DocumentTypes] f  join [dbo].[Prod_FolderStructure] sb  
		on f.FolderKey=sb.FolderKey where f.FolderKey=@FolderKey
 union  
 -- getting details from  document tabbele and SubFolderStructure based on Folder Key

SELECT   f.ShortDocTypeName
		,f.DocumentType
		,f.DocumentTypeId
		,f.FolderKey 
		,f.Sorting

 from [dbo].[DocumentTypes] f 
 join [dbo].[Prod_SubFolderStructure] sb on f.FolderKey= sb.ParentKey 
 --where f.FolderKey=@FolderKey) x 
 where f.FolderKey=(select ParentKey from [dbo].[Prod_SubFolderStructure] where FolderKey= @FolderKey)) x 
 order by Sorting,ShortDocTypeName asc
-- this finction seperate special caractors after that doing order by used cast because some of them or no comming in order  
--order by cast([HCP_Task_Prod].dbo.usp_HCP_Prod_SeparateSpecialCharactersFromColum(SUBSTRING( ShortDocTypeName, 1, 4 ), '^0-9') as int) asc

ENd

		
end
GO

