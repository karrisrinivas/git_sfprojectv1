﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetCompletedAndWaitingStartTime]
(
@TaskInstanceId uniqueidentifier
)
AS 
-- Returns the first name, last name, job title, and contact type for the specified contact.
BEGIN
CREATE TABLE #StartTime 
(
    -- Columns returned by the function
    CompletedStartTime DATETIME NOT NULL, 
    WaitingStartTime DATETIME NOT NULL
)
    INSERT INTO #StartTime 
    SELECT TOP 1 
        (SELECT TOP 1 StartTime
    FROM  dbo.Task  
    WHERE TaskInstanceId = @TaskInstanceId
						AND SequenceId 
							IN	
						(SELECT TOP 1 MAX(SequenceId)
						FROM  dbo.Task 
						WHERE TaskInstanceId = @TaskInstanceId)),    
     
		(SELECT TOP 1 StartTime
    FROM  dbo.Task 
	WHERE TaskInstanceId = @TaskInstanceId
						AND SequenceId 
						IN	
						(SELECT TOP 1 MIN(SequenceId)
						FROM  dbo.Task 
						WHERE TaskInstanceId = @TaskInstanceId))
   FROM  dbo.Task T
   WHERE T.TaskInstanceId = @TaskInstanceId
   END;
   SELECT s.CompletedStartTime, s.WaitingStartTime from #StartTime s
    
SET FMTONLY ON
GO

