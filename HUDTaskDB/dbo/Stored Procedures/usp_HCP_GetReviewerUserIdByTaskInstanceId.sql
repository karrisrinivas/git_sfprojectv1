﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetReviewerUserIdByTaskInstanceId]
( @TaskInstanceId uniqueidentifier)
AS
BEGIN
       
       

       select * from Task t
       join [$(LiveDB)].dbo.HCP_Authentication au
       on t.AssignedTo = au.Username
       where t.SequenceId = 0 and t.taskinstanceid = @TaskInstanceId
END