﻿CREATE PROCEDURE [dbo].[usp_HCP_GetTaskDetailsForAE]
(
@AEId INT,
@FromDate datetime,
@ToDate datetime,
@FromTaskAge int,
@ToTaskAge int,
@ProjectAction varchar( max )
)
AS

/*
07/18/2018		Rashmi		Removing duplicates from OPA records
02/08/2019		Rashmi		Bug: 4989-Fixed Ambiguous column TaskInstanceid error
*/
BEGIN
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
                IF (@FromDate IS NOT NULL AND @ToDate IS NULL)    SET @ToDate =     GETDATE ()
                IF (@FromDate IS NULL AND @ToDate IS NOT NULL)    SET @FromDate ='01-01-2015'
                IF (@FromDate IS NULL AND @ToDate IS NULL)  
                BEGIN   
                                SET @FromDate = '01-01-2015'
                                SET @ToDate =    GETDATE()
                END
                DECLARE @AEName VARCHAR ( MAX)     
                DECLARE @AEEmail VARCHAR ( MAX)
                DECLARE @SelectedProjectActions TABLE ( PamProjectActionId INT )        
                IF @ProjectAction IS NULL OR @ProjectAction = ''
                                INSERT INTO @SelectedProjectActions  SELECT PamProjectActionId FROM   [$(LiveDB)].dbo.PamProjectActionTypes
                ELSE
                                INSERT INTO @SelectedProjectActions SELECT * FROM   [$(LiveDB)]. dbo. fn_StringSplitterToInt (@ProjectAction )
                SET @AEName = (SELECT HUD_Project_Manager_Name FROM [$(LiveDB)]. dbo. HUD_Project_Manager WHERE HUD_Project_Manager_ID = @AEId)
                SET @AEEmail = (SELECT AD .EMAIL FROM [$(LiveDB)].dbo.Address AD JOIN
                                                                                [$(LiveDB)]. dbo. HUD_Project_Manager PM
                                                                               ON AD. AddressID = PM .AddressID WHERE HUD_Project_Manager_ID = @AEId )
                CREATE TABLE #TaskDetails
                (
                                TaskInstanceId UNIQUEIDENTIFIER ,
                                FhaNumber NVARCHAR (15 ),
                                PropertyName NVARCHAR (MAX ),
                                TaskName NVARCHAR (MAX ),
                                DateSubmitted DATETIME ,
                                TaskStatus NVARCHAR (MAX ),
                                TaskAge INT ,
                                AccountExecutiveName NVARCHAR (MAX ),
                                DateAssigned DATETIME
                );
                IF @ProjectAction is null  OR Exists( SELECT a. PamProjectActionId  FROM   @SelectedProjectActions a INNER JOIN  [$(LiveDB)]. dbo. [PamProjectActionTypes] b on  a . PamProjectActionId = b. PamProjectActionId where  b .PamProjectActionId = 1 )
                                BEGIN
                                                INSERT INTO #TaskDetails                                            
                                                SELECT A.*, TR .CreatedOn FROM
                                                                ( SELECT TK.TaskInstanceId, R4R. FHANumber , PropertyName , 'Reserve for Replacement' ASTaskName,
																StartTime, 'In-Process' AS TaskStatus , DATEDIFF ( dd, StartTime ,GETDATE ()) ASTaskAge, 
																--PM. HUD_Project_Manager_Name
																(SELECT FirstName + ' ' + LastName  FROM [$(LiveDB)]. dbo .HCP_Authentication auth WHERE auth.UserName = tk.AssignedTo) as AccountExecutiveName
                                                                FROM dbo. Task TK  JOIN [$(LiveDB)].dbo.Reserve_For_Replacement_Form_Data R4R
                                                                ON TK. TaskId =R4R. TaskId
                                                                JOIN [$(LiveDB)]. dbo. ProjectInfo PF ON R4R . FhaNumber = PF .FHANumber
                                                                JOIN [$(LiveDB)]. dbo. HUD_Project_Manager PM ON PF . HUD_Project_Manager_ID = PM. HUD_Project_Manager_ID
                                                          WHERE TK. TaskInstanceId IN
                                                                (( SELECT T.TaskInstanceId FROM TASK T
                                                                               JOIN   [$(LiveDB)].dbo.Reserve_For_Replacement_Form_Data R
                                                                               ON T. TaskId = R .TaskId WHERE   AssignedTo = @AEEmail AND RequestStatus = 1 AND ( IsReassigned IS NULL OR IsReassigned = 0 ))
                                                                UNION
                                                                SELECT TaskInstanceId FROM TaskReAssignments
                                                                               WHERE   ReAssignedTo = @AEEmail AND ( Deleted_Ind IS NULL OR Deleted_Ind = 0 ))AND  RequestStatus = 1) A
                                                LEFT JOIN TaskReAssignments TR ON A. TaskInstanceId = TR .TaskInstanceId
                                                WHERE TR. Deleted_Ind = 0 OR TR. Deleted_Ind IS NULL
                                END
                IF @ProjectAction is null  OR Exists( SELECT a. PamProjectActionId  FROM   @SelectedProjectActions a INNER JOIN  [$(LiveDB)]. dbo. [PamProjectActionTypes] b on  a . PamProjectActionId = b. PamProjectActionId where  b .PamProjectActionId = 0 )
                                BEGIN
                                                INSERT INTO #TaskDetails
                                                SELECT A.*, TR .CreatedOn FROM
                                                                ( SELECT TK.TaskInstanceId, NCR. FHANumber , PF . ProjectName, 'Non-Critical Repairs Request'AS TaskName,
                                                                                                                 StartTime, 'In-Process' AS TaskStatus , DATEDIFF ( dd, StartTime ,GETDATE ()) ASTaskAge,
															 --PM. HUD_Project_Manager_Name
															 (SELECT FirstName + ' ' + LastName  FROM [$(LiveDB)]. dbo .HCP_Authentication auth WHERE auth.UserName = tk.AssignedTo) as AccountExecutiveName
                                                                FROM dbo. Task TK  JOIN [$(LiveDB)].dbo.NonCriticalRepairsRequests NCR
                                                                ON TK. TaskId =NCR. TaskId
                                                                JOIN [$(LiveDB)]. dbo. ProjectInfo PF ON NCR . FhaNumber = PF .FHANumber
                                                                JOIN [$(LiveDB)]. dbo. HUD_Project_Manager PM ON PF . HUD_Project_Manager_ID = PM. HUD_Project_Manager_ID
                                                                WHERE   TK. TaskInstanceId IN
                                                                (( SELECT TaskInstanceId FROM TASK T
                                                                               JOIN   [$(LiveDB)].dbo.NonCriticalRepairsRequests N
                                                                               ON T. TaskId = N .TaskId WHERE   AssignedTo = @AEEmail AND RequestStatus = 1 AND ( IsReassigned IS NULL OR IsReassigned = 0 ))
                                                                UNION
                                                                SELECT TaskInstanceId FROM TaskReAssignments
                                                                               WHERE   ReAssignedTo = @AEEmail  AND ( Deleted_Ind IS NULL OR Deleted_Ind = 0 ))AND  RequestStatus = 1) A
                                                LEFT JOIN TaskReAssignments TR
                                                ON A. TaskInstanceId = TR. TaskInstanceId
                                                WHERE TR. Deleted_Ind = 0 OR TR. Deleted_Ind IS NULL
                                END
                IF @ProjectAction is null  OR Exists( SELECT a. PamProjectActionId  FROM   @SelectedProjectActions a INNER JOIN  [$(LiveDB)]. dbo. [PamProjectActionTypes] b on  a . PamProjectActionId = b. PamProjectActionId where  b .PamProjectActionId = 2 )
                                BEGIN
                                                INSERT INTO #TaskDetails
                                                SELECT A.*, TR .CreatedOn FROM
                                                                ( SELECT TK.TaskInstanceId, NX. FHANumber , PF . ProjectName, 'Non-Critical Request Extension'  AS TaskName,
                                                                                                                 StartTime, 'In-Process' AS TaskStatus , DATEDIFF ( dd, StartTime ,GETDATE ()) ASTaskAge,
																--PM. HUD_Project_Manager_Name
																(SELECT FirstName + ' ' + LastName  FROM [$(LiveDB)]. dbo .HCP_Authentication auth WHERE auth.UserName = tk.AssignedTo) as AccountExecutiveName
                                                                FROM dbo. Task TK  JOIN [$(LiveDB)].dbo.NonCriticalRequestExtensions NX
                                                                ON TK. TaskId =NX. TaskId
                                                                JOIN [$(LiveDB)]. dbo. ProjectInfo PF ON NX . FhaNumber = PF .FHANumber
                                                                JOIN [$(LiveDB)]. dbo. HUD_Project_Manager PM ON PF . HUD_Project_Manager_ID = PM. HUD_Project_Manager_ID
                                                                WHERE TK. TaskInstanceId IN
                                                                (( SELECT TaskInstanceId FROM TASK T
                                                                               JOIN   [$(LiveDB)].dbo.NonCriticalRequestExtensions N
                                                                               ON T. TaskId = N .TaskId WHERE   AssignedTo = @AEEmail AND ExtensionRequestStatus =  1 AND ( IsReassigned IS NULL OR IsReassigned = 0 ))
                                                                UNION
                                                                SELECT TaskInstanceId FROM TaskReAssignments
                                                                               WHERE   ReAssignedTo = @AEEmail  AND ( Deleted_Ind IS NULL OR Deleted_Ind = 0 ))AND  ExtensionRequestStatus = 1) A
                                                LEFT JOIN TaskReAssignments TR
                                                ON A. TaskInstanceId = TR. TaskInstanceId
                                                WHERE TR. Deleted_Ind = 0 OR TR. Deleted_Ind IS NULL
                                END
  IF @ProjectAction is null   OR Exists( SELECT a . PamProjectActionId FROM   @SelectedProjectActions a INNER JOIN  [$(LiveDB)].dbo. [PamProjectActionTypes] b on   a. PamProjectActionId = b . PamProjectActionId 
  where b .PamProjectActionId > 2 )  
                                BEGIN
                                                INSERT INTO #TaskDetails
                                                SELECT A.*, TR .CreatedOn FROM
                                                                ( SELECT distinct TK.TaskInstanceId, PAF.FHANumber, PF.ProjectName, PA.ProjectActionName,
                                                                StartTime, 'In-Process' AS TaskStatus, DATEDIFF(dd,StartTime,GETDATE()) AS TaskAge, 
																--PM.HUD_Project_Manager_Name
																(SELECT FirstName + ' ' + LastName  FROM [$(LiveDB)]. dbo .HCP_Authentication auth WHERE auth.UserName = tk.AssignedTo) as AccountExecutiveName
                                                                FROM (select TaskInstanceId,fhanumber,max(starttime) as StartTime,max(assignedto) as AssignedTo
																		from  [$(DatabaseName)].dbo.Task 	
																		group by TaskInstanceId,fhanumber
																		having Max(Sequenceid) = 0) TK 
																JOIN [$(LiveDB)].dbo.OPAForm  PAF
                                                                ON TK.Taskinstanceid = PAF.Taskinstanceid 
																
																Left  JOIN [$(DatabaseName)].dbo.ParentChildTask PC 
																ON TK.Taskinstanceid=PC.ParentTaskinstanceid
                                                                JOIN [$(LiveDB)].dbo.ProjectInfo PF ON PAF.FhaNumber = PF.FHANumber 
                                                                JOIN [$(LiveDB)].dbo.HUD_Project_Manager PM ON PF.HUD_Project_Manager_ID = PM.HUD_Project_Manager_ID
                                                                JOIN [$(LiveDB)].dbo.HCP_Project_Action PA ON PAF.ProjectActionTypeId = PA.ProjectActionID
                             WHERE   TK. TaskInstanceId IN
                                                                (( SELECT T. TaskInstanceId FROM TASK T
                                                                               JOIN   [$(LiveDB)].dbo.OPAForm P
                                                                               ON T. Taskinstanceid = P .Taskinstanceid WHERE   AssignedTo = @AEEmail AND RequestStatus = 1 AND   ( IsReassigned IS NULL OR IsReassigned = 0 ))
                                                                UNION
                                                                SELECT TaskInstanceId FROM TaskReAssignments
                                                                               WHERE   ReAssignedTo = @AEEmail  AND ( Deleted_Ind IS NULL OR Deleted_Ind = 0 ))AND  RequestStatus = 1) A
                                                LEFT JOIN TaskReAssignments TR
                                                ON A. TaskInstanceId = TR. TaskInstanceId
                                                WHERE TR. Deleted_Ind = 0 OR TR. Deleted_Ind IS NULL
                                END
                --IF @ProjectAction is null  OR Exists(SELECT a.PamProjectActionId FROM  @SelectedProjectActions a INNER JOIN  [$(LiveDB)].dbo.[PamProjectActionTypes] b on  a.PamProjectActionId = b.PamProjectActionId where b.PamProjectActionId = 10) 
                --             BEGIN
                --                             INSERT INTO #TaskDetails                            
                --                             Select   TK.TaskInstanceId, TK.DataStore1.value('(/FormUploadModel/FHANumber)[1]','varchar(100)'),
                --                             TK.ProjectName, 'Financial Analysis', TK.StartTime,
                --                             'In-Process', DATEDIFF(dd,TK.StartTime,GETDATE()), TR.CreatedOn, @AEName
                --                             FROM
                --                             (SELECT * FROM Task  T
                --                                             JOIN [$(LiveDB)].dbo.ProjectInfo P
                --                                             ON T.DataStore1.value('(/FormUploadModel/FHANumber)[1]','varchar(100)') = P.FHANumber
                --                                             WHERE AssignedTo =  @AEEmail and TaskStepId = 3
                --                                             AND DataStore1.exist('/FormUploadModel') = 'true' AND IsReassigned <> 1) TK
                --                             FULL OUTER JOIN
                --                             (SELECT * FROM TaskReAssignments WHERE  ReAssignedTo = @AEName) TR
                --                             on TK.TaskInstanceId = TR.TaskInstanceId
                --             END
                IF (@FromTaskAge IS NOT NULL AND @FromTaskAge <> 0 ) AND ( @ToTaskAge IS NOT NULL AND @ToTaskAge <> 0 )
                                BEGIN
                                                IF @FromTaskAge = 1 SET @FromTaskAge = 0   
                                                SELECT * FROM #TaskDetails WHERE TaskAge BETWEEN @FromTaskAge AND @ToTaskAge ORDER BY TaskAge DESC               
                                END
                ELSE IF ( @FromTaskAge IS NOT NULL AND @FromTaskAge <> 0 ) AND ( @ToTaskAge IS NULL OR @ToTaskAge = 0 )       
                                SELECT * FROM #TaskDetails WHERE TaskAge >=@FromTaskAge ORDER BY TaskAge DESC                                          
                ELSE IF ( @FromTaskAge IS NULL OR @FromTaskAge = 0 ) AND ( @ToTaskAge IS NULL OR @ToTaskAge = 0)
                                BEGIN                                                    
                                                IF @FromDate IS NOT NULL AND @ToDate IS NOT NULL
                                                                SELECT distinct    * FROM #TaskDetails WHERE CONVERT( DATE ,DateSubmitted ) BETWEEN CONVERT( date ,@FromDate) AND CONVERT( date , @ToDate ) ORDER BY TaskAge DESC
                                                ELSE
                                                                SELECT distinct *  FROM #TaskDetails ORDER BY TaskAge DESC    
                                END
END