﻿
CREATE TABLE [dbo].[GroupTasks](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[RequestStatus] [int] NOT NULL,
	[InUse] [int] NULL,
	[FhaNumber] [varchar](20) NULL,
	[PropertyName] [varchar](100) NULL,
	[RequestDate] [datetime] NOT NULL,
	[RequesterName] [varchar](100) NULL,
	[ServicerSubmissionDate] [datetime] NULL,
	[ProjectActionStartDate] [datetime] NULL,
	[ProjectActionTypeId] [int] NOT NULL,
	[IsDisclimerAccepted] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ServicerComments] [varchar](500) NULL,
	[IsAddressChanged] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

