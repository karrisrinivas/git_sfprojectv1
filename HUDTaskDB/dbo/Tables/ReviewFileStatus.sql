﻿
CREATE TABLE [dbo].[ReviewFileStatus](
	[ReviewFileStatusId] [uniqueidentifier] NOT NULL,
	[TaskFileId] [uniqueidentifier] NOT NULL,
	[ReviewerUserId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[IsChildTaskCreated] [bit] NULL,
	[ReviewerProdViewId] [int] NULL,
 CONSTRAINT [PK_ReviewFileStatus] PRIMARY KEY CLUSTERED 
(
	[ReviewFileStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

