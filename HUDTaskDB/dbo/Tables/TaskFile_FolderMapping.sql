﻿
CREATE TABLE [dbo].[TaskFile_FolderMapping](
	[fileFolderId] [int] IDENTITY(1,1) NOT NULL,
	[FolderKey] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[TaskFileId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

