﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class ProjectInfoViewModel
    {
        public string ProjectName { get; set; }
        public string ServiceName { get; set; }
        public string FHANumber { get; set; }
    }
}
