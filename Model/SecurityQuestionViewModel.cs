﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    public class SecurityQuestionViewModel
    {
        public IList<SelectListItem> QuestionList { get; set; }

        public SecurityQuestionViewModel()
        {
            QuestionList = new List<SelectListItem>();
            IsAnswerStep = true;
            IsViewOrResttingPwdHints = false;//karri    
        }

        public bool IsAnswerStep { get; set; }
        public bool IsViewOrResttingPwdHints { get; set; }
        //karri: flag for identifition of user action during password hints
        public string PwdHintsSaveStatus { get; set; }
        //karri:content message shown after saving password hints during view/reset password hints
        //to keep the value state of the values we bind them to hidden variable in the POST form

        [Required]
        [Display(Name = "First Security Question")]
        [DifferFrom("SecondQuestionId", "ThirdQuestionId", ErrorMessage = "Not unique question one")]
        public string FirstQuestionId { get; set; }

        [Required]
        [Display(Name = "Answer 1")]
        public string FirstAnswer { get; set; }

        [Required]
        [Display(Name = "Second Security Question")]
        [DifferFrom("FirstQuestionId", "ThirdQuestionId", ErrorMessage = "Not unique question two")]
        public string SecondQuestionId { get; set; }

        [Required]
        [Display(Name = "Answer 2")]
        public string SecondAnswer { get; set; }

        [Required]
        [Display(Name = "Third Security Question")]
        [DifferFrom("FirstQuestionId", "SecondQuestionId", ErrorMessage = "Not unique question three")]
        public string ThirdQuestionId { get; set; }

        [Required]
        [Display(Name = "Answer 3")]
        public string ThirdAnswer { get; set; }

        public string UserAnswer1 { get; set; }
        public string UserAnswer2 { get; set; }
        public string UserAnswer3 { get; set; }
        public string UserName { get; set; }
    }
}
