﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HUDHealthcarePortal.Model
{
    public class PaginateSortModel<T> where T : class, new()
    {
        public static PaginateSortModel<T> CreateInstance()
        {
            return new PaginateSortModel<T>();
        }
        public int TotalRows { get; set; }
        public IEnumerable<T> Entities { get; set; }
        public int PageSize { get; set; }
    }

    public enum SqlOrderByDirecton
    {
        ASC,
        DESC
    }

    public static class PaginateSort
    {
        public static PaginateSortModel<T> SortAndPaginate<T>(this IQueryable<T> query,
                                         string strSortBy,
                                         SqlOrderByDirecton sortOrder,
                                         int pageSize,
                                         int? pageNum) where T : class, new()
        {
            int startRecord = ((pageNum ?? 1) - 1) * pageSize;
            var pageSortModel = PaginateSortModel<T>.CreateInstance();

            var sortByList = new List<string>();
            if (!string.IsNullOrEmpty(strSortBy)) 
                sortByList = strSortBy.Split(',').ToList();

            IEnumerable<T> list = null;
            if (sortOrder == SqlOrderByDirecton.ASC)
            {
                if (sortByList.Count <= 1)
                {
                    if (string.IsNullOrEmpty(strSortBy))
                        list = query.Skip(startRecord).Take(pageSize);
                    else
                        list = query.OrderBy(strSortBy).Skip(startRecord).Take(pageSize);
                }
                else
                {
                    list = query.OrderBy(sortByList[0]).ThenBy(sortByList[1]).Skip(startRecord).Take(pageSize);
                }
            }
            else
            {
                if (sortByList.Count <= 1)
                {
                    if (string.IsNullOrEmpty(strSortBy))
                        list = query.Skip(startRecord).Take(pageSize);
                    else
                        list = query.OrderByDescending(strSortBy).Skip(startRecord).Take(pageSize);
                }
                else
                {
                    list = query.OrderByDescending(sortByList[0]).ThenByDescending(sortByList[1]).Skip(startRecord).Take(pageSize);
                }
            }
            pageSortModel.Entities = list.ToList();
            pageSortModel.TotalRows = query.Count();
            pageSortModel.PageSize = pageSize;
            return pageSortModel;
        }
        public static PaginateSortModel<T> PAMSortAndPaginate<T>(this IQueryable<T> query,
                                         string strSortBy,
                                         SqlOrderByDirecton sortOrder,
                                         int pageSize,
                                         int? pageNum) where T : class, new()
        {
            int startRecord = ((pageNum ?? 1) - 1) * pageSize;
            var pageSortModel = PaginateSortModel<T>.CreateInstance();

            var sortByList = new List<string>();
            if (!string.IsNullOrEmpty(strSortBy)) sortByList = strSortBy.Split(',').ToList();

            IEnumerable<T> list = null;
            if (sortOrder == SqlOrderByDirecton.ASC)
            {
                if (sortByList.Count <= 1)
                {
                    list = query.OrderBy(strSortBy).Skip(startRecord).Take(pageSize);
                }
                else
                {
                    list = query.OrderByDescending(sortByList[0]).ThenBy(sortByList[1]).Skip(startRecord).Take(pageSize);
                }
            }
            else
            {
                if (sortByList.Count <= 1)
                {
                    list = query.OrderByDescending(strSortBy).Skip(startRecord).Take(pageSize);
                }
                else
                {
                    list = query.OrderByDescending(sortByList[0]).ThenBy(sortByList[1]).Skip(startRecord).Take(pageSize);
                }
            }
            pageSortModel.Entities = list.ToList();
            pageSortModel.TotalRows = query.Count();
            pageSortModel.PageSize = pageSize;
            return pageSortModel;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string property)
        {
            return ApplyOrder<T>(query, property, "OrderBy");
        }
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> query, string property)
        {
            return ApplyOrder<T>(query, property, "OrderByDescending");
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> query, string property)
        {
            return ApplyOrder<T>(query, property, "ThenBy");
        }
        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> query, string property)
        {
            return ApplyOrder<T>(query, property, "ThenByDescending");
        }
        private static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> query, string property, string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach(string prop in props)
            {
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                               method => method.Name == methodName
                               && method.IsGenericMethodDefinition
                               && method.GetGenericArguments().Length == 2
                               && method.GetParameters().Length == 2)
                               .MakeGenericMethod(typeof(T), type)
                               .Invoke(null, new object[] { query, lambda });
            return (IOrderedQueryable<T>)result;
        }
    }

    public static class PaginateSort_List
    {
        public static PaginateSortModel<T> SortAndPaginateList<T>(this IEnumerable<T> query,
                                         string strSortBy,
                                         SqlOrderByDirecton sortOrder,
                                         int pageSize,
                                         int pageNum) where T : class, new()
        {
            int startRecord = (pageNum - 1) * pageSize;
            var pageSortModel = PaginateSortModel<T>.CreateInstance();

            var sortByList = strSortBy.Split(',').ToList();

        

            IEnumerable<T> list = null;
            if (sortOrder == SqlOrderByDirecton.ASC)
            {
                if (sortByList.Count <= 1)
                {
           
                    list = query.ToList().OrderBy(x => GetPropertyValue(x, sortByList[0].Trim())).Skip(startRecord).Take(pageSize);

                }
                else
                {

         
                    list = query.ToList().OrderBy(x => GetPropertyValue(x, sortByList[0].Trim())).ThenBy(x => GetPropertyValue(x, sortByList[1].Trim())).Skip(startRecord).Take(pageSize);
     
                }
            }
            else
            {

                if (sortByList.Count <= 1)

                   
                {
              
                    list = query.ToList().OrderByDescending(x => GetPropertyValue(x, sortByList[0].Trim())).Skip(startRecord).Take(pageSize);
                }
                else
                {
                    list=query.ToList().OrderByDescending(x => GetPropertyValue(x, sortByList[0].Trim())).ThenByDescending(x => GetPropertyValue(x, sortByList[1].Trim())).Skip(startRecord).Take(pageSize);

                  
                }


            }

            pageSortModel.Entities = list.ToList();
            pageSortModel.TotalRows = query.Count();
            pageSortModel.PageSize = pageSize;
            return pageSortModel;
        }

        public static IEnumerable<T> OrderByGeneric<T>(this IEnumerable<T> input, string queryString, SqlOrderByDirecton sortOrder)
        {
            if (string.IsNullOrEmpty(queryString))
                return input;

            int i = 0;
            foreach (string propname in queryString.Split(','))
            {
                var subContent = propname.Split('|');
                if (Convert.ToInt32(subContent[1].Trim()) == 0)
                {
                    if (i == 0)
                        input = input.OrderBy(x => GetPropertyValue(x, subContent[0].Trim()));
                    else
                        input = ((IOrderedEnumerable<T>)input).ThenBy(x => GetPropertyValue(x, subContent[0].Trim()));
                }
                else
                {
                    if (i == 0)
                        input = input.OrderByDescending(x => GetPropertyValue(x, subContent[0].Trim()));
                    else
                        input = ((IOrderedEnumerable<T>)input).ThenByDescending(x => GetPropertyValue(x, subContent[0].Trim()));
                }
                i++;
            }

            return input;
        }

        private static object GetPropertyValue(object obj, string property)
        {
            System.Reflection.PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
            return propertyInfo.GetValue(obj, null);
        }
    }
}
