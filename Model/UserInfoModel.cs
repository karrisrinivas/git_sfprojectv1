﻿using System;

namespace HUDHealthcarePortal.Model
{
    public class UserInfoModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
        public string Lender_Name { get; set; }
        public Nullable<int> LenderID { get; set; }
        public Nullable<int> ServicerID { get; set; }
        public int UserID { get; set; }
        public int? WorkloadManagerId { get; set; }
        public int? AccountExecutiveId { get; set; }
        public string FhaNumber { get; set; }

        public string Name
        {
            get{ return FirstName + ' ' + LastName; }
        }
    }
}
