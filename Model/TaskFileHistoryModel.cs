﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class TaskFileHistoryModel
    {
        public Guid TaskFileHistoryId { get; set; }
        public Guid ParentTaskFileId { get; set; }
        public Guid ChildTaskFileId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public int TransactionId { get; set; }
    }
}
