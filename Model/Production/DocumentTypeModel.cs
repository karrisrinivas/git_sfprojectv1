﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Model.Production
{
    public class DocumentTypeModel : Prod_TaskXrefModel
    {
       public int DocumentTypeId { get; set; }
       public string DocumentType { get; set; }
       public string ShortDocTypeName { get; set; }
       public int FolderKey { get; set; }
       public SelectListItem FilesToChange { get; set; }
      
       public int CurrentUserId { get; set; }
       public string FileIdlist { get; set; }
       public int AvailableFolders { get; set; }

       
      
    }
}
