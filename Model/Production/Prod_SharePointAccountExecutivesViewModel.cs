﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class Prod_SharePointAccountExecutivesViewModel
    {
        public int SharePointAEId { get; set; }
        public string SharePointAEName { get; set; }
    }
}
