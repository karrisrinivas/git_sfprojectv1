﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public  class FolderUploadModel
    {
        public int FolderKey { get; set; }
        public int CreatedBy { get; set; }
        public string ParentModel { get; set; }
    }
}
