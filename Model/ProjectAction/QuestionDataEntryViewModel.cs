﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model.ProjectAction
{
    public class QuestionDataEntryViewModel
    {
        public IList<SelectListItem> AllProjectActions { get; set; }
        public int SelectedProjectAction { get; set; }

        public QuestionDataEntryViewModel()
        {
            AllProjectActions = new List<SelectListItem>();
        }
    }
}
