﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TaskDetailPerAeModel
    {
        public Guid TaskInstanceId { get; set; }
        public string FhaNumber { get; set; }
        public string PropertyName { get; set; }
        public string TaskName { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime? DateAssigned { get; set; }
        public string AccountExecutiveName { get; set; }
        public string TaskStatus { get; set; }
        public int TaskAge { get; set; }
        public bool IsSelected { get; set; }
    }
}
