﻿using System;

namespace HUDHealthcarePortal.Model
{
    public class AdhocReportModel
    {

        public string Portfolio_Number { get; set; }
        public string Portfolio_Name { get; set; }
        public string FHA_Number { get; set; }
        public string Project_Name { get; set; }
        public int Property_Id { get; set; }
        public string Property_Name { get; set; }
        public string ServiceName { get; set; }
        public int? LenderID { get; set; }
        public string Lender_Name { get; set; }
        public string Lender_Primary_Phone { get; set; }
        public string Lender_eMail { get; set; }
        public int? Servicer_ID { get; set; }

        public string OperatorOwner { get; set; }
        public DateTime PeriodEnding { get; set; }
        public int? MonthsInPeriod { get; set; }
        public string FinancialStatementType { get; set; }
        public int? UnitsInFacility { get; set; }
        public decimal? OperatingCash { get; set; }
        public decimal? Investments { get; set; }
        public decimal? ReserveForReplacementEscrowBalance { get; set; }
        public decimal? AccountsReceivable { get; set; }
        public decimal? CurrentAssets { get; set; }
        public decimal? CurrentLiabilities { get; set; }
        public decimal? TotalRevenues { get; set; }
        public decimal? RentLeaseExpense { get; set; }
        public decimal? DepreciationExpense { get; set; }
        public decimal? AmortizationExpense { get; set; }
        public decimal? TotalExpenses { get; set; }
        public decimal? NetIncome { get; set; }
        public decimal? ReserveForReplacementDeposit { get; set; }
        public decimal? FHAInsuredPrincipalPayment { get; set; }
        public decimal? FHAInsuredInterestPayment { get; set; }
        public decimal? MortgageInsurancePremium { get; set; }
        public DateTime? DateInserted { get; set; }

        public int? HUD_Project_Manager_ID { get; set; }
        public string Account_Executive_Name { get; set; }
        public string Account_Executive_Primary_Phone { get; set; }
        public string Account_Executive_eMail { get; set; }
        public int LDI_ID { get; set; }
        public decimal? ReserveForReplacementBalancePerUnit { get; set; }
        public decimal? WorkingCapital { get; set; }
        public decimal? DebtCoverageRatio { get; set; }
        public decimal? DaysCashOnHand { get; set; }
        public decimal? DaysInAcctReceivable { get; set; }
        public decimal? AvgPaymentPeriod { get; set; }
        public decimal? WorkingCapitalScore { get; set; }
        public decimal? DebtCoverageRatioScore { get; set; }
        public decimal? DaysCashOnHandScore { get; set; }
        public decimal? DaysInAcctReceivableScore { get; set; }
        public decimal? AvgPaymentPeriodScore { get; set; }
        public decimal? ScoreTotal { get; set; }
        public int? ModifiedBy { get; set; }
        public int? OnBehalfOfBy { get; set; }
        public int UserID { get; set; }
        public bool? Deleted_Ind { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public int ProjectInfoID { get; set; }
        public Nullable<int> Property_AddressID { get; set; }
        public Nullable<int> ServicerID { get; set; }
        public Nullable<int> ManagementID { get; set; }
        public Nullable<int> HUD_WorkLoad_Manager_ID { get; set; }
        public string WorkLoad_Manager_Name { get; set; }
        public string WorkLoad_Manager_Primary_Phone { get; set; }
        public string WorkLoad_Manager_eMail { get; set; }
        public Nullable<int> Primary_Loan_Code { get; set; }
        public Nullable<System.DateTime> Initial_Endorsement_Date { get; set; }
        public Nullable<System.DateTime> Final_Endorsement_Date { get; set; }
        public string Field_Office_Status_Name { get; set; }
        public string SOA_Code { get; set; }
        public string Soa_Numeric_Name { get; set; }
        public string Soa_Description_Text { get; set; }
        public string Troubled_Code { get; set; }
        public string Troubled_status_update_date { get; set; }
        public string Reac_Last_Inspection_Score { get; set; }
        public Nullable<System.DateTime> Reac_Last_Inspection_Date { get; set; }
        public string Mddr_In_Default_Status_Name { get; set; }
        public string Is_Active_Dec_Case_Ind { get; set; }
        public Nullable<decimal> Original_Loan_Amount { get; set; }
        public Nullable<decimal> Original_Interest_Rate { get; set; }
        public Nullable<decimal> Amortized_Unpaid_Principal_Bal { get; set; }
        public string Is_Nursing_Home_Ind { get; set; }
        public string Is_Board_And_Care_Ind { get; set; }
        public string Is_Assisted_Living_Ind { get; set; }
        public Nullable<int> Lender_AddressID { get; set; }
        public string Lender_Mortgagee_Name { get; set; }
        public Nullable<int> Servicer_AddressID { get; set; }
        public string Servicing_Mortgagee_Name { get; set; }
        public string Servicer_Organization { get; set; }
        public string Servicer_Address_Line1 { get; set; }
        public string Servicer_Address_Line2 { get; set; }
        public string Servicer_City { get; set; }
        public string Servicer_Zip { get; set; }
        public string Servicer_Zip_4 { get; set; }
        public Nullable<int> Owner_AddressID { get; set; }
        public string Owner_Company_Type { get; set; }
        public string Owner_Legal_Structure_Name { get; set; }
        public string Owner_Organization_Name { get; set; }
        public string Owner_eMail { get; set; }
        public string Owner_Primary_Phone { get; set; }
        public string Owner_Address_Line1 { get; set; }
        public string Owner_Address_Line2 { get; set; }
        public string Owner_City { get; set; }
        public string Owner_State { get; set; }
        public string Owner_Zip { get; set; }
        public string Owner_Zip_4 { get; set; }

        public string Owner_Contact_Indv_Fullname { get; set; }
        public string Owner_Contact_Indv_Title { get; set; }
        public string Owner_Contact_Address_Line1 { get; set; }
        public string Owner_Contact_Address_Line2 { get; set; }
        public string Owner_Contact_City { get; set; }
        public string Owner_Contact_State { get; set; }
        public string Owner_Contact_Zip { get; set; }
        public string Owner_Contact_Zip_4 { get; set; }
        public string Owner_Contact_Primary_Phone { get; set; }
        public string Owner_Contact_eMail { get; set; }

        public string Mgmt_Agent_Company_Type { get; set; }
        public string Mgmt_Agent_org_name { get; set; }
        public string Management_Agent_Address_Line1 { get; set; }
        public string Management_Agent_Address_Line2 { get; set; }
        public string Management_Agent_City { get; set; }
        public string Management_Agent_State { get; set; }
        public string Management_Agent_Zip { get; set; }
        public string Management_Agent_Zip_4 { get; set; }
        public string Management_Agent_Primary_Phone { get; set; }
        public string Management_Agent_eMail { get; set; }

        public string Mgmt_Contact_FullName { get; set; }
        public string Mgmt_Contact_Indv_Title { get; set; }
        public string Management_Contact_Address_Line1 { get; set; }
        public string Management_Contact_Address_Line2 { get; set; }
        public string Management_Contact_City { get; set; }
        public string Management_Contact_State { get; set; }
        public string Management_Contact_Zip { get; set; }
        public string Management_Contact_Zip_4 { get; set; }
        public string Management_Contact_Primary_Phone { get; set; }
        public string Management_Contact_eMail { get; set; }

    }
}
