﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
	public class FirmCommitmentAmendmentTypeModel
	{
		public int FirmCommitmentAmendmentTypeId { get; set; }
		public string FirmCommitmentAmendment { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}


