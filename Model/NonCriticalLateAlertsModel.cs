﻿using System;

namespace Model
{
    public class NonCriticalLateAlertsModel
    {
        public int PropertyID { get; set; }
        public int LenderID { get; set; }
        public string FHANumber { get; set; }
        public string PropertyName { get; set; }
        public string LenderName { get; set; }
        public int TimeSpanId { get; set; }
        public int HCP_EmailId { get; set; }
        public string RecipientIds { get; set; }
    }
}