﻿namespace HUDHealthcarePortal.Model
{
    public class FhaInfoModel
    {
        public string FHANumber { get; set; }
        public string FHADescription { get; set; }
        public int? LenderId { get; set; }
        public string Lender_Name { get; set; }
    }
}
