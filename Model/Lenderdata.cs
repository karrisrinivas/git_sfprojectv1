﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

//#189
namespace HUDHealthcarePortal.Model
{
    public class Lenderdata
    {
        public int LenderID { get; set; }

        [Required]
        [StringLength(100)]
        public string Lender_Name { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }
    }

}
