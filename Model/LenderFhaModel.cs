﻿using System;

namespace HUDHealthcarePortal.Model
{
    public class LenderFhaModel
    {
        public int LenderID { get; set; }
        public string FHANumber { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> OnBehalfOfBy { get; set; }
        public Nullable<System.DateTime> FHA_StartDate { get; set; }
        public Nullable<System.DateTime> FHA_EndDate { get; set; }
    }
}
