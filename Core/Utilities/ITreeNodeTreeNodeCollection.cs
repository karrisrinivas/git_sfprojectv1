﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HUDHealthcarePortal.Core.Utilities
{
    public interface ITreeNodeTreeNodeCollection<T> : IEnumerable<T>, ICloneable, IEquatable<T> where T : class, ITreeNodeTreeNodeCollection<T>
    {
        T Parent { get; }
        ReadOnlyCollection<T> Children { get; }
        Guid NodeId { get; }
        int Level { get; }
        void SetParent(T parent);
        void AddChild(T child);
        void AddChildren(IEnumerable<T> child);
        T ShallowCopy();
        bool FullEquals(T node);
    }
}
