﻿using HUDHealthcarePortal.Core;

namespace Core.Interfaces
{
    public interface IUploadedLoanVisitor
    {
        DerivedFinancial VisitorType { get; }
        void Visit(FundamentalFinancial fundamentalFin);
    }
}
