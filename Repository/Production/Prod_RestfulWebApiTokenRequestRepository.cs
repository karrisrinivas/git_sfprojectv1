﻿using HUDHealthcarePortal.Core;
using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
    public class Prod_RestfulWebApiTokenRequestRepository : IProd_RestfulWebApiTokenRequestRepository
    {

        public Prod_RestfulWebApiTokenRequestRepository()
        {

        }
        public RestfulWebApiTokenResultModel RequestToken()
        {
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            string URL = ConfigurationManager.AppSettings["GenerateAccessToken"].ToString();
            // latest changes by venkatesh on 11/01/2017
            string UserNamePSL = ConfigurationManager.AppSettings["UserNamePSL"].ToString();
            string PasswordPSL = ConfigurationManager.AppSettings["PasswordPSL"].ToString();
            string AccessuserPSL = ConfigurationManager.AppSettings["AccessuserPSL"].ToString();
            //
            RestfulWebApiTokenResultModel auth = new RestfulWebApiTokenResultModel();
            var formData = new List<KeyValuePair<string, string>>();
            formData.Add(new KeyValuePair<string, string>("grant_type", "password"));
            //formData.Add(new KeyValuePair<string, string>("username", UserPrincipal.Current.UserName));
            //formData.Add(new KeyValuePair<string, string>("password", "default"));
            //formData.Add(new KeyValuePair<string, string>("username", "FHA232/H15494"));
            //formData.Add(new KeyValuePair<string, string>("password", "Nashjr01"));
            //formData.Add(new KeyValuePair<string, string>("accessUser", "FHA232/H15494"));
            //formData.Add(new KeyValuePair<string, string>("username", "FHA232/GARTHUR"));
            //formData.Add(new KeyValuePair<string, string>("password", "Cescrew813!"));
            //formData.Add(new KeyValuePair<string, string>("accessUser", "FHA232/GARTHUR"));
            formData.Add(new KeyValuePair<string, string>("username", UserNamePSL));
            formData.Add(new KeyValuePair<string, string>("password", PasswordPSL));
            formData.Add(new KeyValuePair<string, string>("accessUser", AccessuserPSL));
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseURL);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var request = new HttpRequestMessage(HttpMethod.Post, URL);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                                                             Convert.ToBase64String(
                                                             System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                             string.Format("{0}:{1}", "restapp", "restapp"))));
                request.Content = new FormUrlEncodedContent(formData);
                //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
                try
                {
                    using (HttpResponseMessage responseMessage = client.SendAsync(request).Result)
                    {

                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        auth = JsonConvert.DeserializeObject<RestfulWebApiTokenResultModel>(responseData);
                        return auth;
                    }
                }
                catch (Exception ex)
                {
                   // throw ex; 
                }
                return auth;

            }
        }
    }
}
