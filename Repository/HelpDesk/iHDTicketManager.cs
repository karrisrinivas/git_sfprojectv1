﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HUDHealthcarePortal.Model;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;

//karri;#149
namespace HelpDeskController.Repository
{
    public interface iHDTicketManager
    {
        //save
        //update status, assignment, messaging, 
        //void saveTicket(HD_TICKET v1);
        //void saveTicket(HD_TICKET_ViewModel v1);
        int saveTicket(HD_TICKET_ViewModel v1);
        //we will filter in the manger rather than writing one more layer for the filteration
        List<HD_TICKET> GetAllTickets();
    }
}