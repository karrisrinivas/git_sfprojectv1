﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class FirmCommitmentAmendmentTypeRepository : BaseRepository<FirmCommitmentAmendmentType>, IFirmCommitmentAmendmentTypeRepository
	{
		public FirmCommitmentAmendmentTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public FirmCommitmentAmendmentTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<FirmCommitmentAmendmentType> DataToJoin
		{
			get { return DbQueryRoot; }
		}
	
		public List<FirmCommitmentAmendmentTypeModel> GetFirmCommitmentAmendmentTypes()
		{
			var firmCommitments = this.GetAll().ToList();
			//try
			//{
			//	var d = Mapper.Map<List<FirmCommitmentAmendmentTypeModel>>(firmCommitments);
			//}
			//catch (Exception e)
			//{
			//	Console.Write(e.Message);
			//}
			return Mapper.Map<List<FirmCommitmentAmendmentTypeModel>>(firmCommitments);
		}
	}
}


