﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;

namespace HUDHealthcarePortal.Repository
{
    public class ChildTaskNewFileRepository : BaseRepository<TaskDB.ChildTaskNewFiles>, IChildTaskNewFileRepository
    {
        public ChildTaskNewFileRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ChildTaskNewFileRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public void SaveChildTaskNewFiles(ChildTaskNewFileModel newFile)
        {
            var context = (TaskDB.HCP_task)this.Context;
                var task = Mapper.Map<TaskDB.ChildTaskNewFiles>(newFile);
                this.InsertNew(task);
            
            context.SaveChanges();
        }

        public bool IsFileExistsForChildTask(Guid childTaskInstanceId)
        {
            var context = (TaskDB.HCP_task)this.Context;
            var results =
                (from n in context.ChildTaskNewFiles select n).Where(p => p.ChildTaskInstanceId == childTaskInstanceId)
                    .ToList();
            return results.Any();
        }
    }
}
