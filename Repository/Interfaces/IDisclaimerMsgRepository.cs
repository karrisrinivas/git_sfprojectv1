﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IDisclaimerMsgRepository
    {
        // User Story 1901
       string GetDisclaimerMsgByPageType(string pageTypeName);
    }
}
