﻿using Model.InternalExternalTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.InternalExternalTask
{
   public interface IInternalExternalTaskRepository
    {
        Guid AddInternalExternalTask(InternalExternalTaskModel internalExternalTask);
        bool UpdateInternalExternalTask(InternalExternalTaskModel internalExternalTask);

        InternalExternalTaskModel FindByTaskInstanceID(Guid taskInstanceID);
        InternalExternalTaskModel FindByFHANuber(string fhaNumber);

        List<InternalExternalTaskModel> GetAllTasks();

        List<String> GetAvailableFHANumbers();
        InternalExternalTaskModel GetNotCompletedTask(int pageTypeID, string fhaNumber);
         bool UpdateNoiTask(InternalExternalTaskModel model);


    }
}
