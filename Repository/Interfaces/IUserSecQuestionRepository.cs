﻿using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUserSecQuestionRepository
    {
        SecurityQuestionViewModel GetSecQuestionsByUserId(int userId);
        void SaveUserSecQuestions(int userid, SecurityQuestionViewModel securityQuesionModel);
    }
}
