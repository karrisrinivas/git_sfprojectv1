﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model.AssetManagement;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository.Interfaces.AssetManagement
{
    public interface IReserveForReplacementRepository: IJoinable<Reserve_For_Replacement_Form_Data>
    {
        IEnumerable<ReserveForReplacementFormModel> GetR4RFormByPropertyId(int propertyId);
        Guid SaveR4RForm(ReserveForReplacementFormModel model);
        void UpdateR4RForm(ReserveForReplacementFormModel model);
        void UpdateTaskId(ReserveForReplacementFormModel model);
        ReserveForReplacementFormModel GetR4RFormById(Guid r4rId);
        void UpdateApprovedAmount(decimal changeApprovedAmount, string FHANumber, string HudRemarks);
        List<string> GetAllFHANumbersListForR4R();
        ReserveForReplacementFormModel GetApprovedAmountByFHANumber(string fhaNumber);
        List<String> GetSubmitedFHANumbers(int lenderID);
		ReserveForReplacementFormModel GetR4RFormByFHANumber(string fhaNumber);
		ReserveForReplacementFormModel GetR4RbyTaskInstanceId(Guid taskInstanceId);
    }
}
