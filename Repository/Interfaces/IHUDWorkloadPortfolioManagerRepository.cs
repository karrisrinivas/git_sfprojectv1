﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IHUDWorkloadPortfolioManagerRepository
    {
        int GetWorkloadManagerIdFromAeId(int aeId);
        string GetWorkloadManagerEmailFromAeId(int aeId);
    }
}
