﻿using EntityObject.Entities.HCP_live.Programmability;
using HUDHealthcarePortal.Model;


namespace Repository.Interfaces
{


    public interface IUploadStatusHelpdeskreportsRepository
    {
        //System.Collections.Generic.IEnumerable<HD_TICKET_ViewModel> GetUploadStatusByLenderId(int lendId);

        System.Collections.Generic.IEnumerable<usp_HCP_GetTicketDetails_Latest> GetUploadStatusDetailByhelpdeskreport(string Status, string FromDate, string ToDate, string HelpDeskName);
    }
}
