﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using Model.Production;

namespace Repository.Interfaces
{
    public interface ITaskRepository
    {
        IEnumerable<TaskModel> GetTasksByUserName(string userName);
        Guid AddTask(TaskModel model);
        TaskModel GetTaskById(int taskId);
        void UpdateTask(TaskModel task);
        IEnumerable<TaskModel> GetTasksByTaskInstanceId(Guid taskInstanceId);
        TaskModel GetLatestTaskByTaskInstanceId(Guid taskInstanceId);
        void UpdateTaskReassigned(IList<Guid> taskIdList);
        void SaveReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null);
        int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId);
        IEnumerable<TaskModel> GetProductionTasks();
        IEnumerable<TaskModel> GetProductionQueues();
        IEnumerable<TaskModel> GetProductionTaskByType(int type);
        IEnumerable<TaskModel> GetProductionTaskByStatus(int status);
        IEnumerable<TaskModel> GetProductionTaskByTypeAndStatus(int type,int status);
        bool AssignProductionFhaInsert(ProductionTaskAssignmentModel model);
        void UpdateTaskStatus(TaskModel task);
        IEnumerable<TaskModel> GetProductionQueueByType(int type);
        void UpdateTaskNotes(int TaskId, string HudRemarks);
        void UpdateDataStore1(int TaskId, string dataStoreXML);
        int CreateFHARequestTasks(string assignedBy, Guid? taskInstanceID, int userId, string userRoleName,
            bool isPortfolioRequired, bool isCreditReviewRequired);
        IEnumerable<ProductionMyTaskModel> GetProductionTaskByUserName(string userName,string role);
        IEnumerable<FilteredProductionTasksModel> GetFilteredProductionTasks(int productionType, DateTime dateFrom, DateTime dateTo, int lenderId, int loanType);

        //Sharepoint screen
        IEnumerable<ApplicationAndClosingTypeModel> GetApplicationAndClosingType(Guid taskInstanceId);
        IEnumerable<TaskModel> GetCompletedFHAsAndNotAssignedChild();
        #region Production Application
        Prod_TaskXrefModel GetReviewerViewIdByXrefTaskInstanceId(Guid xrefTaskInstanceId);
        int CheckAmendmentExist(string selectedFhaNumber, int PageTypeId, int ViewId);
        TaskModel GetLatestTaskByTaskXrefid(Guid taskXrefId);
        int GetFolderKeyForChildFileId(Guid ParentTaskFileId);       
        void UpdateTaskAssignment(TaskModel task);
        void UpdateTaskStatus(Guid taskInstanceId);
        string GetAssignedCloserByFHANumber(string FHANumber);
        bool IsFirmCommitmentExists(Guid taskInstanceId);
        bool IsFirmCommitmentCompleted(Guid taskInstanceId);

        #endregion
        List<string> GetFHANumbersByInstanceIds(List<string> instanceIds);
        List<int> GetTaskIds(List<string> instanceIds);

        Guid AddForm290Task(TaskModel model);
        bool AssignForm290Task(ProductionTaskAssignmentModel model);
        string GetFHANumberByTaskInstanceId(Guid taskInstanceId);
		IEnumerable<TaskModel> GetTasksByFHANumber(string pFHANumber);

        //sfintegration
        int SFINT_FHA_Insert_TaskAck(int UserId, DateTime AssignedDate, Guid? taskInstanceID, int IsCreditReviewAttachComplete
            , string CreditreviewComments, DateTime CreditReviewDate, string Portfolio_Name, int Portfolio_Number, int IsPortfolioComplete
            , string PortfolioComments, string fhaNumber, int IsFhaInsertComplete, int RequestStatus, int IsReadyForApplication, int PropertyId);

    }
}
