﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
      public interface IProd_TaskXrefRepository
    {
          Guid AddTaskXref(Prod_TaskXrefModel model);
          List<Prod_TaskXrefModel> GetProductionSubTasks(Guid parentInstanceId);
          Guid AssignProductionFhaRequest(ProductionTaskAssignmentModel model);
          Prod_TaskXrefModel GetProductionSubtaskById(Guid taskXrefId);

          int UpdateTaskXrefAndFHARequest(Guid taskInstanceId, int viewId, string comments, string fhaNumber,
              string portfolioName, int portfolioNumber);
          bool UpdateTasXrefCompleteStatus(Guid taskXrefid, string comment);
          void UpdateTaskXrefForFHARequestDeny(Guid taskInstanceId, string comments);
		void UpdateTaskXrefForAmendments(Guid pTaskInstanceId, int pUserId);

		  void UpdateTaskXrefForFHARequestCancel(Guid taskInstanceId, string comments, int fhaRequestType);
         bool UpdateTasXrefComment(Guid taskXrefid, string comment);
          int GetCountOfReviewersTaskPendingForUW(Guid taskInstanceId);
          //Get the Task for Internal Viewid
          List<Prod_TaskXrefModel> GetProductionSubTasksforIR(Guid parentInstanceId);
        Guid GetUnderwriterTaskXrefIdByParentTaskInstanceId(Guid taskInstanceId);
        int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId, int viewId);
        int GetWLMUserIdByTaskInstanceId(Guid taskInstanceId);
    
		bool UpdateTasXref(Prod_TaskXrefModel pProd_TaskXrefModel);
		bool FindTasXrefForAmendmentWLMView(Guid pTaskXrefid);
		List<Guid> GetTasXrefForAmendmentWLMView(Guid pTaskXrefid);
	}
}
