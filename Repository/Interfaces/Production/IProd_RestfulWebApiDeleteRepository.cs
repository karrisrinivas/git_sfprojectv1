﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Production;
using System.Web;

namespace Repository.Interfaces.Production
{
   public interface IProd_RestfulWebApiDeleteRepository
    {
        RestfulWebApiResultModel DeleteDocument(string docid, string token);
        RestfulWebApiResultModel DeleteDocumentFolder(string JsonStr, string token);
    }
}
