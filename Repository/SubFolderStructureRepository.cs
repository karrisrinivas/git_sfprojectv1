﻿using System.Linq;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using Model.Production;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Repository;

namespace Repository
{
    public class SubFolderStructureRepository: BaseRepository<TaskDB.Prod_SubFolderStructure>, ISubFolderStructureRepository
    {
        public SubFolderStructureRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
        public SubFolderStructureRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        public int DeleteSubFolderByID(int ID)
        {
            var entityToUpdate = this.Find(p => p.FolderKey == ID).FirstOrDefault();
            if (entityToUpdate != null)
            {
               this.Delete(entityToUpdate);
                UnitOfWork.Save();
                return 1;
            }
            return 0;
           
        }
    }
}
