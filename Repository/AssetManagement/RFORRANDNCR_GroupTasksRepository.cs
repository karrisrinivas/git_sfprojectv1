﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.AssetManagement;
using Model.AssetManagement;


namespace Repository.AssetManagement
{
    public class RFORRANDNCR_GroupTasksRepository : BaseRepository<RFORRANDNCR_GroupTasks>, IRFORRANDNCR_GroupTasksRepository
    {
        public RFORRANDNCR_GroupTasksRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public RFORRANDNCR_GroupTasksRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public RFORRANDNCR_GroupTaskModel GetGroupTaskAByTaskInstanceId(Guid TaskInstanceId)
        {
            try
            {
                var context = (HCP_task)this.Context;
                //var Prodgrouptask = (from n in context.RFORRANDNCR_GroupTasks where n.TaskInstanceId == TaskInstanceId select n).FirstOrDefault();
                var Prodgrouptask = this.Find(m => m.TaskInstanceId == TaskInstanceId).FirstOrDefault();
                return Mapper.Map<RFORRANDNCR_GroupTaskModel>(Prodgrouptask);
            }
            catch (Exception ex)
            {

                //throw;
            }
            return null;
        }
        public Guid AddR4RGroupTasks(RFORRANDNCR_GroupTaskModel model)
        {
            try
            {
                RFORRANDNCR_GroupTasks RFORRANDNCR_GroupTasks = new RFORRANDNCR_GroupTasks();
                var context = (HCP_task)this.Context;
                //var groupTask = Mapper.Map<RFORRANDNCR_GroupTasks>(model);
                RFORRANDNCR_GroupTasks.TaskInstanceId = model.TaskInstanceId;
                RFORRANDNCR_GroupTasks.RequestStatus = model.RequestStatus;
                RFORRANDNCR_GroupTasks.InUse = model.InUse;
                RFORRANDNCR_GroupTasks.PageTypeId = model.PageTypeId;
                RFORRANDNCR_GroupTasks.CreatedBy = model.CreatedBy;
                RFORRANDNCR_GroupTasks.CreatedOn = model.CreatedOn;
                RFORRANDNCR_GroupTasks.ModifiedBy = model.ModifiedBy;
                RFORRANDNCR_GroupTasks.ModifiedOn = model.ModifiedOn;
                RFORRANDNCR_GroupTasks.IsDisclaimerAccepted = model.IsDisclaimerAccepted;
                RFORRANDNCR_GroupTasks.ServicerComments = model.ServicerComments;
                RFORRANDNCR_GroupTasks.FHANumber = model.FHANumber;
                this.InsertNew(RFORRANDNCR_GroupTasks);
                context.SaveChanges();
                return RFORRANDNCR_GroupTasks.TaskInstanceId;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return new Guid();
        }

        public void DeleteGroupTask(Guid TaskInstanceId, string FHANumber)
        {
            try
            {
                var context = (HCP_task)this.Context;

                var Prodgrouptask = this.Find(m => m.TaskInstanceId == TaskInstanceId && m.FHANumber == FHANumber).FirstOrDefault();
                this.Delete(Prodgrouptask);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
