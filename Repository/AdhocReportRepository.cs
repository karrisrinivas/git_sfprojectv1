﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class AdhocReportRepository : BaseRepository<AdhocReportViewModel>, IAdhocReportRepository
    {
        public AdhocReportRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public AdhocReportRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<AdhocReportModel> GetAdhocReport(string wlmList,
                                                            string aeList,
                                                            DateTime minPeriodEnd,
                                                            DateTime maxPeriodEnd, 
                                                            string partialFhaNumber, 
                                                            string lenderList, 
                                                            int activeAllSelected)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetAdhocReport, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            var results = context.Database.SqlQuerySimple<AdhocReportModel>("usp_HCP_Get_Adhoc_Detail",
                new { wlmListString = wlmList,
                      aeListString = aeList,
                      minPeriodEnding = minPeriodEnd,
                      maxPeriodEnding = maxPeriodEnd,
                      FhaNumberBegin = partialFhaNumber,
                      lenderListString = lenderList,
                      activeSelected = activeAllSelected }).ToList();
            return (results);
        }

        public IEnumerable<KeyValuePair<int, string>> GetAEsbyWLMList(string wlmList)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetAEsbyWLMList, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;

            var results = context.Database.SqlQuerySimple<ReportViewModel>("usp_HCP_Get_AEbyWLMlist",
            new
            {
                wlmListString = wlmList
            }).ToList().Select(p => new KeyValuePair<int, string>(p.HUD_Project_Manager_ID, p.HUD_Project_Manager_Name)).OrderBy(p => p.Value);
            return results;
        }

        public IEnumerable<KeyValuePair<int, string>> GetLendersbyWLMAEList(string wlmList, string aeList)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetLendersbyAEList, please pass in correct context in unit of work.");

            context.Database.CommandTimeout = 600;
            var results = context.Database.SqlQuerySimple<UserViewModel>("usp_HCP_Get_LenderbyWLMAElist",
            new
            {
                aeListString = aeList,
                wlmListString = wlmList
            }).ToList().Select(p => new KeyValuePair<int, string>(p.LenderID, p.LenderName)).OrderBy(p => p.Value);
            return results;
        }
    }
}
