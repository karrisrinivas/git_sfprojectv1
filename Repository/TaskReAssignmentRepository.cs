﻿using System.Data.Entity.Core.Objects;
using System.Data.Objects.SqlClient;
using System.Linq;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using Model;
using Repository;
using Repository.Interfaces;
using TaskDB = EntityObject.Entities.HCP_task;
namespace HUDHealthcarePortal.Repository
{
    public class TaskReAssignmentRepository : BaseRepository<TaskDB.TaskReAssignment>, ITaskReAssignmentRepository 
    {
         public TaskReAssignmentRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
         public TaskReAssignmentRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

         public IEnumerable<TaskReAssignmentViewModel> GetReAssginedTasksByUserName(string userName)
         {
             var context = this.Context as TaskDB.HCP_task;
             if (context == null)
                 throw new InvalidCastException("context is not from db live in usp_HCP_GetReAssignedTasksByUserName, please pass in correct context in unit of work.");
             var enumeratedList = Mapper.Map<IEnumerable<TaskReAssignmentViewModel>>(context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetReAssignedTasksByUserName_Result>("usp_HCP_GetReAssignedTasksByUserName",
                 new { UserName = userName })).ToList();


             foreach (var taskModel in enumeratedList)
             {
                 var mappedResult =
                     Mapper.Map<IEnumerable<TaskReAssignmentViewModel>>(context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetCompletedAndWaitingStartTime_Result>("usp_HCP_GetCompletedAndWaitingStartTime",
                     new { taskModel.TaskInstanceId })).ToList();
                 taskModel.CompletedStartTime = mappedResult[0].CompletedStartTime;
                 taskModel.WaitingStartTime = mappedResult[0].WaitingStartTime;
             }

             return enumeratedList;
         }

        public IEnumerable<KeyValuePair<string, string>> GetAllTaskAgeIntervals()
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in GetSubordinateAEsByWLMId, please pass in correct context in unit of work.");

           return (from n in context.TaskAges
                select n).ToList().Select(p => new KeyValuePair<string, string>(p.TaskAgeInterval, p.TaskAgeIntervalText))
                .OrderBy(p => p.Key);
        }

        public PaginateSortModel<TaskDetailPerAeModel> GetTaskDetailsForAE(string aeIds, DateTime? fromDate, DateTime? toDate, int fromTaskAge,
            int toTaskAge, string projectAction, int? page, string sort, SqlOrderByDirecton sortDir)
        {
            var context = this.Context as TaskDB.HCP_task;

            if (context == null)
                throw new InvalidCastException("context is not from db task in GetTaskDetailsForAE, please pass in correct context in unit of work.");
            context.Database.CommandTimeout = 600;
            //karri:#67;#68; the below sp is tuned to fix the error: ambuiguity in column TaskinstanceID; contact Tilak
            var results = context.Database.SqlQuerySimple<TaskDB.usp_HCP_GetTaskDetailsForAE_Result>("usp_HCP_GetTaskDetailsForAE",
            new
            {
                AEId = int.Parse(aeIds),
                FromDate = fromDate,
                ToDate = toDate,
                FromTaskAge = fromTaskAge,
                ToTaskAge = toTaskAge,
                ProjectAction = projectAction
            }).ToList();

            var taskDetailList =  Mapper.Map<IEnumerable<TaskDB.usp_HCP_GetTaskDetailsForAE_Result>, IEnumerable<TaskDetailPerAeModel>>(results).ToList();
            var resultList = new PaginateSortModel<TaskDetailPerAeModel>
            {
                Entities = taskDetailList.ToList(),
                TotalRows = taskDetailList.ToList().Count,
                PageSize = 10
            };

            //karri:#67;#68;session is storing previous values and results some times are not allowing the result set to display that has the records
            //lesser than 10; this value sis being mistakenly used somewherein displaying the result set
            if (results.Count == taskDetailList.Count && results.Count <= 10)
                page = 1;

            var query = resultList.Entities.AsQueryable();
            return PaginateSort.SortAndPaginate(query, sort, sortDir, resultList.PageSize, page);
        }

        public void InsertReassignedTasks(IList<TaskReAssignmentViewModel> taskList)
        {
            if (taskList != null)
            {
                foreach (var model in taskList)
                {
                    var task = Mapper.Map<TaskDB.TaskReAssignment>(model);
                    this.InsertNew(task);
                }
            }
        }

        public IList<TaskDetailPerAeModel> GetReassignedTasksForAE(string reAssignedTo, List<Guid> selectedTaskInstanceIds)
        {
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in GetReassignedTasksForAE, please pass in correct context in unit of work.");

            
            var results = (from n in context.TaskReAssignment
                           where n.ReAssignedTo == reAssignedTo
                           && selectedTaskInstanceIds.Any(x=>x == n.TaskInstanceId)
                           select n).ToList();
            return Mapper.Map<IEnumerable<TaskDB.TaskReAssignment>, IEnumerable<TaskDetailPerAeModel>>(results).ToList();
        }

        public void UpdateTaskReassignmentWithDeleted_Ind(List<Guid> taskInstanceIdList)
        {
            if (taskInstanceIdList != null)
            {
                foreach (var taskInstanceId in taskInstanceIdList)
                {
                    Guid instanceId = taskInstanceId;
                    var task = this.Find(p => p.TaskInstanceId == instanceId).OrderByDescending(p=>p.CreatedOn).FirstOrDefault();
                    if (task != null)
                    {
                        task.Deleted_Ind = true;
                    }
                    
                }
            }
        }


        public string GetReassignedAE(Guid TaskInstanceId)
        {
            string ReassignedTo = string.Empty;
            var context = this.Context as TaskDB.HCP_task;
            if (context == null)
                throw new InvalidCastException("context is not from db task in GetReassignedTasksForAE, please pass in correct context in unit of work.");



            var results = (from n in context.TaskReAssignment
                           where n.TaskInstanceId == TaskInstanceId
                           select n).FirstOrDefault();
            if (results != null)
            {
                ReassignedTo= results.ReAssignedTo;
            }

            return ReassignedTo;
             
                         
        }
    }
}
